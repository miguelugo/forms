<?php 

/**
 * Incluimos la conexion
 */
include '../conexion.php';
require_once("../modulos/sesion/include/class.phpmailer.php");
/**
 * Recuperamos los valores enviados desde el form
 */

$tipoBene             = $_POST["tipoBene"];
$primerApellido       = $_POST["primerApellido"];
$segundoApellido      = $_POST["segundoApellido"];
$nombre               = $_POST["nombre"];
$fechaNac             = $_POST["fechaNac"];
$sexo                 = $_POST["sexo"];
$estadoCivil          = $_POST["estadoCivil"];
$estudios             = $_POST["estudios"];
$identificacion       = $_POST["identificacion"];
$numeroIdentificacion = $_POST["numeroIdentificacion"];
$nacionalidad         = $_POST["nacionalidad"];
$entidadNac           = $_POST["entidadNac"];
$curp                 = $_POST["curp"];
$calle                = $_POST["calle"];
$numExt               = $_POST["numExt"];
$numInt               = $_POST["numInt"];
$calle1               = $_POST["calle1"];
$calle2               = $_POST["calle2"];
$otraRef              = $_POST["otraRef"];
$colonia              = $_POST["colonia"];
$localidad            = $_POST["localidad"];
$municipio            = $_POST["municipio"];
$entidad              = $_POST["entidad"];
$cp                   = $_POST["cp"];
$telFijo              = $_POST["telFijo"];
$telCel               = $_POST["telCel"];
$correo               = $_POST["correo"];
$taller               = $_POST["taller"];
$grupos               = $_POST["grupos"];

if ($numInt == "") {
	$numInt = 0;
}

/*          _ _     _            _                   _
__   ____ _| (_) __| | __ _  ___(_) ___  _ __     __| | ___    ___ ___  _ __ _ __ ___  ___    _   _    ___ _   _ _ __ _ __
\ \ / / _` | | |/ _` |/ _` |/ __| |/ _ \| '_ \   / _` |/ _ \  / __/ _ \| '__| '__/ _ \/ _ \  | | | |  / __| | | | '__| '_ \
 \ V / (_| | | | (_| | (_| | (__| | (_) | | | | | (_| |  __/ | (_| (_) | |  | | |  __/ (_) | | |_| | | (__| |_| | |  | |_) |
  \_/ \__,_|_|_|\__,_|\__,_|\___|_|\___/|_| |_|  \__,_|\___|  \___\___/|_|  |_|  \___|\___/   \__, |  \___|\__,_|_|  | .__/
                                                                                              |___/                  |_|
*/
/**
 * Comprobamos que el correo y la CURP no esten registrados
 */
$sql1 = "SELECT * FROM emprendendores WHERE correo = '$correo'";
$query = $conexion->prepare($sql1);
$query->execute();
$correoVa = $query->fetchAll();

$sql2 = "SELECT * FROM emprendendores WHERE curp = '$curp'";
$query = $conexion->prepare($sql2);
$query->execute();
$curpVa = $query->fetchAll();

if ($correoVa != null || $curpVa != null) {

	/**
	 * Si ya se encuentran registrados mostrara un mensaje de error
	 */

	if ($correoVa != null) {
		$mensaje = "El correo ya esta registrado";
	} else if ($curpVa != null) {
		$mensaje = "La CURP ya esta registrada";
	} else {
		$mensaje = "El correo y la CURP ya estan registrados";
	}
	echo "enviado desde el servidor";
?>

	<script src="../js/datosPer.js"></script>
		<script>

	$(document).ready(function() {

		$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '<Ant',
		 nextText: 'Sig>',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd-mm-yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
		 };
 			$.datepicker.setDefaults($.datepicker.regional['es']);

		$("#fechaNac").datepicker({
			changeMonth: true,
     		 changeYear: true,
     		 yearRange: '1940:2015'
		});

		html = "";
		
		$("#taller").change(function(event) {

			var nombreTaller = $(this).val();

			if (nombreTaller == "TESE Emprendiendo con éxito") {
				html = "";
				
				$("#grupos").html();
				html += '<select name="gruposNumero" id="gruposNumero">'
				html +=	'<option value="">Seleccione un opción</option>';
				for (var i = 1; i < 21; i++) {
						html += "<option value='Grupo "+i+"'> Grupo "+i+"</option>";
				};

				html += '</select>'
				$("#grupos").html(html);

			} else if(nombreTaller == "Emprendiendo en el CBT"){
				html = "";

				$("#grupos").html();
				html += '<select name="gruposNumero" id="gruposNumero">'
				html +=	'<option value="">Seleccione un opción</option>';
				for (var i = 1; i < 23; i++) {
						html += "<option value='Grupo "+i+"'> Grupo "+i+"</option>";
				};

				html += '</select>'
				$("#grupos").html(html);

			} else if(nombreTaller == "Tultitlán Emprende"){
				html = "";

				$("#grupos").html();
				html += '<select name="gruposNumero" id="gruposNumero">'
				html +=	'<option value="">Seleccione un opción</option>';
				for (var i = 1; i < 11; i++) {
						html += "<option value='Grupo "+i+"'> Grupo "+i+"</option>";
				};

				html += '</select>'
				$("#grupos").html(html);

			} else if(nombreTaller == "Innova y Emprende"){
				html = "";

				$("#grupos").html();
				html += '<select name="gruposNumero" id="gruposNumero">'
				html +=	'<option value="">Seleccione un opción</option>';
				for (var i = 1; i < 101; i++) {
						html += "<option value='Grupo "+i+"'> Grupo "+i+"</option>";
				};

				html += '</select>'
				$("#grupos").html(html);

			} else {

				html = "";

				$("#grupos").html();
				html += '<select name="gruposNumero" id="gruposNumero">'
				html +=	'<option value="">Seleccione un opción</option>';
				for (var i = 1; i < 26; i++) {
						html += "<option value='Grupo "+i+"'> Grupo "+i+"</option>";
				};

				html += '</select>'
				$("#grupos").html(html);


			}
			
		});

	});

	</script>
	

	<header>
		<img src="http://corporativogaia.com.mx/gaiav2/wp-content/uploads/2014/05/logo1.png" alt="">
	</header>
	<br>
	<br>
	<br>
	<div class="errorValidacion"><?php echo $mensaje ?></div>
	<form id="datosPersonales" method="post">
		
	<table class="datos" >
		<tr>
			<td>
				<a>Tipo de beneficiario:</a>
			</td>
			<td>
				<input type="text" id="tipoBene" name="tipoBene" value="Emprendedor">
			</td>
		</tr>
		<tr>
			<td>
				<a>Primer Apellido:</a>
			</td>
			<td>
				<input type="text" id="primerApellido" name="primerApellido" value="<?php echo $primerApellido ?>">
			</td>
		</tr>
		<tr>
			<td>
				<a>Segundo Apellido:</a>
			</td>
			<td>
				<input type="text" id="segundoApellido" name="segundoApellido" value="<?php echo $segundoApellido?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Nombre (s):</a>
			</td>
			<td>
				<input type="text" id="nombre" name="nombre" value="<?php echo $nombre?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Fecha de Nacimiento <span style="font-weight: bold;">(dd-mm-YYYY)</span>:</a>
			</td>
			<td>
				<input type="text" id="fechaNac" name="fechaNac" value="<?php echo $fechaNac?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Sexo:</a>
			</td>
			<td>
				<select name="sexo" id="sexo">
					<option value="">Seleccione un opción</option>
					<option value="F">Femenino</option>
					<option value="M">Masculino</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Estado Civil:</a>
			</td>
			<td>
				<select name="estadoCivil" id="estadoCivil">
					<option value="">Seleccione un opción</option>
					<option value="Soltero">Soltero</option>
					<option value="Casado">Casado</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Grado de estudios:</a>
			</td>
			<td>
				<select name="estudios" id="estudios">
					<option value="">Seleccione un opción</option>
					<option value="Primaria">Primaria</option>
					<option value="Secundaria">Secundaria</option>
					<option value="Bachillerato">Bachillerato</option>
					<option value="Universidad">Universidad</option>
					<option value="Posgrado">Posgrado</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Tipo de identificación oficial:</a>
			</td>
			<td>
				<select name="identificacion" id="identificacion">
					<option value="">Seleccione un opción</option>
					<option value="IFE">IFE</option>
					<option value="Pasaporte">Pasaporte</option>
					<option value="Credencial escolar">Credencial escolar</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Identificador del documento oficial (número):</a>
			</td>
			<td>
				<input type="text" id="numeroIdentificacion" name="numeroIdentificacion" value="<?php echo $numeroIdentificacion?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Nacionalidad:</a>
			</td>
			<td>
				<input type="text" id="nacionalidad" name="nacionalidad" value="Mexicana" >
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entidad de Nacimiento:</a>
			</td>
			<td>
				<select name="entidadNac" id="entidadNac">
					<option value="">Seleccione un opción</option>
					<option value="Aguascalientes">Aguascalientes</option>
					<option value="Baja California">Baja California</option>
					<option value="Baja California Sur">Baja California Sur</option>
					<option value="Campeche">Campeche</option>
					<option value="Chiapas">Chiapas</option>
					<option value="Chihuahua">Chihuahua</option>
					<option value="Coahuila">Coahuila</option>
					<option value="Colima">Colima</option>
					<option value="Distrito Federal">Distrito Federal</option>
					<option value="Durango">Durango</option>
					<option value="Estado de México">Estado de México</option>
					<option value="Guanajuato">Guanajuato</option>
					<option value="Guerrero">Guerrero</option>
					<option value="Hidalgo">Hidalgo</option>
					<option value="Jalisco">Jalisco</option>
					<option value="Michoacán">Michoacán</option>
					<option value="Morelos">Morelos</option>
					<option value="Nayarit">Nayarit</option>
					<option value="Nuevo León">Nuevo León</option>
					<option value="Oaxaca">Oaxaca</option>
					<option value="Puebla">Puebla</option>
					<option value="Querétaro">Querétaro</option>
					<option value="Quintana Roo">Quintana Roo</option>
					<option value="San Luis Potosí">San Luis Potosí</option>
					<option value="Sinaloa">Sinaloa</option>
					<option value="Sonora">Sonora</option>
					<option value="Tabasco">Tabasco</option>
					<option value="Tamaulipas">Tamaulipas</option>
					<option value="Veracruz">Veracruz</option>
					<option value="Yucatán">Yucatán</option>
					<option value="Zacatecas">Zacatecas</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Clave Única de Registro de Población (CURP):</a>
			</td>
			<td>
				<input type="text" id="curp" name="curp" value="<?php echo $curp?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Calle (domicilio):</a>
			</td>
			<td>
				<input type="text" id="calle" name="calle" value="<?php echo $calle?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Número exterior:</a>
			</td>
			<td>
				<input type="text" id="numExt" name="numExt" value="<?php echo $numExt?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Número interior:</a>
			</td>
			<td>
				<input type="text" id="numInt" name="numInt" value="<?php echo $numInt?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entre la calle:</a>
			</td>
			<td>
				<input type="text" id="calle1" name="calle1" value="<?php echo $calle1?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Y la calle:</a>
			</td>
			<td>
				<input type="text" id="calle2" name="calle2" value="<?php echo $calle2?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Otra referencia:</a>
			</td>
			<td>
				<input type="text" id="otraRef" name="otraRef" value="No contesto">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Colonia:</a>
			</td>
			<td>
				<input type="text" id="colonia" name="colonia" value="<?php echo $colonia?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Localidad:</a>
			</td>
			<td>
				<input type="text" id="localidad" name="localidad" value="<?php echo $localidad?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Municipio:</a>
			</td>
			<td>
				<input type="text" id="municipio" name="municipio" value="<?php echo $municipio?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entidad:</a>
			</td>
			<td>
				<select name="entidad" id="entidad">
					<option value="">Seleccione un opción</option>
					<option value="Aguascalientes">Aguascalientes</option>
					<option value="Baja California">Baja California</option>
					<option value="Baja California Sur">Baja California Sur</option>
					<option value="Campeche">Campeche</option>
					<option value="Chiapas">Chiapas</option>
					<option value="Chihuahua">Chihuahua</option>
					<option value="Coahuila">Coahuila</option>
					<option value="Colima">Colima</option>
					<option value="Distrito Federal">Distrito Federal</option>
					<option value="Durango">Durango</option>
					<option value="Estado de México">Estado de México</option>
					<option value="Guanajuato">Guanajuato</option>
					<option value="Guerrero">Guerrero</option>
					<option value="Hidalgo">Hidalgo</option>
					<option value="Jalisco">Jalisco</option>
					<option value="Michoacán">Michoacán</option>
					<option value="Morelos">Morelos</option>
					<option value="Nayarit">Nayarit</option>
					<option value="Nuevo León">Nuevo León</option>
					<option value="Oaxaca">Oaxaca</option>
					<option value="Puebla">Puebla</option>
					<option value="Querétaro">Querétaro</option>
					<option value="Quintana Roo">Quintana Roo</option>
					<option value="San Luis Potosí">San Luis Potosí</option>
					<option value="Sinaloa">Sinaloa</option>
					<option value="Sonora">Sonora</option>
					<option value="Tabasco">Tabasco</option>
					<option value="Tamaulipas">Tamaulipas</option>
					<option value="Veracruz">Veracruz</option>
					<option value="Yucatán">Yucatán</option>
					<option value="Zacatecas">Zacatecas</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Código Postal:</a>
			</td>
			<td>
				<input type="text" id="cp" name="cp" value="<?php echo $cp?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Teléfono fijo con lada:</a>
			</td>
			<td>
				<input type="text" id="telFijo" name="telFijo" maxlength="8" value="<?php echo $telFijo?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Teléfono celular:</a>
			</td>
			<td>
				<input type="text" id="telCel" name="telCel" maxlength="10" value="<?php echo $telCel?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Correo electrónico:</a>
			</td>
			<td>
				<input type="text" id="correo" name="correo" value="<?php echo $correo?>">
			</td>
		</tr>		
			<tr>
			<td>
				<a>Taller:</a>
			</td>
			<td>
				<select name="taller" id="taller">
					<option value="">Seleccione un opción</option>
					<option value="TESE Emprendiendo con éxito">TESE Emprendiendo con éxito</option>
					<option value="Emprendiendo en el CBT">Emprendiendo en el CBT</option>
					<option value="Tultitlán Emprende">Tultitlán Emprende</option>
					<option value="Innova y Emprende">Innova y Emprende</option>
					<option value="Ecatepec Emprende">Ecatepec Emprende</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Grupo:</a>
			</td>
			<td>
				<div id="grupos"></div>
			</td>
		</tr>		
			<tr>
			<td colspan="2" style="text-align: center;">
				<input type="checkbox" id="condiciones" name="condiciones" style="width: 30px;"><a target="_blank" href="http://www.corporativogaia.com.mx/gaiav2/aviso-de-privacidad/">Acepto aviso de privacidad de Gaia.</a>
			</td>
		</tr>		
		<tr>
			<td colspan="2">
				<button id="aceptar" name="aceptar">Guardar</button>
			</td>
		</tr>
	</table>

	</form>


	<div id="confirmacion" style="display: none">
		<p>¿Esta seguro que sus datos estan correctamente?</p>
	</div>



<?php 
		
} else {

	/*
	        _ _     _            _                                                  _         _                     _              _       _
__   ____ _| (_) __| | __ _  ___(_) ___  _ __    ___ _   _ _ __   ___ _ __ __ _  __| | __ _  (_)_ __  ___  ___ _ __| |_ __ _    __| | __ _| |_ ___  ___
\ \ / / _` | | |/ _` |/ _` |/ __| |/ _ \| '_ \  / __| | | | '_ \ / _ \ '__/ _` |/ _` |/ _` | | | '_ \/ __|/ _ \ '__| __/ _` |  / _` |/ _` | __/ _ \/ __|
 \ V / (_| | | | (_| | (_| | (__| | (_) | | | | \__ \ |_| | |_) |  __/ | | (_| | (_| | (_| | | | | | \__ \  __/ |  | || (_| | | (_| | (_| | || (_) \__ \
  \_/ \__,_|_|_|\__,_|\__,_|\___|_|\___/|_| |_| |___/\__,_| .__/ \___|_|  \__,_|\__,_|\__,_| |_|_| |_|___/\___|_|   \__\__,_|  \__,_|\__,_|\__\___/|___/
                                                          |_|
	*/
	
		/**
		 * Generamos la contraseña
		 */

		function generar_correo($length = 5) {
		    $chars = 'abcdefghijklmnopqrstuvwxyz';
		    $count = mb_strlen($chars);
		    for ($i = 0, $result = ''; $i < $length; $i++) {
		        $index = rand(0, $count - 1);
		        $result .= mb_substr($chars, $index, 1);
		    }
		    return $result;
		}

		$pass = generar_correo();

		/**
		 * Insertamos los datos a la base de datos
		 */
		$fechaAlta = date("Y-m-d");
		$fecha = date_create($fechaNac);
		$fecha = date_format($fecha, "Y-m-d");
		$sql = "INSERT INTO emprendendores (tipoBene, primerApellido, segundoApellido, nombre, fechaNac, sexo, estadoCivil, estudios, identificacion, numIdentificacion, nacionalidad, entidadNac, curp, domicilio, numExt, numInt, calle1, calle2, otraRef, colonia, localidad, municipio, entidad, cp, telFijo, telCel, correo, taller, grupo, contrasenia, fechaAlta) VALUES ('$tipoBene', '$primerApellido', '$segundoApellido', '$nombre', '$fecha', '$sexo', '$estadoCivil', '$estudios', '$identificacion', '$numeroIdentificacion', '$nacionalidad', '$entidadNac', '$curp', '$calle', '$numExt', '$numInt', '$calle1', '$calle2', '$otraRef', '$colonia', '$localidad', '$municipio', '$entidad', '$cp', '$telFijo', '$telCel', '$correo', '$taller', '$grupos', '$pass', '$fechaAlta')";
		$query = $conexion->prepare($sql);

		if ($query->execute()) {
			//echo "se inserto";
		} else {
			//echo "no se inserto";
		}

		$idEmprendedor = $conexion->lastInsertId();

		/**
		 * Insertamos el usuaior para el inicio de sesion
		 */
		$sql = "INSERT INTO usuarios (idEmprendedor, correo, contrasenia, fechaAlta) VALUES ($idEmprendedor, '$correo', md5('$pass'), '$fechaAlta')";
		$query = $conexion->prepare($sql);
		$query->execute();

		/**
		 * Enviamos correo de confirmacion
		 */
		////////////////////  ENVIAR CORREO  //////////////////////////////////////////
		$mail = new PHPMailer();
		$mail->CharSet = 'utf-8';
		$mail->IsSMTP();
		$mail->AddAddress($correo, $nombre." ".$primerApellido." ".$segundoApellido);
		$mail->Subject = "Gracias por registrarte";
		$mail->From = "no-replay@ideadiseno.com";        
		$mail->Body ="Hola ".$nombre." ".$primerApellido." ".$segundoApellido."\r\n\r\n".
		"Gracias por registrarte\r\n".
		"usuario: ".$correo."\r\n".
		"Password: ".$pass."\r\n";

		if(!$mail->Send()) {
		  //echo "Error: " . $mail->ErrorInfo;
		} else {
		  //echo "Mensaje enviado.";
		}
		//////////////////  FIN ENVIAR CORREO  //////////////////////////////////////////

?>


	<script src="../js/jquery-1.9.1.min.js"></script>
		<script src="../js/jquery-validate.js"></script>	

	<script src="../js/modulo1.js"></script>

	<header>
			
		<img src="http://corporativogaia.com.mx/gaiav2/wp-content/uploads/2014/05/logo1.png" alt="">

	</header>

	<p>Nota: El correo podría haber llegado a correo no deseado con su usuario y contraseña</p>

	<form method="post" id="modulo1">
		<input type="hidden" id="idEmprendedor" name="idEmprendedor" value="<?php echo $idEmprendedor ?>">
		<table class="datos">
			<tr>
				<td>Nombre del proyecto o idea de negocio:</td>
				<td><input type="text" id="nombreProyecto" name="nombreProyecto"></td>
			</tr>
			<tr>
				<td>Giro:</td>
				<td>
					<select name="giro" id="giro">
					<option value="">Seleccione un opción</option>
					<option value="Automotriz">Automotriz</option>
					<option value="Textil">Textil</option>
					<option value="Turismo">Turismo</option>
					<option value="Equipo y servicio Aeroespacial">Equipo y servicio Aeroespacial</option>
					<option value="Logística">Logística</option>
					<option value="Industria">Industria</option>
					<option value="Comercio">Comercio</option>
					<option value="Servicios">Servicios</option>
					<option value="Agroindustria">Agroindustria</option>
					<option value="Productos Químicos">Productos Químicos</option>
					<option value="Minería">Minería</option>
					<option value="Servicios de investigación">Servicios de investigación</option>
				</select>
				</td>
			</tr>
		</table>
		<br>
		<p>DIAGNÓSTICO DEL EMPRENDEDOR</p>
		<p>Instrucciones: Selecciona la respuesta que más se apegue a tu realidad.</p>
		<p>A.-SI, siempre, B.- A menudo, C.- Poco, alguna vez, D.- No, nunca.</p>
		<table  class="datosModulo1">
			<tr class="titulo">
				<td></td>
				<td>A</td>
				<td>B</td>
				<td>C</td>
				<td>D</td>
			</tr>
			<tr>
				<td>1.- ¿Me interesa mucho todo lo que es nuevo o distinto regularmente?</td>
				<td><input type="radio" name="preguntaDE1" id="preguntaDE1" value="A"></td>
				<td><input type="radio" name="preguntaDE1" id="preguntaDE1" value="B"></td>
				<td><input type="radio" name="preguntaDE1" id="preguntaDE1" value="C"></td>
				<td><input type="radio" name="preguntaDE1" id="preguntaDE1" value="D"></td>
			</tr>			
			<tr>
				<td>2.- ¿Me adapto fácilmente a los cambios?</td>
				<td><input type="radio" name="preguntaDE2" id="preguntaDE2" value="A"></td>
				<td><input type="radio" name="preguntaDE2" id="preguntaDE2" value="B"></td>
				<td><input type="radio" name="preguntaDE2" id="preguntaDE2" value="C"></td>
				<td><input type="radio" name="preguntaDE2" id="preguntaDE2" value="D"></td>
			</tr>			
			<tr>
				<td>3.- ¿Soy entusiasta a la hora de enfrentarme a nuevos retos?</td>
				<td><input type="radio" name="preguntaDE3" id="preguntaDE3" value="A"></td>
				<td><input type="radio" name="preguntaDE3" id="preguntaDE3" value="B"></td>
				<td><input type="radio" name="preguntaDE3" id="preguntaDE3" value="C"></td>
				<td><input type="radio" name="preguntaDE3" id="preguntaDE3" value="D"></td>
			</tr>			
			<tr>
				<td>4.- ¿Me motiva los grandes riesgos?</td>
				<td><input type="radio" name="preguntaDE4" id="preguntaDE4" value="A"></td>
				<td><input type="radio" name="preguntaDE4" id="preguntaDE4" value="B"></td>
				<td><input type="radio" name="preguntaDE4" id="preguntaDE4" value="C"></td>
				<td><input type="radio" name="preguntaDE4" id="preguntaDE4" value="D"></td>
			</tr>			
			<tr>
				<td>5.- ¿Asumo bien los fracasos y saco provecho de ellos?</td>
				<td><input type="radio" name="preguntaDE5" id="preguntaDE5" value="A"></td>
				<td><input type="radio" name="preguntaDE5" id="preguntaDE5" value="B"></td>
				<td><input type="radio" name="preguntaDE5" id="preguntaDE5" value="C"></td>
				<td><input type="radio" name="preguntaDE5" id="preguntaDE5" value="D"></td>
			</tr>			
			<tr>
				<td>6.- ¿Soy metódico(a) y autodisciplinado(a) a la hora de realizar un trabajo?</td>
				<td><input type="radio" name="preguntaDE6" id="preguntaDE6" value="A"></td>
				<td><input type="radio" name="preguntaDE6" id="preguntaDE6" value="B"></td>
				<td><input type="radio" name="preguntaDE6" id="preguntaDE6" value="C"></td>
				<td><input type="radio" name="preguntaDE6" id="preguntaDE6" value="D"></td>
			</tr>			
			<tr>
				<td>7.- ¿Tengo facilidad para comunicarme con otras personas?</td>
				<td><input type="radio" name="preguntaDE7" id="preguntaDE7" value="A"></td>
				<td><input type="radio" name="preguntaDE7" id="preguntaDE7" value="B"></td>
				<td><input type="radio" name="preguntaDE7" id="preguntaDE7" value="C"></td>
				<td><input type="radio" name="preguntaDE7" id="preguntaDE7" value="D"></td>
			</tr>			
			<tr>
				<td>8.- ¿Planifico detalladamente todas las actividades que tengo que realizar?</td>
				<td><input type="radio" name="preguntaDE8" id="preguntaDE8" value="A"></td>
				<td><input type="radio" name="preguntaDE8" id="preguntaDE8" value="B"></td>
				<td><input type="radio" name="preguntaDE8" id="preguntaDE8" value="C"></td>
				<td><input type="radio" name="preguntaDE8" id="preguntaDE8" value="D"></td>
			</tr>			
			<tr>
				<td>9.- ¿Procuro no posponer asuntos y terminar todo a tiempo?</td>
				<td><input type="radio" name="preguntaDE9" id="preguntaDE9" value="A"></td>
				<td><input type="radio" name="preguntaDE9" id="preguntaDE9" value="B"></td>
				<td><input type="radio" name="preguntaDE9" id="preguntaDE9" value="C"></td>
				<td><input type="radio" name="preguntaDE9" id="preguntaDE9" value="D"></td>
			</tr>			
			<tr>
				<td>10.- ¿Me gusta dirigir equipos?</td>
				<td><input type="radio" name="preguntaDE10" id="preguntaDE10" value="A"></td>
				<td><input type="radio" name="preguntaDE10" id="preguntaDE10" value="B"></td>
				<td><input type="radio" name="preguntaDE10" id="preguntaDE10" value="C"></td>
				<td><input type="radio" name="preguntaDE10" id="preguntaDE10" value="D"></td>
			</tr>			
			<tr>
				<td>11.- ¿Me gusta trabajar en equipo?</td>
				<td><input type="radio" name="preguntaDE11" id="preguntaDE11" value="A"></td>
				<td><input type="radio" name="preguntaDE11" id="preguntaDE11" value="B"></td>
				<td><input type="radio" name="preguntaDE11" id="preguntaDE11" value="C"></td>
				<td><input type="radio" name="preguntaDE11" id="preguntaDE11" value="D"></td>
			</tr>			
			<tr>
				<td>12.- Sé administrar mis recursos?</td>
				<td><input type="radio" name="preguntaDE12" id="preguntaDE12" value="A"></td>
				<td><input type="radio" name="preguntaDE12" id="preguntaDE12" value="B"></td>
				<td><input type="radio" name="preguntaDE12" id="preguntaDE12" value="C"></td>
				<td><input type="radio" name="preguntaDE12" id="preguntaDE12" value="D"></td>
			</tr>	
			<tr>
				<td>Total</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>			
		</table>
		<br>
		<p>DIAGNÓSTICO DEL CONOCIMIENTO QUE POSEES ACERCA DE TU NEGOCIO</p>
		<p>Instrucciones: Selecciona la respuesta que más se apegue a tu realidad.</p>
		<table class="datosModulo1">
			<tr  class="titulo">
				<td >MERCADOCTENIA</td>
				<td>Lo tengo claro y definido</td>
				<td>Por definir y/o investigar más</td>
				<td>Aún no lo he investigado</td>
				<td>Desconozco a qué se refiere</td>
			</tr>
			<tr>
				<td>Lista de productos y servicios</td>
				<td><input type="radio" name="preguntaM1" id="preguntaM1" value="A"></td>
				<td><input type="radio" name="preguntaM1" id="preguntaM1" value="B"></td>
				<td><input type="radio" name="preguntaM1" id="preguntaM1" value="C"></td>
				<td><input type="radio" name="preguntaM1" id="preguntaM1" value="D"></td>
			</tr>			
			<tr>
				<td>Segmentación del mercado (geográfico, demográfico y psicográfico)</td>
				<td><input type="radio" name="preguntaM2" id="preguntaM2" value="A"></td>
				<td><input type="radio" name="preguntaM2" id="preguntaM2" value="B"></td>
				<td><input type="radio" name="preguntaM2" id="preguntaM2" value="C"></td>
				<td><input type="radio" name="preguntaM2" id="preguntaM2" value="D"></td>
			</tr>			
			<tr>
				<td>Mercado potencial</td>
				<td><input type="radio" name="preguntaM3" id="preguntaM3" value="A"></td>
				<td><input type="radio" name="preguntaM3" id="preguntaM3" value="B"></td>
				<td><input type="radio" name="preguntaM3" id="preguntaM3" value="C"></td>
				<td><input type="radio" name="preguntaM3" id="preguntaM3" value="D"></td>
			</tr>			
			<tr>
				<td>Mercado meta</td>
				<td><input type="radio" name="preguntaM4" id="preguntaM4" value="A"></td>
				<td><input type="radio" name="preguntaM4" id="preguntaM4" value="B"></td>
				<td><input type="radio" name="preguntaM4" id="preguntaM4" value="C"></td>
				<td><input type="radio" name="preguntaM4" id="preguntaM4" value="D"></td>
			</tr>			
			<tr>
				<td>Identificación de la competencia directa e indirecta</td>
				<td><input type="radio" name="preguntaM5" id="preguntaM5" value="A"></td>
				<td><input type="radio" name="preguntaM5" id="preguntaM5" value="B"></td>
				<td><input type="radio" name="preguntaM5" id="preguntaM5" value="C"></td>
				<td><input type="radio" name="preguntaM5" id="preguntaM5" value="D"></td>
			</tr>			
			<tr>
				<td>Investigación de mercados enfocada a la aceptación del mercado de los productos y/o servicios que pretende ofrecer</td>
				<td><input type="radio" name="preguntaM6" id="preguntaM6" value="A"></td>
				<td><input type="radio" name="preguntaM6" id="preguntaM6" value="B"></td>
				<td><input type="radio" name="preguntaM6" id="preguntaM6" value="C"></td>
				<td><input type="radio" name="preguntaM6" id="preguntaM6" value="D"></td>	
			</tr>			
			<tr>
				<td>Nombre comercial</td>
				<td><input type="radio" name="preguntaM7" id="preguntaM7" value="A"></td>
				<td><input type="radio" name="preguntaM7" id="preguntaM7" value="B"></td>
				<td><input type="radio" name="preguntaM7" id="preguntaM7" value="C"></td>
				<td><input type="radio" name="preguntaM7" id="preguntaM7" value="D"></td>
			</tr>			
			<tr>
				<td>Logotipo registrado en IMPI</td>
				<td><input type="radio" name="preguntaM8" id="preguntaM8" value="A"></td>
				<td><input type="radio" name="preguntaM8" id="preguntaM8" value="B"></td>
				<td><input type="radio" name="preguntaM8" id="preguntaM8" value="C"></td>
				<td><input type="radio" name="preguntaM8" id="preguntaM8" value="D"></td>
			</tr>			
			<tr>
				<td>Ventajas competitivas de mi productos y/o servicios</td>
				<td><input type="radio" name="preguntaM9" id="preguntaM9" value="A"></td>
				<td><input type="radio" name="preguntaM9" id="preguntaM9" value="B"></td>
				<td><input type="radio" name="preguntaM9" id="preguntaM9" value="C"></td>
				<td><input type="radio" name="preguntaM9" id="preguntaM9" value="D"></td>
			</tr>			
			<tr>
				<td>Precios por cada producto y/o servicio</td>
				<td><input type="radio" name="preguntaM10" id="preguntaM10" value="A"></td>
				<td><input type="radio" name="preguntaM10" id="preguntaM10" value="B"></td>
				<td><input type="radio" name="preguntaM10" id="preguntaM10" value="C"></td>
				<td><input type="radio" name="preguntaM10" id="preguntaM10" value="D"></td>
			</tr>			
			<tr>
				<td>Canales de distribución por producto y/o servicio</td>
				<td><input type="radio" name="preguntaM11" id="preguntaM11" value="A"></td>
				<td><input type="radio" name="preguntaM11" id="preguntaM11" value="B"></td>
				<td><input type="radio" name="preguntaM11" id="preguntaM11" value="C"></td>
				<td><input type="radio" name="preguntaM11" id="preguntaM11" value="D"></td>
			</tr>			
			<tr>
				<td>Medios publicitarios de acuerdo al tipo de mercado a atender</td>
				<td><input type="radio" name="preguntaM12" id="preguntaM12" value="A"></td>
				<td><input type="radio" name="preguntaM12" id="preguntaM12" value="B"></td>
				<td><input type="radio" name="preguntaM12" id="preguntaM12" value="C"></td>
				<td><input type="radio" name="preguntaM12" id="preguntaM12" value="D"></td>
			</tr>	
			<tr>
				<td>Promociones de acuerdo a las temporadas de demanda de los productos y/o servicios</td>
				<td><input type="radio" name="preguntaM13" id="preguntaM13" value="A"></td>
				<td><input type="radio" name="preguntaM13" id="preguntaM13" value="B"></td>
				<td><input type="radio" name="preguntaM13" id="preguntaM13" value="C"></td>
				<td><input type="radio" name="preguntaM13" id="preguntaM13" value="D"></td>
			</tr>	
			<tr>
				<td>Pronóstico de ventas mes a mes</td>
				<td><input type="radio" name="preguntaM14" id="preguntaM14" value="A"></td>
				<td><input type="radio" name="preguntaM14" id="preguntaM14" value="B"></td>
				<td><input type="radio" name="preguntaM14" id="preguntaM14" value="C"></td>
				<td><input type="radio" name="preguntaM14" id="preguntaM14" value="D"></td>
			</tr>	
			<tr>
				<td>Registro de marcas</td>
				<td><input type="radio" name="preguntaM15" id="preguntaM15" value="A"></td>
				<td><input type="radio" name="preguntaM15" id="preguntaM15" value="B"></td>
				<td><input type="radio" name="preguntaM15" id="preguntaM15" value="C"></td>
				<td><input type="radio" name="preguntaM15" id="preguntaM15" value="D"></td>
			</tr>	
			<tr>
				<td>Estrategias de venta</td>
				<td><input type="radio" name="preguntaM16" id="preguntaM16" value="A"></td>
				<td><input type="radio" name="preguntaM16" id="preguntaM16" value="B"></td>
				<td><input type="radio" name="preguntaM16" id="preguntaM16" value="C"></td>
				<td><input type="radio" name="preguntaM16" id="preguntaM16" value="D"></td>
			</tr>	
			<tr>
				<td>Controles de venta</td>
				<td><input type="radio" name="preguntaM17" id="preguntaM17" value="A"></td>
				<td><input type="radio" name="preguntaM17" id="preguntaM17" value="B"></td>
				<td><input type="radio" name="preguntaM17" id="preguntaM17" value="C"></td>
				<td><input type="radio" name="preguntaM17" id="preguntaM17" value="D"></td>
			</tr>	
			<tr>
				<td>Gastos de Venta</td>
				<td><input type="radio" name="preguntaM18" id="preguntaM18" value="A"></td>
				<td><input type="radio" name="preguntaM18" id="preguntaM18" value="B"></td>
				<td><input type="radio" name="preguntaM18" id="preguntaM18" value="C"></td>
				<td><input type="radio" name="preguntaM18" id="preguntaM18" value="D"></td>
			</tr>	
			<tr class="titulo">
				<td>ESTUDIO TÉCNICO</td>
				<td>Lo tengo claro y definido</td>
				<td>Por definir y/o investigar más</td>
				<td>Aún no lo he investigado</td>
				<td>Desconozco a qué se refiere</td>
			</tr>
			<tr>
				<td>Proceso paso a paso de lo que conlleva desarrollar tu producto y/u ofrecer tus servicios.</td>
				<td><input type="radio" name="preguntaET1" id="preguntaET1" value="A"></td>
				<td><input type="radio" name="preguntaET1" id="preguntaET1" value="B"></td>
				<td><input type="radio" name="preguntaET1" id="preguntaET1" value="C"></td>
				<td><input type="radio" name="preguntaET1" id="preguntaET1" value="D"></td>
			</tr>	
			<tr>
				<td>Control de inventarios</td>
				<td><input type="radio" name="preguntaET2" id="preguntaET2" value="A"></td>
				<td><input type="radio" name="preguntaET2" id="preguntaET2" value="B"></td>
				<td><input type="radio" name="preguntaET2" id="preguntaET2" value="C"></td>
				<td><input type="radio" name="preguntaET2" id="preguntaET2" value="D"></td>
			</tr>	
			<tr>
				<td>Calidad esperada en el mercado de tus productos y/o servicios</td>
				<td><input type="radio" name="preguntaET3" id="preguntaET3" value="A"></td>
				<td><input type="radio" name="preguntaET3" id="preguntaET3" value="B"></td>
				<td><input type="radio" name="preguntaET3" id="preguntaET3" value="C"></td>
				<td><input type="radio" name="preguntaET3" id="preguntaET3" value="D"></td>
			</tr>	
			<tr>
				<td>Proveedores</td>
				<td><input type="radio" name="preguntaET4" id="preguntaET4" value="A"></td>
				<td><input type="radio" name="preguntaET4" id="preguntaET4" value="B"></td>
				<td><input type="radio" name="preguntaET4" id="preguntaET4" value="C"></td>
				<td><input type="radio" name="preguntaET4" id="preguntaET4" value="D"></td>
			</tr>	
			<tr>
				<td>Criterios de selección de proveedores</td>
				<td><input type="radio" name="preguntaET5" id="preguntaET5" value="A"></td>
				<td><input type="radio" name="preguntaET5" id="preguntaET5" value="B"></td>
				<td><input type="radio" name="preguntaET5" id="preguntaET5" value="C"></td>
				<td><input type="radio" name="preguntaET5" id="preguntaET5" value="D"></td>
			</tr>	
			<tr>
				<td>Tecnología requerida</td>
				<td><input type="radio" name="preguntaET6" id="preguntaET6" value="A"></td>
				<td><input type="radio" name="preguntaET6" id="preguntaET6" value="B"></td>
				<td><input type="radio" name="preguntaET6" id="preguntaET6" value="C"></td>
				<td><input type="radio" name="preguntaET6" id="preguntaET6" value="D"></td>
			</tr>	
			<tr>
				<td>Software básico, intermedio ó avanzado requerido en el negocio</td>
				<td><input type="radio" name="preguntaET7" id="preguntaET7" value="A"></td>
				<td><input type="radio" name="preguntaET7" id="preguntaET7" value="B"></td>
				<td><input type="radio" name="preguntaET7" id="preguntaET7" value="C"></td>
				<td><input type="radio" name="preguntaET7" id="preguntaET7" value="D"></td>
			</tr>	
			<tr>
				<td>Medidas de seguridad industrial</td>
				<td><input type="radio" name="preguntaET8" id="preguntaET8" value="A"></td>
				<td><input type="radio" name="preguntaET8" id="preguntaET8" value="B"></td>
				<td><input type="radio" name="preguntaET8" id="preguntaET8" value="C"></td>
				<td><input type="radio" name="preguntaET8" id="preguntaET8" value="D"></td>
			</tr>	
			<tr>
				<td>Características de las instalaciones a requerir en el negocio</td>
				<td><input type="radio" name="preguntaET9" id="preguntaET9" value="A"></td>
				<td><input type="radio" name="preguntaET9" id="preguntaET9" value="B"></td>
				<td><input type="radio" name="preguntaET9" id="preguntaET9" value="C"></td>
				<td><input type="radio" name="preguntaET9" id="preguntaET9" value="D"></td>
			</tr>	
			<tr>
				<td>Registro de patentes, modelos de utilidad ó diseños industriales</td>
				<td><input type="radio" name="preguntaET10" id="preguntaET10" value="A"></td>
				<td><input type="radio" name="preguntaET10" id="preguntaET10" value="B"></td>
				<td><input type="radio" name="preguntaET10" id="preguntaET10" value="C"></td>
				<td><input type="radio" name="preguntaET10" id="preguntaET10" value="D"></td>
			</tr>	
			<tr>
				<td>Control de tiempos y movimientos</td>
				<td><input type="radio" name="preguntaET11" id="preguntaET11" value="A"></td>
				<td><input type="radio" name="preguntaET11" id="preguntaET11" value="B"></td>
				<td><input type="radio" name="preguntaET11" id="preguntaET11" value="C"></td>
				<td><input type="radio" name="preguntaET11" id="preguntaET11" value="D"></td>
			</tr>	
			<tr>
				<td>Costos unitarios</td>
				<td><input type="radio" name="preguntaET12" id="preguntaET12" value="A"></td>
				<td><input type="radio" name="preguntaET12" id="preguntaET12" value="B"></td>
				<td><input type="radio" name="preguntaET12" id="preguntaET12" value="C"></td>
				<td><input type="radio" name="preguntaET12" id="preguntaET12" value="D"></td>
			</tr>	
			<tr>
				<td>Avances tecnológicos del giro</td>
				<td><input type="radio" name="preguntaET13" id="preguntaET13" value="A"></td>
				<td><input type="radio" name="preguntaET13" id="preguntaET13" value="B"></td>
				<td><input type="radio" name="preguntaET13" id="preguntaET13" value="C"></td>
				<td><input type="radio" name="preguntaET13" id="preguntaET13" value="D"></td>
			</tr>	
			<tr>
				<td>Creación de un comité de innovación permanente</td>
				<td><input type="radio" name="preguntaET14" id="preguntaET14" value="A"></td>
				<td><input type="radio" name="preguntaET14" id="preguntaET14" value="B"></td>
				<td><input type="radio" name="preguntaET14" id="preguntaET14" value="C"></td>
				<td><input type="radio" name="preguntaET14" id="preguntaET14" value="D"></td>
			</tr>
			<tr class="titulo">
				<td>ESTUDIO ADMINISTRATIVO</td>
				<td>Lo tengo claro y definido</td>
				<td>Por definir y/o investigar más</td>
				<td>Aún no lo he investigado</td>
				<td>Desconozco a qué se refiere</td>
			</tr>
			<tr>
				<td>Figura jurídica para iniciar operaciones</td>
				<td><input type="radio" name="preguntaEA1" id="preguntaEA1" value="A"></td>
				<td><input type="radio" name="preguntaEA1" id="preguntaEA1" value="B"></td>
				<td><input type="radio" name="preguntaEA1" id="preguntaEA1" value="C"></td>
				<td><input type="radio" name="preguntaEA1" id="preguntaEA1" value="D"></td>
			</tr>	
			<tr>
				<td>Derechos y obligaciones que tendría de acuerdo a mi figura jurídica</td>
				<td><input type="radio" name="preguntaEA2" id="preguntaEA2" value="A"></td>
				<td><input type="radio" name="preguntaEA2" id="preguntaEA2" value="B"></td>
				<td><input type="radio" name="preguntaEA2" id="preguntaEA2" value="C"></td>
				<td><input type="radio" name="preguntaEA2" id="preguntaEA2" value="D"></td>
			</tr>	
			<tr>
				<td>Misión</td>
				<td><input type="radio" name="preguntaEA3" id="preguntaEA3" value="A"></td>
				<td><input type="radio" name="preguntaEA3" id="preguntaEA3" value="B"></td>
				<td><input type="radio" name="preguntaEA3" id="preguntaEA3" value="C"></td>
				<td><input type="radio" name="preguntaEA3" id="preguntaEA3" value="D"></td>
			</tr>	
			<tr>
				<td>Visión</td>
				<td><input type="radio" name="preguntaEA4" id="preguntaEA4" value="A"></td>
				<td><input type="radio" name="preguntaEA4" id="preguntaEA4" value="B"></td>
				<td><input type="radio" name="preguntaEA4" id="preguntaEA4" value="C"></td>
				<td><input type="radio" name="preguntaEA4" id="preguntaEA4" value="D"></td>
			</tr>	
			<tr>
				<td>Objetivos para el corto, mediano y largo plazo</td>
				<td><input type="radio" name="preguntaEA5" id="preguntaEA5" value="A"></td>
				<td><input type="radio" name="preguntaEA5" id="preguntaEA5" value="B"></td>
				<td><input type="radio" name="preguntaEA5" id="preguntaEA5" value="C"></td>
				<td><input type="radio" name="preguntaEA5" id="preguntaEA5" value="D"></td>
			</tr>	
			<tr>
				<td>Organigrama o estructura organizacional</td>
				<td><input type="radio" name="preguntaEA6" id="preguntaEA6" value="A"></td>
				<td><input type="radio" name="preguntaEA6" id="preguntaEA6" value="B"></td>
				<td><input type="radio" name="preguntaEA6" id="preguntaEA6" value="C"></td>
				<td><input type="radio" name="preguntaEA6" id="preguntaEA6" value="D"></td>
			</tr>	
			<tr>
				<td>Definición de roles y funciones por cada puesto</td>
				<td><input type="radio" name="preguntaEA7" id="preguntaEA7" value="A"></td>
				<td><input type="radio" name="preguntaEA7" id="preguntaEA7" value="B"></td>
				<td><input type="radio" name="preguntaEA7" id="preguntaEA7" value="C"></td>
				<td><input type="radio" name="preguntaEA7" id="preguntaEA7" value="D"></td>
			</tr>	
			<tr>
				<td>Gastos de administración</td>
				<td><input type="radio" name="preguntaEA8" id="preguntaEA8" value="A"></td>
				<td><input type="radio" name="preguntaEA8" id="preguntaEA8" value="B"></td>
				<td><input type="radio" name="preguntaEA8" id="preguntaEA8" value="C"></td>
				<td><input type="radio" name="preguntaEA8" id="preguntaEA8" value="D"></td>
			</tr>	
			<tr>
				<td>Sueldos y salarios por puesto</td>
				<td><input type="radio" name="preguntaEA9" id="preguntaEA9" value="A"></td>
				<td><input type="radio" name="preguntaEA9" id="preguntaEA9" value="B"></td>
				<td><input type="radio" name="preguntaEA9" id="preguntaEA9" value="C"></td>
				<td><input type="radio" name="preguntaEA9" id="preguntaEA9" value="D"></td>
			</tr>	
			<tr>
				<td>Reglamento interno</td>
				<td><input type="radio" name="preguntaEA10" id="preguntaEA10" value="A"></td>
				<td><input type="radio" name="preguntaEA10" id="preguntaEA10" value="B"></td>
				<td><input type="radio" name="preguntaEA10" id="preguntaEA10" value="C"></td>
				<td><input type="radio" name="preguntaEA10" id="preguntaEA10" value="D"></td>
			</tr>	
			<tr>
				<td>Manual de políticas y procedimientos</td>
				<td><input type="radio" name="preguntaEA11" id="preguntaEA11" value="A"></td>
				<td><input type="radio" name="preguntaEA11" id="preguntaEA11" value="B"></td>
				<td><input type="radio" name="preguntaEA11" id="preguntaEA11" value="C"></td>
				<td><input type="radio" name="preguntaEA11" id="preguntaEA11" value="D"></td>
			</tr>	
			<tr>
				<td>Programa anual de capacitación</td>
				<td><input type="radio" name="preguntaEA12" id="preguntaEA12" value="A"></td>
				<td><input type="radio" name="preguntaEA12" id="preguntaEA12" value="B"></td>
				<td><input type="radio" name="preguntaEA12" id="preguntaEA12" value="C"></td>
				<td><input type="radio" name="preguntaEA12" id="preguntaEA12" value="D"></td>
			</tr>	
			<tr>
				<td>Manual de organización</td>
				<td><input type="radio" name="preguntaEA13" id="preguntaEA13" value="A"></td>
				<td><input type="radio" name="preguntaEA13" id="preguntaEA13" value="B"></td>
				<td><input type="radio" name="preguntaEA13" id="preguntaEA13" value="C"></td>
				<td><input type="radio" name="preguntaEA13" id="preguntaEA13" value="D"></td>
			</tr>	
			<tr>
				<td>Contratos para los proveedores</td>
				<td><input type="radio" name="preguntaEA14" id="preguntaEA14" value="A"></td>
				<td><input type="radio" name="preguntaEA14" id="preguntaEA14" value="B"></td>
				<td><input type="radio" name="preguntaEA14" id="preguntaEA14" value="C"></td>
				<td><input type="radio" name="preguntaEA14" id="preguntaEA14" value="D"></td>
			</tr>	
			<tr>
				<td>Contratos para los empleados</td>
				<td><input type="radio" name="preguntaEA15" id="preguntaEA15" value="A"></td>
				<td><input type="radio" name="preguntaEA15" id="preguntaEA15" value="B"></td>
				<td><input type="radio" name="preguntaEA15" id="preguntaEA15" value="C"></td>
				<td><input type="radio" name="preguntaEA15" id="preguntaEA15" value="D"></td>
			</tr>	
			<tr>
				<td>Contratos de los accionistas o socios</td>
				<td><input type="radio" name="preguntaEA16" id="preguntaEA16" value="A"></td>
				<td><input type="radio" name="preguntaEA16" id="preguntaEA16" value="B"></td>
				<td><input type="radio" name="preguntaEA16" id="preguntaEA16" value="C"></td>
				<td><input type="radio" name="preguntaEA16" id="preguntaEA16" value="D"></td>
			</tr>	
			<tr>
				<td>Trámites a realizar para iniciar operaciones</td>
				<td><input type="radio" name="preguntaEA17" id="preguntaEA17" value="A"></td>
				<td><input type="radio" name="preguntaEA17" id="preguntaEA17" value="B"></td>
				<td><input type="radio" name="preguntaEA17" id="preguntaEA17" value="C"></td>
				<td><input type="radio" name="preguntaEA17" id="preguntaEA17" value="D"></td>
			</tr>	
			<tr>
				<td>Estrategias de cobranza y controles de cobranza</td>
				<td><input type="radio" name="preguntaEA18" id="preguntaEA18" value="A"></td>
				<td><input type="radio" name="preguntaEA18" id="preguntaEA18" value="B"></td>
				<td><input type="radio" name="preguntaEA18" id="preguntaEA18" value="C"></td>
				<td><input type="radio" name="preguntaEA18" id="preguntaEA18" value="D"></td>
			</tr>	
			<tr>
				<td>Instituciones financieras o gubernamentales de donde podría obtener financiamiento.</td>
				<td><input type="radio" name="preguntaEA19" id="preguntaEA19" value="A"></td>
				<td><input type="radio" name="preguntaEA19" id="preguntaEA19" value="B"></td>
				<td><input type="radio" name="preguntaEA19" id="preguntaEA19" value="C"></td>
				<td><input type="radio" name="preguntaEA19" id="preguntaEA19" value="D"></td>
			</tr>	
			<tr>
				<td>Controles administrativos</td>
				<td><input type="radio" name="preguntaEA20" id="preguntaEA20" value="A"></td>
				<td><input type="radio" name="preguntaEA20" id="preguntaEA20" value="B"></td>
				<td><input type="radio" name="preguntaEA20" id="preguntaEA20" value="C"></td>
				<td><input type="radio" name="preguntaEA20" id="preguntaEA20" value="D"></td>
			</tr>	
			<tr>
				<td>Programas de trabajo</td>
				<td><input type="radio" name="preguntaEA21" id="preguntaEA21" value="A"></td>
				<td><input type="radio" name="preguntaEA21" id="preguntaEA21" value="B"></td>
				<td><input type="radio" name="preguntaEA21" id="preguntaEA21" value="C"></td>
				<td><input type="radio" name="preguntaEA21" id="preguntaEA21" value="D"></td>
			</tr>
			<tr class="titulo">
				<td>ESTUDIO FINANCIERO</td>
				<td>Lo tengo claro y definido</td>
				<td>Por definir y/o investigar más</td>
				<td>Aún no lo he investigado</td>
				<td>Desconozco a qué se refiere</td>
			</tr>
			<tr>
				<td>Rentabilidad del negocio</td>
				<td><input type="radio" name="preguntaEF1" id="preguntaEF1" value="A"></td>
				<td><input type="radio" name="preguntaEF1" id="preguntaEF1" value="B"></td>
				<td><input type="radio" name="preguntaEF1" id="preguntaEF1" value="C"></td>
				<td><input type="radio" name="preguntaEF1" id="preguntaEF1" value="D"></td>
			</tr>	
			<tr>
				<td>Costo-beneficio del negocio</td>
				<td><input type="radio" name="preguntaEF2" id="preguntaEF2" value="A"></td>
				<td><input type="radio" name="preguntaEF2" id="preguntaEF2" value="B"></td>
				<td><input type="radio" name="preguntaEF2" id="preguntaEF2" value="C"></td>
				<td><input type="radio" name="preguntaEF2" id="preguntaEF2" value="D"></td>
			</tr>	
			<tr>
				<td>Punto de equilibrio</td>
				<td><input type="radio" name="preguntaEF3" id="preguntaEF3" value="A"></td>
				<td><input type="radio" name="preguntaEF3" id="preguntaEF3" value="B"></td>
				<td><input type="radio" name="preguntaEF3" id="preguntaEF3" value="C"></td>
				<td><input type="radio" name="preguntaEF3" id="preguntaEF3" value="D"></td>
			</tr>	
			<tr>
				<td>Interpretación de estados financieros</td>
				<td><input type="radio" name="preguntaEF4" id="preguntaEF4" value="A"></td>
				<td><input type="radio" name="preguntaEF4" id="preguntaEF4" value="B"></td>
				<td><input type="radio" name="preguntaEF4" id="preguntaEF4" value="C"></td>
				<td><input type="radio" name="preguntaEF4" id="preguntaEF4" value="D"></td>
			</tr>	
			<tr>
				<td>Gastos financieros</td>
				<td><input type="radio" name="preguntaEF5" id="preguntaEF5" value="A"></td>
				<td><input type="radio" name="preguntaEF5" id="preguntaEF5" value="B"></td>
				<td><input type="radio" name="preguntaEF5" id="preguntaEF5" value="C"></td>
				<td><input type="radio" name="preguntaEF5" id="preguntaEF5" value="D"></td>
			</tr>	
			<tr>
				<td>Impuestos a pagar de acuerdo a mi figura jurídica</td>
				<td><input type="radio" name="preguntaEF6" id="preguntaEF6" value="A"></td>
				<td><input type="radio" name="preguntaEF6" id="preguntaEF6" value="B"></td>
				<td><input type="radio" name="preguntaEF6" id="preguntaEF6" value="C"></td>
				<td><input type="radio" name="preguntaEF6" id="preguntaEF6" value="D"></td>
			</tr>	
			<tr>
				<td>Beneficios del IMSS</td>
				<td><input type="radio" name="preguntaEF7" id="preguntaEF7" value="A"></td>
				<td><input type="radio" name="preguntaEF7" id="preguntaEF7" value="B"></td>
				<td><input type="radio" name="preguntaEF7" id="preguntaEF7" value="C"></td>
				<td><input type="radio" name="preguntaEF7" id="preguntaEF7" value="D"></td>
			</tr>	
			<tr>
				<td>Beneficios al darse de alta en el SAT</td>
				<td><input type="radio" name="preguntaEF8" id="preguntaEF8" value="A"></td>
				<td><input type="radio" name="preguntaEF8" id="preguntaEF8" value="B"></td>
				<td><input type="radio" name="preguntaEF8" id="preguntaEF8" value="C"></td>
				<td><input type="radio" name="preguntaEF8" id="preguntaEF8" value="D"></td>
			</tr>	
			<tr>
				<td>Tendencias financieras de mi giro de negocio</td>
				<td><input type="radio" name="preguntaEF9" id="preguntaEF9" value="A"></td>
				<td><input type="radio" name="preguntaEF9" id="preguntaEF9" value="B"></td>
				<td><input type="radio" name="preguntaEF9" id="preguntaEF9" value="C"></td>
				<td><input type="radio" name="preguntaEF9" id="preguntaEF9" value="D"></td>
			</tr>	
			<tr>
				<td>Controles financieros</td>
				<td><input type="radio" name="preguntaEF10" id="preguntaEF10" value="A"></td>
				<td><input type="radio" name="preguntaEF10" id="preguntaEF10" value="B"></td>
				<td><input type="radio" name="preguntaEF10" id="preguntaEF10" value="C"></td>
				<td><input type="radio" name="preguntaEF10" id="preguntaEF10" value="D"></td>
			</tr>	
			<tr>
				<td>Total</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>			
		</table>
		<br>	
		<button id="aceptar">Aceptar</button>

	</form>

<?php 

}

?>