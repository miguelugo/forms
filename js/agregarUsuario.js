$(document).ready(function() {
	
	$("#btnAgregarUsuario").click(function(event) {
	
		//event.preventDefault();

		jQuery.validator.setDefaults({

			debug: true,
			success: "valid"

		});

		$("#clienteNuevo").validate({

						rules: {

							nombreCompleto: {required: true, minlength: 1},
							nickUsuario: {required: true, minlength: 1},
							correoUsuario: {required: true, email: true},
							password1: {required: true},
							password2: {equalTo: "#password1"}

						},

						messages: {

							nombreCompleto: "Debe introducir un nombre.",
							nickUsuario: "Debe introducir un nick.",
							correoUsuario: "Debe introducir un correo.",
							password1: "Debe introducir una cotraseña.",
							password2: "Contraseñas diferentes."

						},

						submitHandler: function(form){

							var nombreCompleto = $("#nombreCompleto").val();
							var nickUsuario = $("#nickUsuario").val();
							var correoUsuario = $("#correoUsuario").val();
							var grupoUsuario = $("#grupoUsuario").val();
							var etiquetaUsuario = $("#grupoUsuario option:selected").html();
							var password1 = $("#password1").val();

							$("#dialogo2").dialog("destroy");
							$("#dialogo2").html("");

								$.ajax({		

									  		url:"php/altaUsuario.php", //URL a la que se envían los datos del formulario
									    	cache:false,
									    	type:"POST",

									    	data:{
									    		nombreCompleto:nombreCompleto,
									    		nickUsuario:nickUsuario,
									    		correoUsuario:correoUsuario,
									    		grupoUsuario:grupoUsuario,
									    		etiquetaUsuario:etiquetaUsuario,
									    		password1:password1
									    	},//envio de datos como objeto

									    	success:function(result){ //lo que se hace si funciona...

									    		//location.reload();

									    		 $( "#respuesta" ).html(result).dialog({
									    		 										title: 'Nuevo Usuario',
									    		 										modal:false,
								                                                        height: 'auto',
								                                                        width: 'auto'
								                                                      
								                                                        
												}).dialog('open');

											}
									});
						}

		 	});


	});

	$("#btnCancelarUsuario").click(function(event) {

		event.preventDefault();

		$("#dialogo2").dialog("destroy");
		$("#dialogo2").html("");

	});

});