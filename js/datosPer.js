$(document).ready(function() {
	
	$("#aceptar").click(function(event) {

		jQuery.validator.setDefaults({

				debug: true,
				 success: "valid"
		});



		$("#datosPersonales").validate({

			rules: {
				tipoBene: {required: true},
				primerApellido: {required: true},
				segundoApellido: {required: true},
				nombre: {required: true},
				fechaNac: {required: true},
				sexo: {required: true},
				estadoCivil: {required: true},
				estudios: {required: true},
				identificacion: {required: true},
				numeroIdentificacion: {required: true},
				nacionalidad: {required: true},
				entidadNac: {required: true},
				curp: {required: true},
				calle: {required: true},
				numExt: {required: true, number: true},
				calle1: {required: true},
				calle2: {required: true},
				otraRef: {required: true},
				colonia: {required: true},
				localidad: {required: true},
				municipio: {required: true},
				entidad: {required: true},
				cp: {required: true, number: true},
				telFijo: {required: true, number: true},
				telCel: {required: true, number: true},
				correo: {required: true, email: true},
				taller: {required: true},
				condiciones: {required: true},
				grupos: {required: true}
			},
			messages: {
				tipoBene: "Debe ingresar un tipo de benefactor.",
				primerApellido: "Debe ingresar un apellido.",
				segundoApellido: "Debe ingresar un apellido.",
				nombre: "Debe ingresar un nombre.",
				fechaNac: "Debe ingresar una fecha de nacimiento.",
				sexo: "Debe elegir una opcion.",
				estadoCivil: "Debe elegir una opcion.",
				estudios: "Debe elegir una opcion.",
				identificacion: "Debe elegir una opcion.",
				numeroIdentificacion: "Debe ingresar un identificador.",
				nacionalidad: "Debe ingresar una nacionalidad.",
				entidadNac: "Debe elegir una opcion.",
				curp: "Debe ingresar un CURP.",
				calle: "Debe ingresar una calle.",
				numExt: {
					required: "Debe ingresar un número exterior.",
					number: "Solo se permite números."
				},
				calle1: "Debe ingresar referencia 1.",
				calle2: "Debe ingresar referencia 2.",
				otraRef: "Debe ingresar otra referencia.",
				colonia: "Debe ingresar una colonia.",
				localidad: "Debe ingresar una localidad.",
				municipio: "Debe ingresar un municipio.",
				entidad: "Debe elegir una opcion.",
				cp: {
					required: "Debe ingresar un código postal.",
					number: "Solo se permite números."
				},
				telFijo: {
					required: "Debe ingresar un teléfono fijo.",
					number: "Solo se permite números."
				},
				telCel: {
					required: "Debe ingresar un teléfono celular.",
					number: "Solo se permite números."
				},
				correo: {
					required: "Debe ingresar un correo electrónico.",
					email: "Debe ingresar un correo electrónico valido."
				},
				taller: "Debe elegir una opcion.",
				condiciones: "Debe aceptar las condiciones.",
				gruposNumero: "Debe elegir una opcion."
			},
			submitHandler: function(form){

				var tipoBene             = $("#tipoBene").val();
				var primerApellido       = $("#primerApellido").val();
				var segundoApellido      = $("#segundoApellido").val();
				var nombre               = $("#nombre").val();
				var fechaNac             = $("#fechaNac").val();
				var sexo                 = $("#sexo").val();
				var estadoCivil          = $("#estadoCivil").val();
				var estudios             = $("#estudios").val();
				var identificacion       = $("#identificacion").val();
				var numeroIdentificacion = $("#numeroIdentificacion").val();
				var nacionalidad         = $("#nacionalidad").val();
				var entidadNac           = $("#entidadNac").val();
				var curp                 = $("#curp").val();
				var calle                = $("#calle").val();
				var numExt               = $("#numExt").val();
				var numInt               = $("#numInt").val();
				var calle1               = $("#calle1").val();
				var calle2               = $("#calle2").val();
				var otraRef              = $("#otraRef").val();
				var colonia              = $("#colonia").val();
				var localidad            = $("#localidad").val();
				var municipio            = $("#municipio").val();
				var entidad              = $("#entidad").val();
				var cp                   = $("#cp").val();
				var telFijo              = $("#telFijo").val();
				var telCel               = $("#telCel").val();
				var correo               = $("#correo").val();
				var taller               = $("#taller").val();
				var grupos               = $("#gruposNumero").val();

				console.log(curp);
				console.log(correo);

					$("#confirmacion").dialog({ modal:true,
										open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
										height: "auto",
										width: 700, 
										title: "Confirmacion",
										buttons: {
										    "Aceptar": function() {
																		    
												$.ajax({

														url:"./modulo1.php", //URL a la que se envían los datos del formulario
														cache:false,
														type:"POST",
												    			
														data:{	//datos a enviar en formato como_se_obtuvo:como_se_envia
															tipoBene:tipoBene,
															primerApellido:primerApellido,
															segundoApellido:segundoApellido,
															nombre:nombre,
															fechaNac:fechaNac,
															sexo:sexo,
															estadoCivil:estadoCivil,
															estudios:estudios,
															identificacion:identificacion,
															numeroIdentificacion:numeroIdentificacion,
															nacionalidad:nacionalidad,
															entidadNac:entidadNac,
															curp:curp,
															calle:calle,
															numExt:numExt,
															numInt:numInt,
															calle1:calle1,
															calle2:calle2,
															otraRef:otraRef,
															colonia:colonia,
															localidad:localidad,
															municipio:municipio,
															entidad:entidad,
															cp:cp,
															telFijo:telFijo,
															telCel:telCel,
															correo:correo,
															taller:taller,
															grupos:grupos
										 				},

														success:function(result){ //lo que se hace si funciona...
															
																// $( this ).dialog( "destroy" );
																$("body").html(result);

														}
													});
										},
										    "Cancel": function() {
                                                            
                        						$( this ).dialog( "destroy" );

							                }//cancel
              							}//buttons
           			}).dialog('open');


			}

		});


	});

});