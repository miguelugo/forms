$(document).ready(function() {
	
	$("#aceptar").click(function(event) {

		jQuery.validator.setDefaults({

				debug: true,
				 success: "valid"
		});

		$("#modulo1").validate({

			rules: {
				nombreProyecto: {required: true},
				giro: {required: true},
				preguntaDE1: {required: true},
				preguntaDE2: {required: true},
				preguntaDE3: {required: true},
				preguntaDE4: {required: true},
				preguntaDE5: {required: true},
				preguntaDE6: {required: true},
				preguntaDE7: {required: true},
				preguntaDE8: {required: true},
				preguntaDE9: {required: true},
				preguntaDE10: {required: true},
				preguntaDE11: {required: true},
				preguntaDE12: {required: true},
				preguntaM1: {required: true},
				preguntaM2: {required: true},
				preguntaM3: {required: true},
				preguntaM4: {required: true},
				preguntaM5: {required: true},
				preguntaM6: {required: true},
				preguntaM7: {required: true},
				preguntaM8: {required: true},
				preguntaM9: {required: true},
				preguntaM10: {required: true},
				preguntaM11: {required: true},
				preguntaM12: {required: true},
				preguntaM13: {required: true},
				preguntaM14: {required: true},
				preguntaM15: {required: true},
				preguntaM16: {required: true},
				preguntaM17: {required: true},
				preguntaM18: {required: true},
				preguntaET1: {required: true},
				preguntaET2: {required: true},
				preguntaET3: {required: true},
				preguntaET4: {required: true},
				preguntaET5: {required: true},
				preguntaET6: {required: true},
				preguntaET7: {required: true},
				preguntaET8: {required: true},
				preguntaET9: {required: true},
				preguntaET10: {required: true},
				preguntaET11: {required: true},
				preguntaET12: {required: true},
				preguntaET13: {required: true},
				preguntaET14: {required: true},
				preguntaEA1: {required: true},
				preguntaEA2: {required: true},
				preguntaEA3: {required: true},
				preguntaEA4: {required: true},
				preguntaEA5: {required: true},
				preguntaEA6: {required: true},
				preguntaEA7: {required: true},
				preguntaEA8: {required: true},
				preguntaEA9: {required: true},
				preguntaEA10: {required: true},
				preguntaEA11: {required: true},
				preguntaEA12: {required: true},
				preguntaEA13: {required: true},
				preguntaEA14: {required: true},
				preguntaEA15: {required: true},
				preguntaEA16: {required: true},
				preguntaEA17: {required: true},
				preguntaEA18: {required: true},
				preguntaEA19: {required: true},
				preguntaEF1: {required: true},
				preguntaEF2: {required: true},
				preguntaEF3: {required: true},
				preguntaEF4: {required: true},
				preguntaEF5: {required: true},
				preguntaEF6: {required: true},
				preguntaEF7: {required: true},
				preguntaEF8: {required: true},
				preguntaEF9: {required: true},
				preguntaEF10: {required: true}			
			},
			messages: {
				nombreProyecto: "Debe ingresar un nombre de proyecto",
				giro: "Debe elegir una opcion",
				preguntaDE1: "Debe elegir una opcion",
				preguntaDE2: "Debe elegir una opcion",
				preguntaDE3: "Debe elegir una opcion",
				preguntaDE4: "Debe elegir una opcion",
				preguntaDE5: "Debe elegir una opcion",
				preguntaDE6: "Debe elegir una opcion",
				preguntaDE7: "Debe elegir una opcion",
				preguntaDE8: "Debe elegir una opcion",
				preguntaDE9: "Debe elegir una opcion",
				preguntaDE10: "Debe elegir una opcion",
				preguntaDE11: "Debe elegir una opcion",
				preguntaDE12: "Debe elegir una opcion",
				preguntaM1: "Debe elegir una opcion",
				preguntaM2: "Debe elegir una opcion",
				preguntaM3: "Debe elegir una opcion",
				preguntaM4: "Debe elegir una opcion",
				preguntaM5: "Debe elegir una opcion",
				preguntaM6: "Debe elegir una opcion",
				preguntaM7: "Debe elegir una opcion",
				preguntaM8: "Debe elegir una opcion",
				preguntaM9: "Debe elegir una opcion",
				preguntaM10: "Debe elegir una opcion",
				preguntaM11: "Debe elegir una opcion",
				preguntaM12: "Debe elegir una opcion",
				preguntaM13: "Debe elegir una opcion",
				preguntaM14: "Debe elegir una opcion",
				preguntaM15: "Debe elegir una opcion",
				preguntaM16: "Debe elegir una opcion",
				preguntaM17: "Debe elegir una opcion",
				preguntaM18: "Debe elegir una opcion",
				preguntaET1: "Debe elegir una opcion",
				preguntaET2: "Debe elegir una opcion",
				preguntaET3: "Debe elegir una opcion",
				preguntaET4: "Debe elegir una opcion",
				preguntaET5: "Debe elegir una opcion",
				preguntaET6: "Debe elegir una opcion",
				preguntaET7: "Debe elegir una opcion",
				preguntaET8: "Debe elegir una opcion",
				preguntaET9: "Debe elegir una opcion",
				preguntaET10: "Debe elegir una opcion",
				preguntaET11: "Debe elegir una opcion",
				preguntaET12: "Debe elegir una opcion",
				preguntaET13: "Debe elegir una opcion",
				preguntaET14: "Debe elegir una opcion",
				preguntaEA1: "Debe elegir una opcion",
				preguntaEA2: "Debe elegir una opcion",
				preguntaEA3: "Debe elegir una opcion",
				preguntaEA4: "Debe elegir una opcion",
				preguntaEA5: "Debe elegir una opcion",
				preguntaEA6: "Debe elegir una opcion",
				preguntaEA7: "Debe elegir una opcion",
				preguntaEA8: "Debe elegir una opcion",
				preguntaEA9: "Debe elegir una opcion",
				preguntaEA10: "Debe elegir una opcion",
				preguntaEA11: "Debe elegir una opcion",
				preguntaEA12: "Debe elegir una opcion",
				preguntaEA13: "Debe elegir una opcion",
				preguntaEA14: "Debe elegir una opcion",
				preguntaEA15: "Debe elegir una opcion",
				preguntaEA16: "Debe elegir una opcion",
				preguntaEA17: "Debe elegir una opcion",
				preguntaEA18: "Debe elegir una opcion",
				preguntaEA19: "Debe elegir una opcion",
				preguntaEF1: "Debe elegir una opcion",
				preguntaEF2: "Debe elegir una opcion",
				preguntaEF3: "Debe elegir una opcion",
				preguntaEF4: "Debe elegir una opcion",
				preguntaEF5: "Debe elegir una opcion",
				preguntaEF6: "Debe elegir una opcion",
				preguntaEF7: "Debe elegir una opcion",
				preguntaEF8: "Debe elegir una opcion",
				preguntaEF9: "Debe elegir una opcion",
				preguntaEF10: "Debe elegir una opcion"
			},
			submitHandler: function(form){

				var formulario = $("#modulo1").serialize();

				$.ajax({

						url:"./finRegistro.php", //URL a la que se envían los datos del formulario
						cache:false,
						type:"POST",
				    			
						data:{	//datos a enviar en formato como_se_obtuvo:como_se_envia
							formulario:formulario
		 				},

						success:function(result){ //lo que se hace si funciona...
							$("body").html(result);

						}
				});


			}

		});

	});

});