 $(document).ready(function() {


 	$("table tbody tr").dblclick(function(event) {
		
 			idEmprendedor = $(this).attr("idEmprendedor");
 			
      $("tbody tr").css('background-color', '');
      $("tr#tr"+idEmprendedor).css('background-color', '#F0EB84');

       $.ajax({

            url:"./formEditar.php", //URL a la que se envían los datos del formulario
            cache:false,
            type:"POST",
                  
            data:{  //datos a enviar en formato como_se_obtuvo:como_se_envia
                  idEmprendedor:idEmprendedor
            },

            success:function(result){ //lo que se hace si funciona...
               // $("#Recibos").html(result);
                  $( "#respuesta" ).html(result).dialog({ modal:true,
                                                            height: 'auto',
                                                            width: 'auto', 
                                                            buttons: {
                                                                "Editar": function() {
                                                                    
                                                                    $( this ).dialog( "destroy" );
                                                                    var datos = $("#editar").serialize();

                                                                    $.ajax({
                                                                      url:"./updateEmprendedor.php", //URL a la que se envían los datos del formulario
                                                                      cache:false,
                                                                      type:"POST",
                                                                            
                                                                      data:{  //datos a enviar en formato como_se_obtuvo:como_se_envia
                                                                          
                                                                        datos:datos,
                                                                        idEmprendedor:idEmprendedor

                                                                      },

                                                                      success:function(result){ //lo que se hace si funciona...

                                                                        $("#respuesta").html(result).dialog({ modal:true,
                                                                            height: 'auto',
                                                                            width: 'auto', 
                                                                            buttons: {
                                                                                "Aceptar": function() {
                                                                                    
                                                                                    $( this ).dialog( "destroy" );
                                                                                    location.reload();
                                                                                }
                                                                            }
                                                                          }).dialog('open');;
                                                                       
                                                                      }
                                                                });//ajax

                                                                                              
                                                                },
                                                                "Cancelar": function(){

                                                                      $( this ).dialog( "destroy" );

                                                                }
                                                            }
                                                          }).dialog('open');;
            }

      });//ajax

  });

});
