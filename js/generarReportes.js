$(document).ready(function() {
	
	$("#generarReportes").click(function() {

		$("#resultado").html("Generando Reportes");

		$.ajax({

			url:"./reporte/excel.php", //URL a la que se envían los datos del formulario
			cache:false,
			type:"POST",
			success:function(result){ //lo que se hace si funciona...
				
				$(result).appendTo('#resultado');
				
					$.ajax({

						url:"./reporte/modulosExcel.php", //URL a la que se envían los datos del formulario
						cache:false,
						type:"POST",
						success:function(result){ //lo que se hace si funciona...
							
							$(result).appendTo('#resultado');

							$.ajax({

								url:"./reporte/comprimir.php", //URL a la que se envían los datos del formulario
								cache:false,
								type:"POST",
								success:function(result){ //lo que se hace si funciona...
									
									$(result).appendTo('#resultado');
									
								}
							});
							
						}
					});

			}
		});

	});

});	