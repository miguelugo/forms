$(document).ready(function() {
	
	$("#enviar").click(function(event) {
	
		jQuery.validator.setDefaults({

				debug: true,
				 success: "valid"
		});

		$("#modulos").validate({

			rules: {
				// modulo2: {required: true},
				// modulo3: {required: true},
				// modulo4: {required: true},
				// modulo5_1: {required: true},
				// modulo5_2: {required: true},
				// modulo5_3: {required: true},
				// modulo5_4: {required: true},
				// modulo5_5: {required: true},
				// modulo5_6: {required: true},
				// modulo5_7: {required: true},
				// modulo5_8: {required: true},
				// modulo6_1: {required: true},
				// modulo6_2: {required: true},
				// modulo7_1: {required: true},
				// modulo7_2: {required: true},
			},
			messages: {
				// modulo2: "Este campo no puede quedar vacio.",
				// modulo3: "Este campo no puede quedar vacio.",
				// modulo4: "Este campo no puede quedar vacio.",
				// modulo5_1: "Este campo no puede quedar vacio.",
				// modulo5_2: "Este campo no puede quedar vacioo.",
				// modulo5_3: "Este campo no puede quedar vacio",
				// modulo5_4: "Este campo no puede quedar vacio.",
				// modulo5_5: "Este campo no puede quedar vacio.",
				// modulo5_6: "Este campo no puede quedar vacio.",
				// modulo5_7: "Este campo no puede quedar vacio.",
				// modulo5_8: "Este campo no puede quedar vacio.",
				// modulo6_1: "Este campo no puede quedar vacio.",
				// modulo6_2: "Debe elegir una opcion.",
				// modulo7_1: "Este campo no puede quedar vacio.",
				// modulo7_2: "Este campo no puede quedar vacio."
			},
			submitHandler: function(form){

				var idEmprendedor = $("#idEmprendedor").val();
				var modulo2       = $("#modulo2").val();
				var modulo3       = $("#modulo3").val();
				var modulo4       = $("#modulo4").val();
				var modulo5_1     = $("#modulo5_1").val();
				var modulo5_2     = $("#modulo5_2").val();
				var modulo5_3     = $("#modulo5_3").val();
				var modulo5_4     = $("#modulo5_4").val();
				var modulo5_5     = $("#modulo5_5").val();
				var modulo5_6     = $("#modulo5_6").val();
				var modulo5_7     = $("#modulo5_7").val();
				var modulo5_8     = $("#modulo5_8").val();
				var modulo6_1     = $("#modulo6_1").val();
				var modulo6_2     = $("#modulo6_2").val();
				var modulo7_1     = $("#modulo7_1").val();
				var modulo7_2     = $("#modulo7_2").val();

				var data = new FormData();

				var archivo = document.getElementById('plan_financiero');
				var archivos = archivo.files;

				for(i=0; i<=archivos.length; i++){
					data.append('plan_financiero',archivos[i]);
				}

				data.append('idEmprendedor', idEmprendedor);
				data.append('modulo2', modulo2);
				data.append('modulo3', modulo3);
				data.append('modulo4', modulo4);
				data.append('modulo5_1', modulo5_1);
				data.append('modulo5_2', modulo5_2);
				data.append('modulo5_3', modulo5_3);
				data.append('modulo5_4', modulo5_4);
				data.append('modulo5_5', modulo5_5);
				data.append('modulo5_6', modulo5_6);
				data.append('modulo5_7', modulo5_7);
				data.append('modulo5_8', modulo5_8);
				data.append('modulo6_1', modulo6_1);
				data.append('modulo6_2', modulo6_2);
				data.append('modulo7_1', modulo7_1);
				data.append('modulo7_2', modulo7_2);

				$.ajax({

						url:"./modulosInsert.php", //URL a la que se envían los datos del formulario
						cache:false,
						type:"POST",
				    	contentType:false,
						processData:false,	
						data:data,/*{	//datos a enviar en formato como_se_obtuvo:como_se_envia
								},*/

						success:function(result){ //lo que se hace si funciona...
							$("#contenido").html(result);
						}
					});

			}

		});

	});

});	
