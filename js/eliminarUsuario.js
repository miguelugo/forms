$(document).ready(function() {
	
	nombre = "";
	idEmprendedor = "";

 	$("table.datosModulo1 tbody tr").click(function(event) {

		idEmprendedor = $(this).attr("idEmprendedor");
	 	nombre = $(this).attr("nombre");

	 	$("tbody tr").css('background-color', '');
	 	$("tr#tr"+idEmprendedor).css('background-color', '#F04F4F');

	 });

 	$("#eliminarUsuario").click(function(event) {
 		
 		if (nombre != "") {
 			
	 		$("#nickElimnar").html(nombre);

	 		$("#eliminar").dialog({ modal:true,
	 								title:"Eliminar Usuario",
	                  				open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
									height: "auto",
	                                width: "auto",
	                                buttons: {
		                                
		                                "Elimiar": function() {

			                                	$("#eliminar").dialog("destroy");

													$.ajax({		

												  		url:"./eliminarUsuario.php", //URL a la que se envían los datos del formulario
												    	cache:false,
												    	type:"POST",

												    	data:{
												    		nombre:nombre,
												    		idEmprendedor:idEmprendedor,

												    	},//envio de datos como objeto

												    	success:function(result){ //lo que se hace si funciona...

												    		 $( "#respuesta" ).html(result).dialog({ modal:false,
												    		 										open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
											                                                        height: 200,
											                                                        width: 300, 
											                                                        buttons: {
											                                                            "OK": function() {
											                      
											                                                                $( this ).dialog( "destroy" );
												    		 												location.reload();
											                      
											                                                            }
											                                                        }
															}).dialog('open');

														}
													});

		                                },//button editar

		                                "Cancelar": function() {
		                                    
		                                      // $("#password").val("");
		                                      // $("#password2").val("");
		                                      // $("#error").html("");
		                                    $(this).dialog("destroy");

		                                }

		                            }//buttons

	        });

 		} else{

 			$("#errorElimiar").dialog({
 				modal:true,
				title:"Error",
				open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
				height: "auto",
                width: "auto",
                buttons: {
                    
                    "Aceptar": function() {

                    	$(this).dialog("destroy");

                    }
                 }
 			});
 			//alert("Debe eligir un usuario");

 		}

 	});

});