<?php 

/**
 * Incluimos la conexion
 */
include '../conexion.php';

require_once("../modulos/sesion/include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("../modulos/index.php");
    exit;
}

$sql = "SELECT * FROM emprendendores";
$query = $conexion->prepare($sql);
$query->execute();
$row = $query->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Panel Administración</title>
	<link rel="stylesheet" href="../css/estilos.css">
	<script src="../js/jquery-1.9.1.min.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<script src="../js/jquery.dataTables.js"></script>
	
	<script src="../js/eliminarUsuario.js"></script>
	<script src="../js/editarUsuario.js"></script>
	<script src="../js/generarRespaldo.js"></script>
	<script src="../js/generarReportes.js"></script>

	<link rel="stylesheet" type="text/css" href="../css/demo_page.css">
    <link rel="stylesheet" type="text/css" href="../css/demo_table.css">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">

	 <script type="text/javascript">

      	$(document).ready(function() {

			$(".datosModulo1").dataTable({
				'iDisplayLength': 15
			});
		} );

      </script>

		<style>
			ul{
				list-style: none;
				display: flex;
				margin: auto;
				width: 50%;
				cursor: pointer;
			}

			ul li{
				text-align: center;
				padding-left: 15px;
				padding-right: 15px;
			}

			#resultado{
				margin: auto;
				width: 50%;
				text-align: center;
				font-size: 18px;
				font-weight: bold;
				color: #000;
			}

		</style>

</head>
<body>

	<header>
			
		<img src="http://corporativogaia.com.mx/gaiav2/wp-content/uploads/2014/05/logo1.png" alt="">

		<p style="padding-top: 40px;">PANEL DE ADMINISTRACIÓN</p>

	</header>

<ul>
	<li>
		<p><a id="generarReportes">Generar Reportes</a></p>
	</li>
	<li>
		<p><a id="generarRespaldo">Generar Respaldo</a></p>
	</li>
	<li>
		<p><a id="eliminarUsuario">Eliminar Usuario</a></p>
	</li>
	<li>
		<p><a href='../modulos/sesion/salir.php'>Salir</a></p>
	</li>
</ul>

<div id="resultado"></div>


<table class="datosModulo1" cellpadding="0" cellspacing="0" border="0" class="display">
	<thead>
		<tr class="titulo">
			<th>Tipo Benefactor</th>
			<th>Nombre Completo</th>
			<th>Domicilio</th>
			<th>Num. Ext.</th>
			<th>Num. Int.</th>
			<th>Colonia</th>
			<th>Localidad</th>
			<th>Municipio</th>
			<th>Entidad</th>
			<th>C.P.</th>
			<th>Telefono Fijo</th>
			<th>Telefono Celular</th>
			<th>Correo</th>
			<th>Contraseña</th>
			<th>Taller</th>
			<th>Grupo</th>
		</tr>
	</thead>
	<tbody>

	<?php

	foreach ($row as $row) {
	?>
		<tr idEmprendedor="<?php echo $row[0] ?>" class="tr" id="tr<?php echo $row[0] ?>" nombre="<?php echo $row[4]." ".$row[2]." ".$row[3] ?>">
			<td ><?php echo $row[1] ?></td>
			<td><?php echo $row[4]." ".$row[2]." ".$row[3] ?></td>
			<td><?php echo $row[14] ?></td>
			<td><?php echo $row[15] ?></td>
			<td><?php echo $row[16] ?></td>
			<td><?php echo $row[20] ?></td>
			<td><?php echo $row[21] ?></td>
			<td><?php echo $row[22] ?></td>
			<td><?php echo $row[23] ?></td>
			<td><?php echo $row[24] ?></td>
			<td><?php echo $row[25] ?></td>
			<td><?php echo $row[26] ?></td>
			<td><?php echo $row[27] ?></td>
			<td><?php echo $row[30] ?></td>
			<td><?php echo $row[28] ?></td>
			<td><?php echo $row[29] ?></td>
			
		</tr>
	<?php 
	}
	 ?>

	</tbody>
</table>

<div id="dialog" style="display:none">
	
	<form>
		<input type="password" id="password" placeholder="Password">
		<br>
		<input type="password" id="password2" placeholder="Confirmar Password">

		<div id="error" style="color:red; font-size: 0.7em; font-style: italic;">
		</div>
	</form>

</div>

<div id="eliminar" style="display:none; text-align:center">
	
	<p>
	Desea eliminar al emprendedor :

			<a id="nickElimnar"></a>

	</p>
	
</div>
<div id="errorElimiar" style="display:none">
	
	<p>Debe elegir un usuario</p>

</div>

<div id="dialogo2" style="display:none"></div>
<div id="respuesta" style="display:none"></div>
<div id="respuesta1" ></div>


</body>
</html>