<?php 

require_once("../include/conexion.php");

$qry = "SELECT * FROM usuarios";

$query = $conexion->prepare($qry);

$query->execute();

$row = $query->fetchAll();

?>

      <script type="text/javascript">

      	$(document).ready(function() {

			$("#example1").dataTable({
				'iDisplayLength': 20
			});
		} );

      </script>
	
<table id="example1" cellpadding="0" cellspacing="0" border="0" class="display"  >
	
	<thead>
		<th width="20%">Nick</th>
		<th width="25%">Nombre</th>
		<th width="25%">Apellidos</th>
		<th width="15%">Email</th>
	</thead>

	<tbody>
		
		<?php 

			foreach ($row as $row) {
		?>
		
		<tr class="tr" id="tr<?php echo $row['id_usuario'] ?>" value="<?php echo $row['id_usuario'] ?>" nick="<?php echo $row['nick'] ?>">
			<td><?php echo $row['nick'] ?></td>
			<td><?php echo utf8_encode($row['nombre']) ?></td>
			<td><?php echo utf8_encode($row['apellidos']) ?></td>
			<td><?php echo $row['email'] ?></td>
		</tr>

		<?php 

			}

		?>

	</tbody>

</table>


<div id="error" style="display:none; text-align:center">
	
	<p>
	
	Debe elegir un usuario primero

	</p>
	
</div>