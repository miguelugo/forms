<?php 

/**
 * Incluimos la conexion
 */
require_once("../conexion.php");

$idEmprendedor = $_POST['idEmprendedor'];

/**
 * Seleccionamos todos los datos 
 */
$sql = "SELECT * FROM emprendendores WHERE idEmprendendores = $idEmprendedor";

$query = $conexion->prepare($sql);
$query->execute();
$row = $query->fetchAll();

foreach ($row as $row) {

?>
<form id="editar" accept-charset="UTF-8">

<table class="datos" >
		<tr>
			<td>
				<a>Tipo de beneficiario:</a>
			</td>
			<td>
				<input type="text" id="tipoBene" name="tipoBene" value="Emprendedor" value="<?php echo $row['tipoBene'] ?>">
			</td>
		</tr>
		<tr>
			<td>
				<a>Primer Apellido:</a>
			</td>
			<td>
				<input type="text" id="primerApellido" name="primerApellido" value="<?php echo $row['primerApellido'] ?>">
			</td>
		</tr>
		<tr>
			<td>
				<a>Segundo Apellido:</a>
			</td>
			<td>
				<input type="text" id="segundoApellido" name="segundoApellido" value="<?php echo $row['segundoApellido'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Nombre (s):</a>
			</td>
			<td>
				<input type="text" id="nombre" name="nombre" value="<?php echo $row['nombre'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Fecha de Nacimiento <span style="font-weight: bold;">(dd/mm/YYYY)</span>:</a>
			</td>
			<td>
				<input type="date" id="fechaNac" name="fechaNac" value="<?php echo $row['fechaNac'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Sexo:</a>
			</td>
			<td>	
				<script>
					$(document).ready(function() {
						$("#sexo option[value='<?php echo $row['sexo'] ?>']").attr("selected",true);
					});
				</script>
				<select name="sexo" id="sexo">
					<option value="">Seleccione un opción</option>
					<option value="F">Femenino</option>
					<option value="M">Masculino</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Estado Civil:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#estadoCivil option[value='<?php echo $row['estadoCivil'] ?>']").attr("selected",true);
					});
				</script>
				<select name="estadoCivil" id="estadoCivil">
					<option value="">Seleccione un opción</option>
					<option value="Soltero">Soltero</option>
					<option value="Casado">Casado</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Grado de estudios:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#estudios option[value='<?php echo $row['estudios'] ?>']").attr("selected",true);
					});
				</script>
				<select name="estudios" id="estudios">
					<option value="">Seleccione un opción</option>
					<option value="Primaria">Primaria</option>
					<option value="Secundaria">Secundaria</option>
					<option value="Bachillerato">Bachillerato</option>
					<option value="Universidad">Universidad</option>
					<option value="Posgrado">Posgrado</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Tipo de identificación oficial:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#identificacion option[value='<?php echo $row['identificacion'] ?>']").attr("selected",true);
					});
				</script>
				<select name="identificacion" id="identificacion">
					<option value="">Seleccione un opción</option>
					<option value="IFE">IFE</option>
					<option value="Pasaporte">Pasaporte</option>
					<option value="Credencial escolar">Credencial escolar</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Identificador del documento oficial (número):</a>
			</td>
			<td>
				<input type="text" id="numeroIdentificacion" name="numeroIdentificacion" value="<?php echo $row['numIdentificacion'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Nacionalidad:</a>
			</td>
			<td>
				<input type="text" id="nacionalidad" name="nacionalidad" value="Mexicana" value="<?php echo $row['nacionalidad'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entidad de Nacimiento:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#entidadNac option[value='<?php echo $row['entidadNac'] ?>']").attr("selected",true);
					});
				</script>
				<select name="entidadNac" id="entidadNac">
					<option value="">Seleccione un opción</option>
					<option value="Aguascalientes">Aguascalientes</option>
					<option value="Baja California">Baja California</option>
					<option value="Baja California Sur">Baja California Sur</option>
					<option value="Campeche">Campeche</option>
					<option value="Chiapas">Chiapas</option>
					<option value="Chihuahua">Chihuahua</option>
					<option value="Coahuila">Coahuila</option>
					<option value="Colima">Colima</option>
					<option value="Distrito Federal">Distrito Federal</option>
					<option value="Durango">Durango</option>
					<option value="Estado de México">Estado de México</option>
					<option value="Guanajuato">Guanajuato</option>
					<option value="Guerrero">Guerrero</option>
					<option value="Hidalgo">Hidalgo</option>
					<option value="Jalisco">Jalisco</option>
					<option value="Michoacán">Michoacán</option>
					<option value="Morelos">Morelos</option>
					<option value="Nayarit">Nayarit</option>
					<option value="Nuevo León">Nuevo León</option>
					<option value="Oaxaca">Oaxaca</option>
					<option value="Puebla">Puebla</option>
					<option value="Querétaro">Querétaro</option>
					<option value="Quintana Roo">Quintana Roo</option>
					<option value="San Luis Potosí">San Luis Potosí</option>
					<option value="Sinaloa">Sinaloa</option>
					<option value="Sonora">Sonora</option>
					<option value="Tabasco">Tabasco</option>
					<option value="Tamaulipas">Tamaulipas</option>
					<option value="Veracruz">Veracruz</option>
					<option value="Yucatán">Yucatán</option>
					<option value="Zacatecas">Zacatecas</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Clave Única de Registro de Población (CURP):</a>
			</td>
			<td>
				<input type="text" id="curp" name="curp" value="<?php echo $row['curp'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Calle (domicilio):</a>
			</td>
			<td>
				<input type="text" id="calle" name="calle" value="<?php echo $row['domicilio'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Número exterior:</a>
			</td>
			<td>
				<input type="text" id="numExt" name="numExt" value="<?php echo $row['numExt'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Número interior:</a>
			</td>
			<td>
				<input type="text" id="numInt" name="numInt" value="<?php echo $row['numInt'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entre la calle:</a>
			</td>
			<td>
				<input type="text" id="calle1" name="calle1" value="<?php echo $row['calle1'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Y la calle:</a>
			</td>
			<td>
				<input type="text" id="calle2" name="calle2" value="<?php echo $row['calle2'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Otra referencia:</a>
			</td>
			<td>
				<input type="text" id="otraRef" name="otraRef" value="No contesto" value="<?php echo $row['otraRef'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Colonia:</a>
			</td>
			<td>
				<input type="text" id="colonia" name="colonia" value="<?php echo $row['colonia'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Localidad:</a>
			</td>
			<td>
				<input type="text" id="localidad" name="localidad" value="<?php echo $row['localidad'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Municipio:</a>
			</td>
			<td>
				<input type="text" id="municipio" name="municipio" value="<?php echo $row['municipio'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Entidad:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#entidad option[value='<?php echo $row['entidad'] ?>']").attr("selected",true);
					});
				</script>
				<select name="entidad" id="entidad">
					<option value="">Seleccione un opción</option>
					<option value="Aguascalientes">Aguascalientes</option>
					<option value="Baja California">Baja California</option>
					<option value="Baja California Sur">Baja California Sur</option>
					<option value="Campeche">Campeche</option>
					<option value="Chiapas">Chiapas</option>
					<option value="Chihuahua">Chihuahua</option>
					<option value="Coahuila">Coahuila</option>
					<option value="Colima">Colima</option>
					<option value="Distrito Federal">Distrito Federal</option>
					<option value="Durango">Durango</option>
					<option value="Estado de México">Estado de México</option>
					<option value="Guanajuato">Guanajuato</option>
					<option value="Guerrero">Guerrero</option>
					<option value="Hidalgo">Hidalgo</option>
					<option value="Jalisco">Jalisco</option>
					<option value="Michoacán">Michoacán</option>
					<option value="Morelos">Morelos</option>
					<option value="Nayarit">Nayarit</option>
					<option value="Nuevo León">Nuevo León</option>
					<option value="Oaxaca">Oaxaca</option>
					<option value="Puebla">Puebla</option>
					<option value="Querétaro">Querétaro</option>
					<option value="Quintana Roo">Quintana Roo</option>
					<option value="San Luis Potosí">San Luis Potosí</option>
					<option value="Sinaloa">Sinaloa</option>
					<option value="Sonora">Sonora</option>
					<option value="Tabasco">Tabasco</option>
					<option value="Tamaulipas">Tamaulipas</option>
					<option value="Veracruz">Veracruz</option>
					<option value="Yucatán">Yucatán</option>
					<option value="Zacatecas">Zacatecas</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Código Postal:</a>
			</td>
			<td>
				<input type="text" id="cp" name="cp" value="<?php echo $row['cp'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Teléfono fijo con lada:</a>
			</td>
			<td>
				<input type="text" id="telFijo" name="telFijo" value="<?php echo $row['telFijo'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Teléfono celular:</a>
			</td>
			<td>
				<input type="text" id="telCel" name="telCel" value="<?php echo $row['telCel'] ?>">
			</td>
		</tr>		
		<tr>
			<td>
				<a>Correo electrónico:</a>
			</td>
			<td>
				<input type="text" id="correo" name="correo" value="<?php echo $row['correo'] ?>">
			</td>
		</tr>		
			<tr>
			<td>
				<a>Taller:</a>
			</td>
			<td>
				<script>
					$(document).ready(function() {
						$("#taller option[value='<?php echo $row['taller'] ?>']").attr("selected",true);
					});
				</script>
				<select name="taller" id="taller">
					<option value="">Seleccione un opción</option>
					<option value="TESE Emprendiendo Con éxito">TESE Emprendiendo con éxito</option>
					<option value="Emprendiendo En El CBT">Emprendiendo en el CBT</option>
					<option value="Tultitlán Emprende">Tultitlán Emprende</option>
					<option value="Innova y Emprende">Innova y Emprende</option>
					<option value="Ecatepec Emprende">Ecatepec Emprende</option>
				</select>
			</td>
		</tr>		
		<tr>
			<td>
				<a>Grupo:</a>
			</td>
			<td>
				<div id="grupos"></div>
			</td>
		</tr>		
	</table>
</form>

<?php 

}

?>