<?php 
require_once("../include/conexion.php");

$qry = "SELECT name, id FROM jos_core_acl_aro_groups WHERE parent_id = 28 AND lft = 3 AND rgt=1 ORDER BY name ";

$query = $conexion->prepare($qry);

$query->execute();

$row = $query->fetchAll();

?>

<script type="text/javascript">
	
	$(document).ready(function() {
		
		$("#btnAgregarUsuario").button();
		$("#btnCancelarUsuario").button();

	});	

</script>

<script type="text/javascript" src="js/agregarUsuario.js"></script>
<script type="text/javascript" src="js/jquery-validate.js"></script>

<style type="text/css">
	
	.error{
		color:red; 
		font-size: 0.7em; 
		font-style: italic;
	}

</style>

<div style="width: 233.9px; text-align:center">
	
	<form id="clienteNuevo" method="post">
		
		<input type="text" id="nombreCompleto" name="nombreCompleto" placeholder="Nombre Completo">
		<br>
		<input type="text" id="nickUsuario" name="nickUsuario" placeholder="Nick">
		<br>
		<input type="text" id="correoUsuario" name="correoUsuario" placeholder="Correo Electronico">
		<br>
		<select id="grupoUsuario" name="grupoUsuario">
			<?php 
			
			foreach ($row as $row) {
			
			?>
			<option value="<?php echo $row['id'] ?>" etiqueta="<?php echo $row['name'] ?>"><?php echo $row['name'] ?></option>
			<?php 

			}

			?>
		</select>
		<br>
		<input type="password" id="password1" name="password1" placeholder="Contraseña">
		<br>
		<input type="password" id="password2" name="password2" placeholder="Repetir Contraseña">
		<br>
		<button id="btnAgregarUsuario">Agregar</button>
		<button id="btnCancelarUsuario">Cancelar</button>

	</form>

</div>