<?php 

/** Include path **/
// ini_set('include_path',ini_get('include_path').PATH_SEPARATOR.'../private/utilities/PHPExcel/Classes/');

 /** PHPExcel */
 require_once '../libs/Classes/PHPExcel.php';

 /** PHPExcel_Writer_PDF */
 require_once '../libs/Classes/PHPExcel/IOFactory.php';
 require_once '../libs/Classes/PHPExcel/Writer/PDF/tcPDF.php';

// // Create new PHPExcel object
// $objPHPExcel = new PHPExcel();

// $objReader = PHPExcel_IOFactory::createReader('Excel2007');
// $objPHPExcel = $objReader->load("./METADATOS.xlsx");

// // Set properties
// $objPHPExcel->getProperties()->setCreator('Maarten Balliauw');
// $objPHPExcel->getProperties()->setLastModifiedBy('Maarten Balliauw');
// $objPHPExcel->getProperties()->setTitle('Office 2007 XLSX Test Document');
// $objPHPExcel->getProperties()->setSubject('Office 2007 XLSX Test Document');
// $objPHPExcel->getProperties()->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.');

// // redirect output to client browser
// header('Content-Type: application/pdf');
// header('Connection: Close');
// header('Content-Disposition: attachment;filename="test.pdf"');
// //header('Content-Disposition: inline');
// //header('Content-Length: 499096');
// //header('Accept-Ranges: bytes');

// //output a PDF file
// $objWriter = new PHPExcel_Writer_PDF($objPHPExcel);
// $objWriter->setTempDir('./');
// $objWriter->save('test.pdf');


$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
echo $rendererName."<br><br>";
$rendererLibrary = 'tcPDF';
$rendererLibraryPath = dirname(__FILE__).'/libs/Classes/PHPExcel/Writer/' .
$rendererLibrary;
if (!PHPExcel_Settings::setPdfRenderer(
$rendererName,
$rendererLibraryPath
)) {
die(
'Please set the '.$rendererName.' and '.$rendererLibraryPath.' values' .
PHP_EOL .
' as appropriate for your directory structure'
);
}


?>