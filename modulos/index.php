<?PHP
require_once("./sesion/include/membersite_config.php");

if(isset($_POST['submitted']))
{
   if($fgmembersite->Login())
   {
      if ($fgmembersite->tipo() == 0) {
          $fgmembersite->RedirectToURL("./modulos.php");
      } else {
          $fgmembersite->RedirectToURL("../panelAdmin/panelAdmin.php");
      }
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Acceso</title>
      <link rel="STYLESHEET" type="text/css" href="../css/estilo_login.css" />
      <script type='text/javascript' src='sesion/scripts/gen_validatorv31.js'></script>
</head>
<body>

<!-- Form Code Start -->


      <div id='fg_membersite'>

          <div class="logo">
              
              <img src="http://corporativogaia.com.mx/gaiav2/wp-content/uploads/2014/05/logo1.png" alt="">

          </div>
          <form id='login' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
          
          <fieldset >

                <input type='hidden' name='submitted' id='submitted' value='1'/>

                <label>LOGIN</label>
              
                <div><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span></div>
                <div class='container'>
                        <input type='text' name='username' id='username' value='<?php echo $fgmembersite->SafeDisplay('username') ?>' maxlength="50" placeholder="Usuario" /><br/>
                    <span id='login_username_errorloc' class='error'></span>
                </div>
                <div class='container'>
                       <input type='password' name='password' id='password' maxlength="50" placeholder="Contraseña"/><br/>
                    <span id='login_password_errorloc' class='error'></span>
                </div>

                <button id="iniciar" name='Submit'>Iniciar</button>

                <div class='short_explanation1'><a href='sesion/recuperar_password.php'>Olvidaste tu contraseña?</a></div>
          
          </fieldset>
          
          </form>
  
          <script type='text/javascript'>
          // <![CDATA[

              var frmvalidator  = new Validator("login");
              frmvalidator.EnableOnPageErrorDisplay();
              frmvalidator.EnableMsgsTogether();

              frmvalidator.addValidation("username","req","Por favor indícanos tu usuario");
              
              frmvalidator.addValidation("password","req","Por favor indícanos tu contraseña");

          // ]]>
          </script>

<!--           <div class="persona">
            <img src="../imagenes/img02.png" alt="">
          </div> -->

      </div>

<div id="sombra">
</div>


</body>
</html>