<?PHP
require_once("./include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("acceso.php");
    exit;
}

if(isset($_POST['submitted']))
{
   if($fgmembersite->ChangePassword())
   {

         if ($fgmembersite->nivel() != 0) {

            $fgmembersite->RedirectToURL("../inicio.php");

          } else {
            
            $fgmembersite->RedirectToURL("../usuario/homeUsuario.php");

          }
   }
}

?>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>

    <link rel="stylesheet" href="../css/estilos.css" media="all"  type="text/css">
    <link rel="STYLESHEET" type="text/css" href="style/fg_membersite_registro.css" />
    <link rel="STYLESHEET" type="text/css" href="sesion/style/pwdwidget.css" />

    <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
    <script type="text/javascript" src="scripts/pwdwidget.js"></script>

 <body>
      <header>
      
                
      </header>

         <section>

            <article>

              <?php

              if ($fgmembersite->nivel() != 0)

              {
            
                  include('../include/listaZonas.php');

              }

              ?>

            </article>

            <aside>

                <div id= "buscador">

                <?php 


                  if ($fgmembersite->nivel() != 0)

                  {

                ?>

                          <div id="abec">
                          A - B - C - D - E - F - G - H - I - J - K - L - M - N - O <br> P - Q - R - S - T - U - V - W - X - Y - Z
                          </div><!--fin div abec-->

                          <div id="inputBuscador">

                            <form id="buscadorZona">

                               <input id="zonas" name="nombre"/>
                               <input type="hidden" id="idZona" name="idZonas" />

                            </form>

                          </div><!--fin div inputBucador-->

                    <?php 

                  }

                    ?>

                </div> <!--fin div buscador-->



                        <div id="contenido">

                              <div class="unidades">

                        
                              </div>   

                              <div id="informacionEdificio">
                                
                                    <br>
                                    <br>
                                    <!-- Form Code Start -->
                                    <div id='fg_membersite'>

                                            <form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

                                                    <input type='hidden' name='submitted' id='submitted' value='1'/>


                                                    <div><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span></div>
                                                    <div class='container' style="height: 70px;">
                                                        <label for='oldpwd' >Contraseña anterior:</label><br/>
                                                        <div class='pwdwidgetdiv' id='oldpwddiv' ></div>
                                                        <noscript>
                                                        <input type='password' name='oldpwd' id='oldpwd' maxlength="50" placeholder="Contraseña anterior"/>
                                                        </noscript>    
                                                        <div id='changepwd_oldpwd_errorloc' class='error'></div>
                                                    </div>

                                                    <div class='container'>
                                                        <label for='newpwd' >Contraseña nueva:</label><br/>
                                                        <div class='pwdwidgetdiv' id='newpwddiv' style="height: 80px;"></div>
                                                        <noscript>
                                                        <input type='password' name='newpwd' id='newpwd' maxlength="50" /><br/>
                                                        </noscript>
                                                        <div id='changepwd_newpwd_errorloc' class='error'></div>
                                                    </div>


                                                    <div class='container'>
                                                        <button type='submit' name='Submit'>Cambiar</button>
                                                    </div>

                                            </form>

                                            <script type='text/javascript'>
                                            // <![CDATA[
                                                var pwdwidget = new PasswordWidget('oldpwddiv','oldpwd');
                                                pwdwidget.enableGenerate = false;
                                                pwdwidget.enableShowStrength=false;
                                                pwdwidget.enableShowStrengthStr =false;
                                                pwdwidget.MakePWDWidget();
                                                
                                                var pwdwidget = new PasswordWidget('newpwddiv','newpwd');
                                                pwdwidget.MakePWDWidget();
                                                
                                                
                                                var frmvalidator  = new Validator("changepwd");
                                                frmvalidator.EnableOnPageErrorDisplay();
                                                frmvalidator.EnableMsgsTogether();

                                                frmvalidator.addValidation("oldpwd","req","Introduce tu contraseña actual");
                                                
                                                frmvalidator.addValidation("newpwd","req","Introduce tu contraseña nueva");

                                            // ]]>
                                            </script>

                                    </div>
      
                              </div>
                        </div>

                           <div id= "pie">

                                      <?php 

                                      if(isset($_SESSION['debug'])) {

                                        echo $sql;

                                        }        

                                      ?>

                                    <div id="insert"></div>
                                    <div id="update"></div>
                           </div>

               
            </aside>

         </section>

   </body>