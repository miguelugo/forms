<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Soliitud de recuperación de contraseña</title>
      <link rel="STYLESHEET" type="text/css" href="../../css/estilo_login.css" />
      <script type='text/javascript' src='../../js/jquery-1.9.1.min.js'></script>
      <script type='text/javascript' src='../../js/enviarCorreo.js'></script>
      
      <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
</head>
<body>
<!-- Form Code Start -->
<div id='fg_membersite'>
<fieldset >

<input type='hidden' name='submitted' id='submitted' value='1'/>

<div class='container'>
        <input type='text' name='email' id='email'  maxlength="50" placeholder="Correo Electronico"/><br/>
    <span id='resetreq_email_errorloc' class='error'></span>
</div>
<div class='container' style="height: 10px">
    <button id="Submit" name='Submit'>Enviar</button>
</div>
<div class='short_explanation'>Enviaremos un vínculo a tu correo para que recuperes tu contraseña.</div>

</fieldset>


</div>


</body>
</html>

