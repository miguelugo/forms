<?PHP
require_once("./include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("acceso.php");
    exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Inicio</title>
      <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css">
</head>
<body>
<div id='fg_membersite_content'>
<h2>Inicio</h2>
Bienvenid@ <?= $fgmembersite->UserFullName(); ?> <?= $fgmembersite->nivel(); ?>!

<p><a href='cambiar_password.php'>Cambiar contraseña</a></p>

<p><a href='ejemplo.php'>Ejemplo</a></p><br>
<p><a href='registro.php'>Alta usuario</a></p>
<br><br><br>
<p><a href='salir.php'>Salir</a></p>
</div>
</body>
</html>
