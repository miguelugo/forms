<?PHP

session_start();

//require_once("sesion/include/membersite_config.php");

//include ('../conexion/conexion.php');

//include ('../include/cabezeraRegistro.php');

if(isset($_POST['submitted']))
{
   if($fgmembersite->RegisterUser())
   {
        $fgmembersite->RedirectToURL("../catalogos.php");
   }
}

?>


      <link rel="stylesheet" href="css/estilos.css" media="all"  type="text/css">

    
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery-validate.js"></script>
    <script src="js/additional-methods.js"></script>

    <script src="js/form_banco.js"></script>
    <script src="js/form_comision.js"></script>
    <script src="js/form_empresa.js"></script>
    <script src="js/form_inmobiliaria.js"></script>
    <script src="js/form_lecturista.js"></script>
    <script src="js/form_servicio.js"></script>
    <script src="js/form_vendedores.js"></script>
    <script src="js/form_registro.js"></script>

    <script src="js/cambiarPass.js"></script>

    <script src="js/modoEditar.js"></script>
    <script src="js/modoDebug.js"></script>

    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    
    <link type="text/css" href="css/buscador.css" rel="stylesheet" />

    <link rel="STYLESHEET" type="text/css" href="sesion/style/fg_membersite_registro.css" />
    <link rel="STYLESHEET" type="text/css" href="sesion/style/pwdwidget.css" />
    <script type='text/javascript' src='sesion/scripts/gen_validatorv31.js'></script>
    <script type="text/javascript" src="sesion/scripts/pwdwidget.js"></script>

<body>

<section>
    


<article>
    
    
</article>

<aside>

    
        <div id= "buscador">

            <div id="abec">
                  A - B - C - D - E - F - G - H - I - J - K - L - M - N - O <br> P - Q - R - S - T - U - V - W - X - Y - Z
            </div><!--fin div abec-->

            <div id="inputBuscador">

                <form id="buscadorZona">

                   <input id="zonas" name="nombre"/>
                   <input type="hidden" id="idZona" name="idZonas" />

                </form>

            </div><!--fin div inputBucador-->

        </div>  

    <div id="contenido">
                        
        <div class="unidades">
            <br>

                 <ul id="listaUnidades">

                 <?php
                            $usuario = 'SELECT * FROM usuarios WHERE status=1 AND id_tipo <> 0';

                            $sentenciaUsuario = $conexion ->prepare($usuario);

                            $sentenciaUsuario -> execute();
                 
                            $rows2 = $sentenciaUsuario -> fetchAll();

                            foreach ($rows2 as $rows2) {

                                if ($rows2['id_tipo'] == 1) {
                                    $nivel = "Super Administrador";
                                } elseif ($rows2['id_tipo'] == 2) {
                                    $nivel = "Administrador";
                                } elseif ($rows2['id_tipo'] == 3) {
                                    $nivel = "Capturista";
                                }
                                

                            ?>                

                                <li id = "<?php echo $rows2['id_usuario']; ?>" >


                                
                                        <?php 
                                             if (isset($_SESSION['editar'])) {
                                         ?>
                                                   <div class="editUnidad" id="<?php echo $rows2['id_usuario'];?>"></div>
                                         <?php 
                                             }
                                         ?>

                                    <div id="nombre" idUsuario="<?php echo $rows2['id_usuario']; ?>">
                                        
                                  <a id = "<?php echo $rows2['id_usuario']; ?>" ><?php echo $rows2['usuario'];?></a>

                                    </div>

                                </li>

                                <div id="editarBanco<?php echo $rows2['id_usuario'];?>" style="display: none"></div>
                            <?php
                             }
                            ?>
                </ul>
                                
        </div>  
                    
        <div id="informacionEdificio">
                        
            <div id= "descripcion">

                        <script src="js/mostrar_form_usuario.js"></script>
                        <script src="js/editarUsuario.js"></script>

                        <div id="usuario_nuevo" >

                                <div id='fg_membersite'>
                                <form id='register' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>


                                <input type='hidden' name='submitted' id='submitted' value='1'/>

                                
                                <input type='text'  class='spmhidip' name='<?php echo $fgmembersite->GetSpamTrapInputName(); ?>' />

                                <div><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span></div>
                                <div class='container'>
                                    <input type='text' name='name' id='name' value='<?php echo $fgmembersite->SafeDisplay('name') ?>' maxlength="50" placeholder="Nombre Completo"/><br/>
                                    <div id='register_name_errorloc' class='error'></div>
                                </div>
                                <div class='container'>
                                    <input type='text' name='email' id='email' value='<?php echo $fgmembersite->SafeDisplay('email') ?>' maxlength="50" placeholder="Correo Electronico"/><br/>
                                    <div id='register_email_errorloc' class='error'></div>
                                </div>
                                <div class='container'>
                                    <label for='fechaAlta' >Fecha de Alta:</label><br/>
                                    <input type='date' name='fechaAlta' id='fechaAlta' value='<?php echo $fgmembersite->SafeDisplay('fechaAlta') ?>' maxlength="50" /><br/>
                                    <div id='register_fechaAlta_errorloc' class='error'></div>
                                </div>
                                <div class='container'>
                                    <label for='nivel' >Nivel:</label><br/>
                                    <select id="nivel" name="nivel">
                                        <option  value='1'>Super Administrador</option>
                                            <option  value='2'>Administrador</option>
                                            <option  value='3'>Capturista</option>
                                            <option  value='4'>Capturista (CC)</option>
                                            <option  value='5'>Operaciones</option>
                                    </select><br>
                                    <span id='register_nivel_errorloc' class='error'></span>
                                </div>
                                <div class='container' style='height:80px;'>
                                    <label for='password' >Contraseña:</label><br/>
                                    <div class='pwdwidgetdiv' id='thepwddiv' ></div>
                                    <div id='register_password_errorloc' class='error' style='clear:both'></div>
                                    <noscript>
                                    <input type='password' name='password' id='password' maxlength="50" />
                                    </noscript>    
                                </div>
                                <div class='container'>
                                    <input type='radio' name='status' id='status' value='1' checked/><label>Activar</label>
                                    <input type='radio' name='status' id='status' value='0' /><label>Desactivar</label>
                                    <span id='register_fechaAlta_errorloc' class='error'></span>
                                </div>
                                <div class='container'>
                                    <input type='submit' name='Submit' value='Enviar' />
                                </div>
                                </form>

                                </div>

                        </div>

                        <!-- client-side Form Validations:
                        Uses the excellent form validation script from JavaScript-coder.com-->

                        <script type='text/javascript'>
                        // <![CDATA[
                            var pwdwidget = new PasswordWidget('thepwddiv','password');

                            pwdwidget.MakePWDWidget();
                            
                            var frmvalidator  = new Validator("register");

                            frmvalidator.EnableOnPageErrorDisplay();

                            frmvalidator.EnableMsgsTogether();
                            
                            frmvalidator.addValidation("name","req","Por favor ingresa tu nombre");

                            frmvalidator.addValidation("email","req","Por favor ingresa tu Email");

                            frmvalidator.addValidation("email","email","La dirección de correo que enviaste no es válida");
                            
                            frmvalidator.addValidation("password","req","Por favor escribe una contraseña");

                        // ]]>
                        </script>

                        <!--
                        Form Code End (see html-form-guide.com for more info.)
                        -->

            </div>  

            <div class="imagenZona" id= "imagenZona">

            </div>  

        </div><!--fin div informacionEdificio-->

    </div>

    <div id= "pie">

    </div>  

</aside>

</section>
    
</body>
