-- MySQL dump 10.13  Distrib 5.1.54, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: formularios
-- ------------------------------------------------------
-- Server version	5.1.54

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emprendendores`
--

DROP TABLE IF EXISTS `emprendendores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emprendendores` (
  `idemprendendores` int(11) NOT NULL AUTO_INCREMENT,
  `tipoBene` varchar(45) DEFAULT NULL,
  `primerApellido` varchar(60) DEFAULT NULL,
  `segundoApellido` varchar(60) DEFAULT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `fechaNac` date DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `estadoCivil` varchar(45) DEFAULT NULL,
  `estudios` varchar(45) DEFAULT NULL,
  `identificacion` varchar(45) DEFAULT NULL,
  `numIdentificacion` varchar(100) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `entidadNac` varchar(45) DEFAULT NULL,
  `curp` varchar(45) DEFAULT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `numExt` int(20) DEFAULT NULL,
  `numInt` int(45) DEFAULT NULL,
  `calle1` varchar(100) DEFAULT NULL,
  `calle2` varchar(100) DEFAULT NULL,
  `otraRef` varchar(100) DEFAULT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `entidad` varchar(100) DEFAULT NULL,
  `cp` int(15) DEFAULT NULL,
  `telFijo` int(15) DEFAULT NULL,
  `telCel` int(15) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `taller` varchar(60) DEFAULT NULL,
  `grupo` varchar(45) DEFAULT NULL,
  `contrasenia` varchar(100) NOT NULL,
  PRIMARY KEY (`idemprendendores`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emprendendores`
--

LOCK TABLES `emprendendores` WRITE;
/*!40000 ALTER TABLE `emprendendores` DISABLE KEYS */;
INSERT INTO `emprendendores` VALUES (42,'Emprendedor','SANTOS','HERNANDEZ','ERIKA','0000-00-00','F','Casado','Universidad','IFE','IDMEX1132267374','Mexicana','Estado de MÃ©xico','SAHE750329MMCNRR06','LAGO YURIRIA',108,0,'SIERRA DE LOS MIMBRES','MIMOSAS','No contesto','VILLAS DE TERRANOVA','ACOLMAN','ACOLMAN','Estado de MÃ©xico',55883,2147483647,2147483647,'e.lestitis@gmail.com','Innova y Emprende','Grupo 2',''),(41,'Emprendedor','cedeÃ±o','nuÃ±ez','Jair ','1993-08-11','M','Casado','Bachillerato','IFE','6031127784816','Mexicana','Distrito Federal','cenj930811hdfdxr09','avenida adolfo lopez mateos',15,0,'calle 2','circunvalacion oriente','No contesto','jardines de santa clara','0001','ecatepec de morelos','Estado de MÃ©xico',55120,2147483647,2147483647,'miclothemaster619@gmail.com','Innova y Emprende','Grupo 2',''),(40,'Emprendedor','cedeÃ±o','nuÃ±ez','Jair ','1993-08-11','M','Casado','Bachillerato','IFE','6031127784816','Mexicana','Distrito Federal','cenj930811hdfdxr09','avenida adolfo lopez mateos',15,0,'calle 2','circunvalacion oriente','No contesto','jardines de santa clara','0001','ecatepec de morelos','Estado de MÃ©xico',55120,2147483647,2147483647,'miclothemaster619@gmail.com','Innova y Emprende','Grupo 2',''),(39,'Emprendedor','GARCIA','ESQUIVEL','MARIBEL','0000-00-00','F','Casado','Universidad','IFE','IDMEX1116988853 13111031371727107241M2412311MEX','Mexicana','Estado de MÃ©xico','GAEM710724MTLRSR02','CALLE 1',1,0,'CALLE 2 ','CALLE 3','TIENDA DE ABARROTES EL NETO EN LA ESQUINA DE CALLE 1','U. HAB. BALCONES DE ECATEPEC','EL TEJOCOTE','ECATEPEC DE MORELOS','Estado de MÃ©xico',55017,2147483647,2147483647,'maribel_garcia_e@yahoo.com','Innova y Emprende','Grupo 2',''),(38,'Emprendedor','ZARATE','VELAZQUEZ','DIANA ELIZABETH','1994-04-23','F','Soltero','Universidad','IFE','1626129862489','Mexicana','Distrito Federal','ZAVD940423MDFRLN06','GARABATO MANZANA 3 LOTE',4,0,'PARCELA','PERIODISTAS','No contesto','NOVELA MEXICANA 2','MEXICO','ECATEPEC','Estado de MÃ©xico',55268,2147483647,2147483647,'diana.zarate.23@outlook.com','Innova y Emprende','Grupo 2',''),(37,'Emprendedor','Garcia','Delgado','JesÃºs','1947-02-04','M','Soltero','Bachillerato','IFE','IFE - GRDLJS47020409H800','Mexicana','Distrito Federal','GADJ470204HDFRLS06','AND. FUENTE DE LA RANA',11,0,'CALZADA DE LA VIGA','AV. MEXICO','No contesto','FRACC. FUENTES E SAN CRISTOBAL','SAN CRISTOBAL','ECATEPEC DE MORELOS','Estado de MÃ©xico',55040,2147483647,2147483647,'jesusgarciadelgado0402@gmiel.com','Innova y Emprende','Grupo 2',''),(8,'Emprendedor','Diaz','Cortes','Michel','0000-00-00','M','Soltero','Universidad','IFE','DZTLMC88121309H800','Mexicana','Distrito Federal','DITM88121309H800','Salaverry',834,402,'Av politecnico','Av Lindavista','No contesto','lindavista','G.A.M','G.A.M','Distrito Federal',7300,2147483647,2147483647,'linkinpark_37@hotmail.com','Innova y Emprende','Grupo 100','ztekj'),(36,'Emprendedor','VELAZQUEZ','NEGRETE','REBECA ELIZABETH','1965-04-03','F','Casado','Bachillerato','IFE','6274011184723','Mexicana','Distrito Federal','VENR650403MDFLGB09','BOULEVARD JARDINES MANZANA 27 LOTE',13,0,'AVENIDA OZUMBILLA','ALFABIA','JARDINES DEL REAL','FRACCIONAMIENTO LOS HEROES TECAMAC SECCION JARDINES','TECAMAC','TECAMAC','Estado de MÃ©xico',55764,2147483647,2147483647,'rebe.vel@hotmail.com','Innova y Emprende','Grupo 2',''),(11,'Emprendedor','barreto','lazcano','mayra','1992-04-09','F','Soltero','Universidad','IFE','brlzmy92040915m600','Mexicana','Estado de MÃ©xico','balm920409mmcrzy08','fuente de cleo',93,0,'fuente de cervantes','fuente de hercules','No contesto','fuentes del valle ','estado de mexico','tultitlan','Estado de MÃ©xico',54910,58799223,2147483647,'aryambl_22@hotmail.com','Innova y Emprende','Grupo 99',''),(35,'Emprendedor','Gonzalez','Jimenez','MarÃ­a Gisela','0000-00-00','F','Soltero','Universidad','IFE','0283047355453','Mexicana','Guanajuato','GOJG771103MGTNMS06','Queretaro',9,0,'Avenida Jalisco','La Paz','No contesto','Ampliacion Adolfo Lopez Mateos','Atizapan de Zaragoza','Atizapan de Zaragoza','Guanajuato',52910,21640531,2147483647,'giselle_glez@hotmail.com','Innova y Emprende','Grupo 99',''),(34,'Emprendedor','Avalos','Rivero ','Nora Ivonne','1977-10-08','F','Soltero','Universidad','IFE','idmex1225714310','Mexicana','Distrito Federal','aarn771008mdfvvr05','Manuel Avila Camacho ',33,0,'Berriozabal  ','Av. Ecatepec','no contesto','12 de Diciembre','Ecatepec','Ecatepec','Estado de MÃ©xico',55000,2147483647,2147483647,'nora77ivon@gmail.com','Ecatepec Emprende','Grupo 2',''),(33,'Emprendedor','Arias','Cruz','Jonathan Ricardo','1988-07-01','M','Soltero','Secundaria','Pasaporte','G07804706','Mexicana','Estado de MÃ©xico','17794583','Avenida Sur',9,0,'Sur 2','Sur 3','Entre calles Alamo y Boulevard Homex','Hogares Mexicanos','Ecatepec de Morelos','Ecatepec','Estado de MÃ©xico',55040,2147483647,2147483647,'jon_negro@hotmail.com','Innova y Emprende','Grupo 2',''),(32,'Emprendedor','Arias','Cruz','Jonathan Ricardo','1988-07-01','M','Soltero','Secundaria','Pasaporte','G07804706','Mexicana','Estado de MÃ©xico','17794583','Avenida Sur',9,0,'Sur 2 y Sur 3','Avenida Lopez Rayon','Boulevard HOMEX','Hogares Mexicanos','Ecatepec de morelos','Ecatepec','Estado de MÃ©xico',55040,2147483647,2147483647,'jon_negro@hotmail.com','Ecatepec Emprende','Grupo 2',''),(43,'Emprendedor','CORONA','ENRIQUEZ','LUDWIG ARTURO','1980-10-20','M','Soltero','Bachillerato','IFE','crenld80102009h201','Mexicana','Distrito Federal','COEL801020HDFRND08','IGNACIO M ALTAMIRANO',52,0,'5 DE MAYO','MELCHOR OCAMPO','No contesto','HOGARES MARLA','SAN CRISTOBAL ','ECATEPEC','Estado de MÃ©xico',55030,57872624,2147483647,'LUDWIGCORP1@GMAIL.COM','Innova y Emprende','Grupo 2',''),(54,'Emprendedor','Hurtado','Ramirez','MarÃ­a Teresa ','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Distrito Federal','HURT640511MDFRMR','Circuito cesar Camacho Quiroz mz 9 lote 46',46,0,'Valle del don','Valle de gualdaviquir','No contesto','Granjas independencia ','Estado de MÃ©xico ','Ecatepec','Estado de MÃ©xico',55290,2147483647,2147483647,'sandyjibran@hotmail.com','Innova y Emprende','Grupo 2',''),(53,'Emprendedor','AYALA','AGUILAR','SALVADOR','1963-06-18','M','Casado','Universidad','IFE','instituto nacional electoral','Mexicana','Distrito Federal','AAAS630618HDFYGL05','ORIENTE 239',112,0,'SUR 20','RÃO FRIO','No contesto','AGRÃCOLA ORIENTAL','MÃ‰XICO','IZTACALCO','Distrito Federal',8500,2147483647,2147483647,'ayala_as@yahoo.com.mx','Innova y Emprende','Grupo 3',''),(51,'Emprendedor','AYALA','AGUILAR','SALVADOR','1963-06-18','M','Casado','Universidad','IFE','instituto nacional electoral','Mexicana','Distrito Federal','AAAS630618HDFYGL05','ORIENTE 239',112,0,'SUR 20','RÃO FRIO','No contesto','AGRÃCOLA ORIENTAL','MÃ‰XICO','IZTACALCO','Distrito Federal',8500,2147483647,2147483647,'ayala_as@yahoo.com.mx','Innova y Emprende','Grupo 3',''),(52,'Emprendedor','AYALA','AGUILAR','SALVADOR','1963-06-18','M','Casado','Universidad','IFE','instituto nacional electoral','Mexicana','Distrito Federal','AAAS630618HDFYGL05','ORIENTE 239',112,0,'SUR 20','RÃO FRIO','No contesto','AGRÃCOLA ORIENTAL','MÃ‰XICO','IZTACALCO','Distrito Federal',8500,2147483647,2147483647,'ayala_as@yahoo.com.mx','Innova y Emprende','Grupo 3',''),(55,'Emprendedor','reyes','zepeda','alejandra','1994-11-04','F','Casado','Secundaria','IFE','1877131822572','Mexicana','Distrito Federal','reza941104mdfypl06','avenida adolfo lopez mateos',15,0,'4','circunvalacion sur','No contesto','jardines de santa clara','0001','ecatepec de morelos','Estado de MÃ©xico',55120,2147483647,2147483647,'alecedenorz@gmail.com','Innova y Emprende','Grupo 2',''),(56,'Emprendedor','Dominguez','Martinez','Estefania','0000-00-00','F','Casado','Universidad','IFE','DMMRES94120715M500','Mexicana','Estado de MÃ©xico','DOME941207MMCMRS03','CDA PRIMAVERA 9',5,0,'GUACAMAYAS','HALCONES','No contesto','SAN FRANCISCO DE ASIS ','ECATEPEC DE MORELOS','ECATEPEC DE MORELOS','Estado de MÃ©xico',55010,2147483647,2147483647,'estefania.dominguez.mtz@gmail.com','Innova y Emprende','Grupo 2',''),(57,'Emprendedor','Dominguez','Martinez','Estefania','0000-00-00','F','Casado','Universidad','IFE','DMMRES94120715M500','Mexicana','Estado de MÃ©xico','DOME941207MMCMRS03','CDA PRIMAVERA 9',5,0,'GUACAMAYAS','HALCONES','No contesto','SAN FRANCISCO DE ASIS ','ECATEPEC DE MORELOS','ECATEPEC DE MORELOS','Estado de MÃ©xico',55010,2147483647,2147483647,'estefania.dominguez.mtz@gmail.com','Innova y Emprende','Grupo 2',''),(58,'Emprendedor','HURTADO','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Distrito Federal','HURT640511MDFRMR00','CIRCUITO CESAR CAMACHO QUIROZ Mz 9 lT 46',46,0,'VALLE DEL DON','AVENIDA TOLUCA','No contesto','GRANJAS INDEPENDENCIA','ECATEPEC','ECATEPEC','Estado de MÃ©xico',55290,2147483647,2147483647,'sandyjibran@hotmail.com','Innova y Emprende','Grupo 2',''),(59,'Emprendedor','HURTADO','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Distrito Federal','HURT640511MDFRMR00','CIRCUITO CESAR CAMACHO QUIROZ Mz 9 lT 46',46,0,'VALLE DEL DON','AVENIDA TOLUCA','No contesto','GRANJAS INDEPENDENCIA','ECATEPEC','ECATEPEC','Estado de MÃ©xico',55290,2147483647,2147483647,'sandyjibran@hotmail.com','Innova y Emprende','Grupo 2',''),(60,'Emprendedor','HURTADO','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Distrito Federal','HURT640511MDFRMR00','CIRCUITO CESAR CAMACHO QUIROZ Mz 9 lT 46',46,0,'VALLE DEL DON','AVENIDA TOLUCA','No contesto','GRANJAS INDEPENDENCIA','ECATEPEC','ECATEPEC','Estado de MÃ©xico',55290,2147483647,2147483647,'sandyjibran@hotmail.com','Innova y Emprende','Grupo 2',''),(61,'Emprendedor','HURTADO','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Guanajuato','Hurt640511mdfrmr00','Mier y Pesado.',135,25,'Xola ','Morena','No contesto','Del Valle','Mexico','D.F','Distrito Federal',3100,2147483647,2147483647,'maritehr.mhr@gmail.com','Innova y Emprende','Grupo 2',''),(62,'Emprendedor','HURTADO','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Guanajuato','Hurt640511mdfrmr00','Mier y Pesado.',135,25,'Xola ','Morena','No contesto','Del Valle','Mexico','D.F','Distrito Federal',3100,2147483647,2147483647,'maritehr.mhr@gmail.com','Innova y Emprende','Grupo 2',''),(63,'Emprendedor','MAYORGA','RAMIREZ','FANCISCO JAVIER ','1956-01-01','M','Casado','Primaria','IFE','0000028766677','Mexicana','Distrito Federal','MARF560124HDFYMR02','JOSEFA O DE DGUEZ ',1,31,'AV TOLUCA ','AV ALMAZORA','UNA BASE DE TAXIS ','NICOLAS BRAVO ','ECATEPEC','ECATEPEC DE MORELOS ','Estado de MÃ©xico',55297,51213448,2147483647,'frac.javier_mr@hotmail.com','TESE Emprendiendo con Ã©xito','Grupo 2',''),(64,'Emprendedor','MAYORGA','RAMIREZ','FANCISCO JAVIER ','1956-01-01','M','Casado','Primaria','IFE','0000028766677','Mexicana','Distrito Federal','MARF560124HDFYMR02','JOSEFA O DE DGUEZ ',1,31,'AV TOLUCA ','AV ALMAZORA','UNA BASE DE TAXIS ','NICOLAS BRAVO ','ECATEPEC','ECATEPEC DE MORELOS ','Estado de MÃ©xico',55297,51213448,2147483647,'frac.javier_mr@hotmail.com','TESE Emprendiendo con Ã©xito','Grupo 2',''),(65,'Emprendedor','MAYORGA','RAMIREZ','FANCISCO JAVIER ','1956-01-01','M','Casado','Primaria','IFE','0000028766677','Mexicana','Distrito Federal','MARF560124HDFYMR02','JOSEFA O DE DGUEZ ',1,31,'AV TOLUCA ','AV ALMAZORA','UNA BASE DE TAXIS ','NICOLAS BRAVO ','ECATEPEC','ECATEPEC DE MORELOS ','Estado de MÃ©xico',55297,51213448,2147483647,'frac.javier_mr@hotmail.com','TESE Emprendiendo con Ã©xito','Grupo 2',''),(66,'Emprendedor','MAYORGA','RAMIREZ','FANCISCO JAVIER ','1956-01-01','M','Casado','Primaria','IFE','0000028766677','Mexicana','Distrito Federal','MARF560124HDFYMR02','JOSEFA O DE DGUEZ ',1,31,'AV TOLUCA ','AV ALMAZORA','UNA BASE DE TAXIS ','NICOLAS BRAVO ','ECATEPEC','ECATEPEC DE MORELOS ','Estado de MÃ©xico',55297,51213448,2147483647,'frac.javier_mr@hotmail.com','TESE Emprendiendo con Ã©xito','Grupo 2',''),(67,'Emprendedor','MAYORGA','RAMIREZ','FANCISCO JAVIER ','1956-01-01','M','Casado','Primaria','IFE','0000028766677','Mexicana','Distrito Federal','MARF560124HDFYMR02','JOSEFA O DE DGUEZ ',1,31,'AV TOLUCA ','AV ALMAZORA','UNA BASE DE TAXIS ','NICOLAS BRAVO ','ECATEPEC','ECATEPEC DE MORELOS ','Estado de MÃ©xico',55297,51213448,2147483647,'frac.javier_mr@hotmail.com','TESE Emprendiendo con Ã©xito','Grupo 2',''),(68,'Emprendedor','velazquez','soledad','ricardo','1982-08-28','M','Casado','Universidad','IFE','1796112158543','Mexicana','Aguascalientes','ves820828hasllc09','ruiseÃ±ores',57,0,'av palomas','av alondras','hay dos tiendas en la calle y esta a tres cuadras del aurrera de palomas','izcalli jardines','ecatepec de morelos','ecatepec','Estado de MÃ©xico',55050,2147483647,2147483647,'richardvelazquezsol@hotmail.com','Innova y Emprende','Grupo 2',''),(69,'Emprendedor','velazquez','soledad','ricardo','1982-08-28','M','Casado','Universidad','IFE','1796112158543','Mexicana','Aguascalientes','ves820828hasllc09','ruiseÃ±ores',57,0,'av palomas','av alondras','hay dos tiendas en la calle y esta a tres cuadras del aurrera de palomas','izcalli jardines','ecatepec de morelos','ecatepec','Estado de MÃ©xico',55050,2147483647,2147483647,'richardvelazquezsol@hotmail.com','Innova y Emprende','Grupo 2',''),(70,'Emprendedor','velazquez','soledad','ricardo','1982-08-28','M','Casado','Universidad','IFE','1796112158543','Mexicana','Aguascalientes','ves820828hasllc09','ruiseÃ±ores',57,0,'av palomas','av alondras','hay dos tiendas en la calle y esta a tres cuadras del aurrera de palomas','izcalli jardines','ecatepec de morelos','ecatepec','Estado de MÃ©xico',55050,2147483647,2147483647,'richardvelazquezsol@hotmail.com','Innova y Emprende','Grupo 2',''),(71,'Emprendedor','jimenez','garcia','julio martin','1963-02-23','M','Casado','Universidad','IFE','1666019534072','Mexicana','Distrito Federal','jigj630223hdfmrl07','dalias',3,1,' hortencia','azucena','No contesto','florida','0001','ecatepec de morelos','Estado de MÃ©xico',55240,2147483647,2147483647,'tecnollaves@live.com.mx','Innova y Emprende','Grupo 2',''),(72,'Emprendedor','velazquez','soledad','ricardo','1982-08-28','M','Casado','Universidad','IFE','1796112158543','Mexicana','Aguascalientes','ves820828hasllc09','ruiseÃ±ores',57,0,'av palomas','av alondras','hay dos tiendas en la calle y esta a tres cuadras del aurrera de palomas','izcalli jardines','ecatepec de morelos','ecatepec','Estado de MÃ©xico',55050,2147483647,2147483647,'richardvelazquezsol@hotmail.com','Innova y Emprende','Grupo 2',''),(73,'Emprendedor','cedeÃ±o','paredes','alberto','1968-04-13','M','Casado','Bachillerato','IFE','1922005706589','Mexicana','Distrito Federal','cepa680413hdfdrl04','boulevar de lo aztecas',12,0,'adolfo lopez mateos ','acolman','No contesto','cuidad azteca','0001','ecatepec de morelos','Estado de MÃ©xico',55120,2147483647,2147483647,'albertocerpar46@gmail.com','Innova y Emprende','Grupo 2',''),(74,'TALLER INNOVA Y EMPRENDE GRUPO 2','HURTADO ','RAMIREZ','MARIA TERESA','1964-05-11','F','Casado','Secundaria','IFE','4336121384710','Mexicana','Guanajuato','HURT640511MDFRMR00','MIERY PESADO',135,25,'XOLA','MORENA','No contesto','DEL VALLE','MEXICO','DISTRITO FEDERAL','Distrito Federal',3100,2147483647,2147483647,'SANDYJIBRAN@HOMAIL.COM','Innova y Emprende','Grupo 2',''),(75,'Emprendedor','chavez','lugo','miguel','1989-12-30','M','Soltero','Universidad','IFE','343242343214534534','Mexicana','Distrito Federal','calm891230hmchgg006','indenpendencia',22,0,'av. tlahuac','san rafael atlixoc','No contesto','san francisco tlaltenco','tlahuac','tlahuac','Distrito Federal',12300,2147483647,1234567890,'miguel@ideadiseno.com','TultitlÃ¡n Emprende','Grupo 1','urofy'),(76,'Emprendedor','Feria','Victoria','Ana de la Cruz','1968-11-24','F','Casado','Universidad','IFE','4252008724211','Mexicana','Distrito Federal','FEVA681124MDFRCN09','decima cda. Bosques de los Cedros',46,4,'pirules','casuarinas','No contesto','Fracc. HerÃ³es TecÃ¡mac, Secc. Bosques','TecÃ¡mac','TecÃ¡mac','Estado de MÃ©xico',55764,2147483647,2147483647,'ana_feria_vic@yahoo.com.mx','Innova y Emprende','Grupo 3','aoikc');
/*!40000 ALTER TABLE `emprendendores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_1`
--

DROP TABLE IF EXISTS `modulo_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_1` (
  `idmodulo_1` int(11) NOT NULL AUTO_INCREMENT,
  `nombreProyecto` varchar(100) DEFAULT NULL,
  `giro` varchar(60) DEFAULT NULL,
  `DE1` varchar(5) DEFAULT NULL,
  `DE2` varchar(5) DEFAULT NULL,
  `DE3` varchar(5) DEFAULT NULL,
  `DE4` varchar(5) DEFAULT NULL,
  `DE5` varchar(5) DEFAULT NULL,
  `DE6` varchar(5) DEFAULT NULL,
  `DE7` varchar(5) DEFAULT NULL,
  `DE8` varchar(5) DEFAULT NULL,
  `DE9` varchar(5) DEFAULT NULL,
  `DE10` varchar(5) DEFAULT NULL,
  `DE11` varchar(5) DEFAULT NULL,
  `DE12` varchar(5) DEFAULT NULL,
  `idEmprendendores` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmodulo_1`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_1`
--

LOCK TABLES `modulo_1` WRITE;
/*!40000 ALTER TABLE `modulo_1` DISABLE KEYS */;
INSERT INTO `modulo_1` VALUES (4,'Kawaii+Inc.','Industria','A','A','A','B','B','A','B','A','A','B','A','A',32),(5,'Kawaii+Inc.','Industria','A','A','A','A','B','A','B','A','A','B','A','A',33),(6,'Venta+de+Golocina','Comercio','A','A','B','A','A','B','A','B','B','A','A','A',34),(7,'ESTETICA+ATRACTION','Servicios','A','A','B','B','B','B','B','B','B','C','B','C',35),(8,'Panader%C3%ADa+Do%C3%B1a+Triny','Comercio','A','A','A','A','A','A','A','A','A','A','A','A',37),(9,'ESCUELA+Y+VENTA+DE+PASTELERIA%2C+REPOSTERIA+Y+PANADERIA','Servicios','B','B','A','B','A','A','A','A','A','A','A','A',36),(10,'NO+TENGO+PROYECTO+DEFINIDO','Servicios','A','A','A','A','A','A','B','A','B','A','A','C',38),(11,'CONSULTORIO+PODOL%C3%93GICO','Servicios','B','B','A','B','B','A','A','B','B','A','A','B',39),(12,'','','','','','','','','','','','','','',0),(13,'equipamiento+para+sistemas+computacionales+automotrizes','Automotriz','A','A','A','A','A','A','A','B','B','B','A','B',40),(14,'LES+TITIS+CONFITER%C3%8DA','Comercio','A','B','A','B','B','C','B','B','B','B','B','B',42),(15,'king','Servicios','A','A','A','A','A','B','B','B','B','A','A','A',43),(16,'proyecto','Automotriz','A','A','A','A','A','A','A','A','A','A','A','A',46),(17,'proyecto','Automotriz','A','A','A','A','A','A','A','A','A','A','A','A',46),(18,'AYALA+RAM%C3%8DREZ+Y+ASOCIADOS','Servicios','A','A','A','B','B','B','B','B','A','B','B','A',53),(19,'Servicios+de+belleza','Servicios','A','A','A','A','A','A','A','A','A','A','A','A',60),(20,'LEIDYMARRY','Servicios','A','A','B','A','B','A','A','A','A','A','A','A',62),(21,'JUGIUETES+DE+MADERA+Y+MUEBLES+INFANTILES+','Comercio','A','A','A','A','A','A','A','A','A','A','A','A',67),(22,'cerrajeria+tecnollaves','Servicios','A','A','A','A','A','A','A','A','B','D','A','B',71),(23,'restautant+regional','Comercio','A','A','A','A','B','B','A','B','A','A','A','C',72),(24,'cerrajeria+azteca','Servicios','A','A','A','A','A','A','A','A','A','B','B','B',73),(25,'idea+diseno','Servicios','A','B','B','A','B','A','B','A','B','A','B','A',75);
/*!40000 ALTER TABLE `modulo_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_1_EA`
--

DROP TABLE IF EXISTS `modulo_1_EA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_1_EA` (
  `idmodulo_1_Mer` int(11) NOT NULL AUTO_INCREMENT,
  `idModulo_1` int(11) DEFAULT NULL,
  `EA1` varchar(5) DEFAULT NULL,
  `EA2` varchar(5) DEFAULT NULL,
  `EA3` varchar(5) DEFAULT NULL,
  `EA4` varchar(5) DEFAULT NULL,
  `EA5` varchar(5) DEFAULT NULL,
  `EA6` varchar(5) DEFAULT NULL,
  `EA7` varchar(5) DEFAULT NULL,
  `EA8` varchar(5) DEFAULT NULL,
  `EA9` varchar(5) DEFAULT NULL,
  `EA10` varchar(5) DEFAULT NULL,
  `EA11` varchar(5) DEFAULT NULL,
  `EA12` varchar(5) DEFAULT NULL,
  `EA13` varchar(5) DEFAULT NULL,
  `EA14` varchar(5) DEFAULT NULL,
  `EA15` varchar(5) DEFAULT NULL,
  `EA16` varchar(5) DEFAULT NULL,
  `EA17` varchar(5) DEFAULT NULL,
  `EA18` varchar(5) DEFAULT NULL,
  `EA19` varchar(5) DEFAULT NULL,
  `EA20` varchar(10) CHARACTER SET utf8 NOT NULL,
  `EA21` varchar(10) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`idmodulo_1_Mer`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_1_EA`
--

LOCK TABLES `modulo_1_EA` WRITE;
/*!40000 ALTER TABLE `modulo_1_EA` DISABLE KEYS */;
INSERT INTO `modulo_1_EA` VALUES (4,4,'B','C','A','A','B','B','B','C','C','C','C','C','C','C','C','B','C','C','C','',''),(5,5,'C','B','A','A','A','B','B','C','C','B','C','B','C','C','C','B','D','C','C','',''),(6,6,'C','C','B','B','B','B','B','B','B','B','B','D','D','D','C','C','C','C','C','',''),(7,7,'C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','',''),(8,8,'A','A','A','A','A','A','A','A','A','A','A','B','B','B','B','A','A','A','A','',''),(9,9,'C','C','C','C','C','C','C','C','C','C','C','C','C','B','B','C','A','C','B','',''),(10,10,'C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','',''),(11,11,'A','B','B','B','B','B','B','B','B','B','B','A','B','B','A','B','A','B','B','',''),(12,12,'','','','','','','','','','','','','','','','','','','','',''),(13,13,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','',''),(14,14,'C','C','B','B','B','B','B','C','C','C','C','C','C','C','B','B','C','B','B','',''),(15,15,'A','A','B','B','B','B','A','A','B','B','B','B','B','B','B','B','B','C','C','',''),(16,16,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','',''),(17,17,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','',''),(18,18,'A','A','A','A','A','A','A','A','A','B','B','B','B','B','A','B','A','A','B','A','A'),(19,19,'B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B'),(20,20,'C','A','A','A','A','A','A','C','C','A','A','A','A','A','A','C','A','A','C','C','C'),(21,21,'B','B','A','A','A','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B'),(22,22,'B','B','A','A','A','A','A','A','B','B','B','A','A','B','B','B','A','A','A','A','A'),(23,23,'C','B','A','A','A','A','A','B','B','B','B','B','B','B','B','B','B','A','A','B','B'),(24,24,'A','A','A','A','A','A','A','A','A','A','A','A','A','B','B','B','A','A','A','A','A'),(25,25,'C','D','C','B','A','B','C','D','C','B','A','B','C','D','C','B','A','B','C','D','C');
/*!40000 ALTER TABLE `modulo_1_EA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_1_EF`
--

DROP TABLE IF EXISTS `modulo_1_EF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_1_EF` (
  `idmodulo_1_EF` int(11) NOT NULL AUTO_INCREMENT,
  `idModulo_1` int(11) DEFAULT NULL,
  `EF1` varchar(5) DEFAULT NULL,
  `EF2` varchar(5) DEFAULT NULL,
  `EF3` varchar(5) DEFAULT NULL,
  `EF4` varchar(5) DEFAULT NULL,
  `EF5` varchar(5) DEFAULT NULL,
  `EF6` varchar(5) DEFAULT NULL,
  `EF7` varchar(5) DEFAULT NULL,
  `EF8` varchar(5) DEFAULT NULL,
  `EF9` varchar(5) DEFAULT NULL,
  `EF10` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idmodulo_1_EF`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_1_EF`
--

LOCK TABLES `modulo_1_EF` WRITE;
/*!40000 ALTER TABLE `modulo_1_EF` DISABLE KEYS */;
INSERT INTO `modulo_1_EF` VALUES (4,4,'A','A','A','B','B','C','C','C','C','B'),(5,5,'A','B','B','B','B','C','C','C','C','B'),(6,6,'B','B','C','C','C','C','C','C','C','C'),(7,7,'C','C','D','C','C','C','C','C','C','C'),(8,8,'A','A','A','A','A','B','B','B','B','B'),(9,9,'A','A','A','B','B','C','C','C','C','B'),(10,10,'C','C','C','C','C','C','C','C','C','C'),(11,11,'A','B','B','B','B','A','B','A','B','B'),(12,12,'','','','','','','','','',''),(13,13,'A','A','A','B','B','A','B','B','A','A'),(14,14,'B','B','B','C','C','C','C','C','C','C'),(15,15,'B','B','C','C','C','B','C','C','C','C'),(16,16,'A','A','A','A','A','A','A','A','A','A'),(17,17,'A','A','A','A','A','A','A','A','A','A'),(18,18,'B','B','B','A','A','A','A','A','B','A'),(19,19,'B','B','B','B','B','B','B','B','B','B'),(20,20,'A','A','A','A','A','A','A','A','A','A'),(21,21,'B','B','B','B','B','B','B','B','B','B'),(22,22,'A','A','A','A','B','B','B','B','A','A'),(23,23,'B','B','B','C','B','C','C','C','C','C'),(24,24,'A','A','A','A','B','B','B','B','A','A'),(25,25,'B','A','B','C','D','C','B','A','B','C');
/*!40000 ALTER TABLE `modulo_1_EF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_1_ET`
--

DROP TABLE IF EXISTS `modulo_1_ET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_1_ET` (
  `idmodulo_1_Mer` int(11) NOT NULL AUTO_INCREMENT,
  `idModulo_1` int(11) DEFAULT NULL,
  `ET1` varchar(5) DEFAULT NULL,
  `ET2` varchar(5) DEFAULT NULL,
  `ET3` varchar(5) DEFAULT NULL,
  `ET4` varchar(5) DEFAULT NULL,
  `ET5` varchar(5) DEFAULT NULL,
  `ET6` varchar(5) DEFAULT NULL,
  `ET7` varchar(5) DEFAULT NULL,
  `ET8` varchar(5) DEFAULT NULL,
  `ET9` varchar(5) DEFAULT NULL,
  `ET10` varchar(5) DEFAULT NULL,
  `ET11` varchar(5) DEFAULT NULL,
  `ET12` varchar(5) DEFAULT NULL,
  `ET13` varchar(5) DEFAULT NULL,
  `ET14` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idmodulo_1_Mer`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_1_ET`
--

LOCK TABLES `modulo_1_ET` WRITE;
/*!40000 ALTER TABLE `modulo_1_ET` DISABLE KEYS */;
INSERT INTO `modulo_1_ET` VALUES (4,4,'A','A','A','B','A','A','A','B','A','B','A','A','B','B'),(5,5,'B','B','A','B','B','B','B','C','C','C','C','B','C','B'),(6,6,'B','B','A','A','A','A','A','B','A','B','A','B','D','D'),(7,7,'C','C','C','C','D','D','D','C','D','C','C','B','C','C'),(8,8,'A','A','A','B','B','A','B','A','A','D','A','B','B','B'),(9,9,'A','A','B','A','A','A','B','B','B','B','B','A','B','B'),(10,10,'C','C','C','C','C','C','C','C','C','C','C','C','C','C'),(11,11,'B','B','A','A','A','A','A','A','A','C','C','A','A','C'),(12,12,'','','','','','','','','','','','','',''),(13,13,'A','B','A','A','A','A','A','A','A','B','A','A','A','B'),(14,14,'A','B','A','A','A','B','A','C','B','C','C','A','C','C'),(15,15,'C','B','B','C','C','A','A','B','C','C','C','A','A','A'),(16,16,'A','A','A','A','A','A','A','A','A','A','A','A','A','A'),(17,17,'A','A','A','A','A','A','A','A','A','A','A','A','A','A'),(18,18,'A','A','A','A','A','A','A','B','A','A','A','A','A','B'),(19,19,'B','B','B','B','B','B','B','B','B','B','B','B','B','B'),(20,20,'A','A','A','A','A','A','B','A','A','B','B','A','A','A'),(21,21,'A','A','A','B','B','A','B','A','A','A','A','A','A','A'),(22,22,'A','A','A','A','A','A','A','A','A','B','A','A','A','B'),(23,23,'A','A','A','B','B','A','A','A','A','C','A','B','B','C'),(24,24,'A','A','A','A','A','A','A','A','A','B','A','A','A','B'),(25,25,'A','B','C','D','C','B','A','B','C','D','C','B','A','B');
/*!40000 ALTER TABLE `modulo_1_ET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_1_Mer`
--

DROP TABLE IF EXISTS `modulo_1_Mer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_1_Mer` (
  `idmodulo_1_Mer` int(11) NOT NULL AUTO_INCREMENT,
  `idModulo_1` int(11) DEFAULT NULL,
  `M1` varchar(5) DEFAULT NULL,
  `M2` varchar(5) DEFAULT NULL,
  `M3` varchar(5) DEFAULT NULL,
  `M4` varchar(5) DEFAULT NULL,
  `M5` varchar(5) DEFAULT NULL,
  `M6` varchar(5) DEFAULT NULL,
  `M7` varchar(5) DEFAULT NULL,
  `M8` varchar(5) DEFAULT NULL,
  `M9` varchar(5) DEFAULT NULL,
  `M10` varchar(5) DEFAULT NULL,
  `M11` varchar(5) DEFAULT NULL,
  `M12` varchar(5) DEFAULT NULL,
  `M13` varchar(5) DEFAULT NULL,
  `M14` varchar(5) DEFAULT NULL,
  `M15` varchar(5) DEFAULT NULL,
  `M16` varchar(5) DEFAULT NULL,
  `M17` varchar(5) DEFAULT NULL,
  `M18` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idmodulo_1_Mer`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_1_Mer`
--

LOCK TABLES `modulo_1_Mer` WRITE;
/*!40000 ALTER TABLE `modulo_1_Mer` DISABLE KEYS */;
INSERT INTO `modulo_1_Mer` VALUES (4,4,'B','B','A','A','B','A','A','C','A','B','B','B','B','A','C','B','A',''),(5,5,'A','B','A','A','B','A','A','D','B','B','B','C','B','A','C','B','A',''),(6,6,'B','B','B','B','B','B','B','B','B','B','B','B','A','B','B','B','B',''),(7,7,'C','C','C','C','C','C','D','D','C','C','D','C','D','D','C','C','C',''),(8,8,'B','A','A','A','A','B','A','D','A','A','A','B','A','B','B','A','A',''),(9,9,'A','B','A','A','A','B','B','B','A','A','B','B','A','B','B','A','B',''),(10,10,'C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C','C',''),(11,11,'A','B','B','B','B','C','B','C','B','B','B','B','A','B','C','B','C',''),(12,12,'','','','','','','','','','','','','','','','','',''),(13,13,'A','A','A','A','A','A','A','B','A','A','A','B','B','B','A','A','A',''),(14,14,'A','B','B','B','B','C','A','C','A','A','A','B','A','C','C','C','C',''),(15,15,'B','B','B','B','A','B','A','C','B','B','C','C','B','C','B','A','A',''),(16,16,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A',''),(17,17,'A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A','A',''),(18,18,'A','A','B','B','B','B','A','B','A','A','A','B','B','B','B','B','B','A'),(19,19,'B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B','B'),(20,20,'B','B','B','A','A','A','A','C','C','B','B','C','B','B','C','B','B','B'),(21,21,'A','B','B','A','A','A','A','B','A','A','B','B','B','A','B','C','A','A'),(22,22,'A','A','A','A','A','A','A','B','A','A','A','A','C','B','B','A','A','A'),(23,23,'B','B','B','B','B','B','B','C','C','C','C','B','D','C','C','C','C','C'),(24,24,'A','A','A','A','A','A','A','B','A','A','A','A','B','A','B','A','A','A'),(25,25,'A','B','C','D','C','B','A','B','C','D','C','B','A','B','C','D','C','B');
/*!40000 ALTER TABLE `modulo_1_Mer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_2`
--

DROP TABLE IF EXISTS `modulo_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_2` (
  `idmodulo_2` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M2` text,
  PRIMARY KEY (`idmodulo_2`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_2`
--

LOCK TABLES `modulo_2` WRITE;
/*!40000 ALTER TABLE `modulo_2` DISABLE KEYS */;
INSERT INTO `modulo_2` VALUES (6,35,'lajkdlfklakf'),(7,46,'modulo 2 ');
/*!40000 ALTER TABLE `modulo_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_3`
--

DROP TABLE IF EXISTS `modulo_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_3` (
  `idmodulo_3` int(11) DEFAULT NULL,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M3` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_3`
--

LOCK TABLES `modulo_3` WRITE;
/*!40000 ALTER TABLE `modulo_3` DISABLE KEYS */;
INSERT INTO `modulo_3` VALUES (NULL,35,'lzkdlfkalkdflk'),(NULL,46,'modulo 3');
/*!40000 ALTER TABLE `modulo_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_4`
--

DROP TABLE IF EXISTS `modulo_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_4` (
  `idmodulo_4` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M3` text,
  PRIMARY KEY (`idmodulo_4`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_4`
--

LOCK TABLES `modulo_4` WRITE;
/*!40000 ALTER TABLE `modulo_4` DISABLE KEYS */;
INSERT INTO `modulo_4` VALUES (6,35,'.,zkdlglakdflÃ±kalÃ±sdfklÃ±askdflÃ±kalÃ±'),(7,46,'modulo 4');
/*!40000 ALTER TABLE `modulo_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_5`
--

DROP TABLE IF EXISTS `modulo_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_5` (
  `idmodulo_5` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M5_1` text,
  `M5_2` text,
  `M5_3` text,
  `M5_4` text,
  `M5_5` text,
  `M5_6` text,
  `M5_7` text,
  `M5_8` text,
  PRIMARY KEY (`idmodulo_5`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_5`
--

LOCK TABLES `modulo_5` WRITE;
/*!40000 ALTER TABLE `modulo_5` DISABLE KEYS */;
INSERT INTO `modulo_5` VALUES (6,35,'lkajsdklfjkaljdfkljakld','laksdlfkÃ±lakdslÃ±fk','Ã±lakdlÃ±fkalÃ±kdf','Ã±lakdlÃ±kflÃ±akdflÃ±k','Ã±lakdlÃ±fklaÃ±skdflÃ±k','Ã±akdfÃ±kaÃ±ldkflÃ±k','laklÃ±dskflÃ±kaldÃ±fklÃ±','.alkdlÃ±fkalsÃ±dkflÃ±ka'),(7,46,'','','','','','','','');
/*!40000 ALTER TABLE `modulo_5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_6`
--

DROP TABLE IF EXISTS `modulo_6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_6` (
  `idmodulo_6` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M6_1` text,
  `M6_2` text,
  PRIMARY KEY (`idmodulo_6`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_6`
--

LOCK TABLES `modulo_6` WRITE;
/*!40000 ALTER TABLE `modulo_6` DISABLE KEYS */;
INSERT INTO `modulo_6` VALUES (6,35,'Ã±lklÃ±dfklÃ±aksdf','Si'),(7,46,'','Si');
/*!40000 ALTER TABLE `modulo_6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo_7`
--

DROP TABLE IF EXISTS `modulo_7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo_7` (
  `idmodulo_7` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendendores` int(11) DEFAULT NULL,
  `M7_1` text,
  `M7_2` text,
  PRIMARY KEY (`idmodulo_7`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo_7`
--

LOCK TABLES `modulo_7` WRITE;
/*!40000 ALTER TABLE `modulo_7` DISABLE KEYS */;
INSERT INTO `modulo_7` VALUES (6,35,'aÃ±lkdÃ±lfkalÃ±kdflÃ±ka','Ã±lakdlÃ±kfÃ±lakdlÃ±fkalÃ±dkf'),(7,46,'','');
/*!40000 ALTER TABLE `modulo_7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `idEmprendedor` int(11) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `contrasenia` varchar(100) NOT NULL,
  `fechaAlta` date NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idUsuarios`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (9,8,'linkinpark_37@hotmail.com','48fbc43af20cd753897d2ee15325180a','0000-00-00',1),(12,11,'aryambl_22@hotmail.com','f37c878b764b167841f6650744227729','0000-00-00',1),(33,32,'jon_negro@hotmail.com','f36ade0229308d45118d24f4fcdc7c74','0000-00-00',1),(34,33,'jon_negro@hotmail.com','d11a3ea11a74363fd04feb31691d9764','0000-00-00',1),(35,34,'nora77ivon@gmail.com','cfbe85ec4c020865220af1d79b047af3','0000-00-00',1),(36,35,'giselle_glez@hotmail.com','de4314180ad5ee4ab4af1c5a39b38ec3','0000-00-00',1),(37,36,'rebe.vel@hotmail.com','76c99b06a73da2b0cdaf9ad095cdb5c5','0000-00-00',1),(38,37,'jesusgarciadelgado0402@gmiel.com','3e0caa360f93c2739f6b4e9815b5f561','0000-00-00',1),(39,38,'diana.zarate.23@outlook.com','54627a6ec857a2c09c2a01a66a23da61','0000-00-00',1),(40,39,'maribel_garcia_e@yahoo.com','dacdd3b352eb256f8853bd8b06ae0d42','0000-00-00',1),(41,40,'miclothemaster619@gmail.com','24434731e5a67b2d7d82387d3c501a38','0000-00-00',1),(42,41,'miclothemaster619@gmail.com','ea420b5b0226df09b1a547e59d920748','0000-00-00',1),(43,42,'e.lestitis@gmail.com','e7d91063023a6d347330cdefbe9b7d5c','0000-00-00',1),(44,43,'LUDWIGCORP1@GMAIL.COM','25b4b558e4e4c31f335b7481bf4dc2ff','0000-00-00',1),(52,51,'ayala_as@yahoo.com.mx','80c2415c8c32be5a814185d5547fcd0b','0000-00-00',1),(53,52,'ayala_as@yahoo.com.mx','cf80eebc6aaaf35ac85e2db68c4e9b27','0000-00-00',1),(54,53,'ayala_as@yahoo.com.mx','a3ba61b7bb4a8cc68da293b57f911373','0000-00-00',1),(55,54,'sandyjibran@hotmail.com','bdc23518cdf01b2b8dae5a3a74589cae','0000-00-00',1),(56,55,'alecedenorz@gmail.com','58c58f355c00d20ea12ecc455325cd29','0000-00-00',1),(57,56,'estefania.dominguez.mtz@gmail.com','dc3348431fc1ec9e1394f7daee7080e1','0000-00-00',1),(58,57,'estefania.dominguez.mtz@gmail.com','a135ea5ad854daeeb8c96dafb8b0a8c4','0000-00-00',1),(59,58,'sandyjibran@hotmail.com','8c6cd6c878f80935a9c9ed442401357a','0000-00-00',1),(60,59,'sandyjibran@hotmail.com','4bacd71cfce4f6cebc46838bbb1e97ff','0000-00-00',1),(61,60,'sandyjibran@hotmail.com','e4a53d13cf7ffdf754d9cf1022775420','0000-00-00',1),(62,61,'maritehr.mhr@gmail.com','b26999959d77e5e2f19c9bf9344c4a8c','0000-00-00',1),(63,62,'maritehr.mhr@gmail.com','92c98024ad2d821fab1e14f42e715dc5','0000-00-00',1),(64,63,'frac.javier_mr@hotmail.com','ae7202f7de8e3617f8f0853c7574788a','0000-00-00',1),(65,64,'frac.javier_mr@hotmail.com','6a1f525629babd2a96619b5df23071e9','0000-00-00',1),(66,65,'frac.javier_mr@hotmail.com','a28c77b9827d66715341d399ffd29270','0000-00-00',1),(67,66,'frac.javier_mr@hotmail.com','2562538ac054f969c7d04e7a082c1a4d','0000-00-00',1),(68,67,'frac.javier_mr@hotmail.com','61206e79d2361decebc742b2d575a5b9','0000-00-00',1),(69,68,'richardvelazquezsol@hotmail.com','e06e929d505bd89bee2c14402289f0df','0000-00-00',1),(70,69,'richardvelazquezsol@hotmail.com','ec4b418628cdd1fd05ecec4d04d5f98f','0000-00-00',1),(71,70,'richardvelazquezsol@hotmail.com','1165b9ea480b75f2900686436d8873d8','0000-00-00',1),(72,71,'tecnollaves@live.com.mx','ddb893d66cafd6d322f696f5276a4c72','0000-00-00',1),(73,72,'richardvelazquezsol@hotmail.com','6302428f0d4115e85a5fda75ea1d132e','0000-00-00',1),(74,73,'albertocerpar46@gmail.com','de71ec46d37e4a15f0f50086e3f3ecc8','0000-00-00',1),(75,74,'SANDYJIBRAN@HOMAIL.COM','6723c32d70e0cf011c91b4ffa5f4a3bb','0000-00-00',1),(76,75,'arkhein_red@hotmail.com','f03e56b8aa351daa79ea4784d41ab8c7','0000-00-00',1),(77,76,'ana_feria_vic@yahoo.com.mx','44ba325a5d7fe52c9cf88ccb1f43ae2b','0000-00-00',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-12 15:50:17
