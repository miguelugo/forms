<?php 
         #####  ##   ## ###### ######  #### ##   ## #### ######           ######    ####  ##   ## #### ##   ##  #####
        ##   ## ### ###  ##  ## ##  ##  ##  ### ###  ##   ##  ##           ##  ##  ##  ## ##   ##  ##  ##   ## ##   ##
  ####  ##   ## #######  ##  ## ##  ##  ##  #######  ##   ##  ##    ####   ##  ## ##      ##   ##  ##   ## ##  ##   ##
 ##  ## ##   ## #######  #####  #####   ##  #######  ##   #####        ##  #####  ##      #######  ##   ## ##  ##   ##
 ##     ##   ## ## # ##  ##     ## ##   ##  ## # ##  ##   ## ##     #####  ## ##  ##      ##   ##  ##    ###   ##   ##
 ##  ## ##   ## ##   ##  ##     ##  ##  ##  ##   ##  ##   ##  ##   ##  ##  ##  ##  ##  ## ##   ##  ##    ###   ##   ##
  ####   #####  ##   ## ####   #### ## #### ##   ## #### #### ##    ##### #### ##   ####  ##   ## ####    #     #####


//primero creamos la función que hace la magia
      //esta funcion recorre carpetas y subcarpetas
      //añadiendo todo archivo que encuentre a su paso
      //recibe el directorio y el zip a utilizar
      function agregar_zip($dir, $zip){ 
        //verificamos si $dir es un directorio
        if (is_dir($dir)) { 
          //abrimos el directorio y lo asignamos a $da
          if ($da = opendir($dir)) {          
            //leemos del directorio hasta que termine
            while (($archivo = readdir($da))!== false) {   
              //Si es un directorio imprimimos la ruta
              //y llamamos recursivamente esta función 
              //para que verifique dentro del nuevo directorio
              //por mas directorios o archivos
              if (is_dir($dir . $archivo) && $archivo!="." && $archivo!=".."){
                //echo "<strong>Creando directorio: $dir$archivo</strong><br/>";                 
                agregar_zip($dir.$archivo . "/", $zip);  
 
              //si encuentra un archivo imprimimos la ruta donde se encuentra
              //y agregamos el archivo al zip junto con su ruta
              }elseif(is_file($dir.$archivo) && $archivo!="." && $archivo!=".."){
                //echo "Agregando archivo: $dir$archivo <br/>";                                    
                $zip->addFile($dir.$archivo, $dir.$archivo);                     
              }             
            }
            //cerramos el directorio abierto en el momento
            closedir($da); 
          }
        }       
      } //fin de la función
 
      //creamos una instancia de ZipArchive      
      $zip = new ZipArchive();
 
      //directorio a comprimir
      //la barra inclinada al final es importante
      //la ruta debe ser relativa no absoluta      
      $dir = 'talleres/';
 
      //ruta donde guardar los archivos zip, ya debe existir
      $rutaFinal="./";
 	  $fecha = date("d-m-Y");


      $archivoZip = $fecha."_Talleres.zip";  
 
      if($zip->open($archivoZip,ZIPARCHIVE::CREATE)===true) {  
        agregar_zip($dir, $zip);
        $zip->close();
 
        chmod($archivoZip, 0777);

        // //Muevo el archivo a una ruta
        // //donde no se mezcle los zip con los demas archivos
        // @rename($archivoZip, "$rutaFinal$archivoZip");
 
        // //Hasta aqui el archivo zip ya esta creado
 
        // //Verifico si el archivo ha sido creado
        // if (file_exists($rutaFinal.$archivoZip)){
        //   //echo "Proceso Finalizado!! <br/><br/>
        //   //    Descargar: <a href='$rutaFinal$archivoZip'>$archivoZip</a>";  
        // }else{
        //   //echo "Error, archivo zip no ha sido creado!!";
        // }                    
      }

        ##   ## ##   ## ####   ##   ######      ####   ##### ######  ######  #######  #####
        ###  ## ##   ##  ##   ####   ##  ##    ##  ## ##   ## ##  ##  ##  ##  ##   # ##   ##
  ####  #### ##  ## ##   ##  ##  ##  ##  ##   ##      ##   ## ##  ##  ##  ##  ## #   ##   ##
 ##  ## ## ####  ## ##   ##  ##  ##  #####    ##      ##   ## #####   #####   ####   ##   ##
 ###### ##  ###   ###    ##  ######  ## ##    ##      ##   ## ## ##   ## ##   ## #   ##   ##
 ##     ##   ##   ###    ##  ##  ##  ##  ##    ##  ## ##   ## ##  ##  ##  ##  ##   # ##   ##
  ##### ##   ##    #    #### ##  ## #### ##     ####   ##### #### ## #### ## #######  #####
/*
require_once("./libs/class.phpmailer.php");

$mail = new PHPMailer();

$mail->CharSet = 'utf-8';

$mail->IsSMTP();

$mail->AddAddress("arkhein_red@hotmail.com", "Miguel Lugo");
$mail->AddAddress("vagabundo.dela.vida@gmail.com", "Miguel Lugo");

// $mail->AddAddress("miguel@ideadiseno.com");
// $mail->AddAddress("sergio@ideadiseno.com");
// $mail->AddAddress("efren@ideadiseno.com");
// $mail->AddAddress("michel_gaia@outlook.es");
// $mail->AddAddress("lilia.barreto.neri@gmail.com");
// $mail->AddAddress("lalo@ideadiseno.com");



$mail->Subject = "Talleres y Grupos con información";

$mail->From = "no-replay@corporativogaia.com";        

$mail->Body ="Hola estos son los talleres y grupos en donde se ha resgistrado\r\n\r\n";

$mail->AddAttachment($archivoZip,$archivoZip);

if(!$mail->Send()) {
//echo "Error: " . $mail->ErrorInfo;
} else {
//echo "Mensaje enviado.";
}
*/
/*
function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $subject, $message) {
    $file = $path.$filename;
    echo "archivo".$file."<br>";
    $file_size = filesize($file);
    echo "Tamaño ".$filesize."<br>";
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    if (mail($mailto, $subject, "", $header)) {
    	
        echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
    }
}

$my_file = $fecha."_Talleres.zip"; // puede ser cualquier formato
$my_path = $_SERVER['DOCUMENT_ROOT']."/formularios/";
$my_name = "Miguel Lugo";
$my_mail = "miguel@ideadiseno.com";
$my_replyto = "tumail@tudominio.com";
$my_subject = "Le adjuntamos la credencial de invuitación al evento";
$my_message = "Tu mensaje";


mail_attachment($archivoZip, $my_path, "arkhein_red@hotmail.com", $my_mail, $my_name, $my_subject, $my_message);
*/
?>