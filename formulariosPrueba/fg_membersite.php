<?PHP
/*
    Registration/Login script from HTML Form Guide
    V1.0

    This program is free software published under the
    terms of the GNU Lesser General Public License.
    http://www.gnu.org/copyleft/lesser.html
    

This program is distributed in the hope that it will
be useful - WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

For updates, please visit:
http://www.html-form-guide.com/php-form/php-registration-form.html
http://www.html-form-guide.com/php-form/php-login-form.html

*/
require_once("class.phpmailer.php");
require_once("formvalidator.php");

class FGMembersite
{
    var $admin_email;
    var $from_address;
    
    var $username;
    var $pwd;
    var $database;
    var $tablename;
    var $connection;
    var $rand_key;
    
    var $error_message;
    
    //-----Initialization -------
    function FGMembersite()
    {
        $this->sitename = 'YourWebsiteName.com';
        $this->rand_key = '0iQx5oBk66oVZep';
    }
    
    function InitDB($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
        
    }
    function SetAdminEmail($email)
    {
        $this->admin_email = $email;
    }
    
    function SetWebsiteName($sitename)
    {
        $this->sitename = $sitename;
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function RegisterUser()
    {
        if(!isset($_POST['submitted']))
        {
           return false;
        }
        
        $formvars = array();
        
        if(!$this->ValidateRegistrationSubmission())
        {
            return false;
        }
        
        $this->CollectRegistrationSubmission($formvars);
        
        if(!$this->SaveToDatabase($formvars))
        {
            return false;
        }
        //print_r($formvars);
        if(!$this->SendUserConfirmationEmail($formvars))
        {
            return false;
        }

        $this->SendAdminIntimationEmail($formvars);
        
        return true;
    }

    function ConfirmUser()
    {
        if(empty($_GET['code'])||strlen($_GET['code'])<=10)
        {
            $this->HandleError("Please provide the confirm code");
            return false;
        }
        $user_rec = array();
        if(!$this->UpdateDBRecForConfirmation($user_rec))
        {
            return false;
        }
        
        $this->SendUserWelcomeEmail($user_rec);
        
        $this->SendAdminIntimationOnRegComplete($user_rec);
        
        return true;
    }    
    
    function Login()
    {
        if(empty($_POST['username']))
        {
            $this->HandleError("Usuario vacio!");
            return false;
        }
        
        if(empty($_POST['password']))
        {
            $this->HandleError("Password vacio!");
            return false;
        }
        
        $username = trim($_POST['username']);
        $password = trim($_POST['password']);
        
        if(!isset($_SESSION)){ session_start(); }
        if(!$this->CheckLoginInDB($username,$password))
        {
            return false;
        }
        
        $_SESSION[$this->GetLoginSessionVar()] = $username;
        
        return true;
    }
    
    function CheckLogin()
    {
         if(!isset($_SESSION)){ session_start(); }

         $sessionvar = $this->GetLoginSessionVar();
         
         if(empty($_SESSION[$sessionvar]))
         {
            return false;
         }
         return true;
    }
    
    function UserFullName()
    {
        return isset($_SESSION['id_emprendedor'])?$_SESSION['id_emprendedor']:'';
    }
   
    function idUser()
    {
        return isset($_SESSION['id_usuario'])?$_SESSION['id_usuario']:'';
    }

    function tipo()
    {
        return isset($_SESSION['tipo'])?$_SESSION['tipo']:'';
    }
    
    function LogOut()
    {
        session_start();
        
        $sessionvar = $this->GetLoginSessionVar();
        
        $_SESSION[$sessionvar]=NULL;
        
        unset($_SESSION[$sessionvar]);

        return true;
    }
    
    function EmailResetPasswordLink()
    {
        if(empty($_POST['email']))
        {
            $this->HandleError("Email vacio!");
            return false;
        }
        $user_rec = array();
        if(false === $this->GetUserFromEmail($_POST['email'], $user_rec))
        {
            return false;
        }
        if(false === $this->SendResetPasswordLink($user_rec))
        {
            return false;
        }
        return true;
    }
    
    function ResetPassword()
    {
        if(empty($_GET['email']))
        {
            $this->HandleError("Email vacio!");
            return false;
        }
        if(empty($_GET['code']))
        {
            $this->HandleError("reset code is empty!");
            return false;
        }
        $email = trim($_GET['email']);
        $code = trim($_GET['code']);
        
        if($this->GetResetPasswordCode($email) != $code)
        {
            $this->HandleError("Bad reset code!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($email,$user_rec))
        {
            return false;
        }
        
        $new_password = $this->ResetUserPasswordInDB($user_rec);
        if(false === $new_password || empty($new_password))
        {
            $this->HandleError("Error actualizando nuevo password");
            return false;
        }
        
        if(false == $this->SendNewPassword($user_rec,$new_password))
        {
            $this->HandleError("Error enviando nuevo password");
            return false;
        }
        return true;
    }
    
    function ChangePassword()
    {
        if(!$this->CheckLogin())
        {
            $this->HandleError("Not logged in!");
            return false;
        }
        
        if(empty($_POST['oldpwd']))
        {
            $this->HandleError("Password anterior esta vacio!");
            return false;
        }
        if(empty($_POST['newpwd']))
        {
            $this->HandleError("Nuevo password esta vacio!");
            return false;
        }
        
        $user_rec = array();
        if(!$this->GetUserFromEmail($this->UserEmail(),$user_rec))
        {
            return false;
        }
        
        $pwd = trim($_POST['oldpwd']);
        
        if($user_rec['contrasenya'] != md5($pwd))
        {
            $this->HandleError("La password anterior no coincide!");
            return false;
        }
        $newpwd = trim($_POST['newpwd']);
        
        if(!$this->ChangePasswordInDB($user_rec, $newpwd))
        {
            return false;
        }
        return true;
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return htmlentities($_SERVER['PHP_SELF']);
    }    
    
    function SafeDisplay($value_name)
    {
        if(empty($_POST[$value_name]))
        {
            return'';
        }
        return htmlentities($_POST[$value_name]);
    }
    
    function RedirectToURL($url)
    {
        header("Location: $url");
        exit;
    }
    
    function GetSpamTrapInputName()
    {
        return 'sp'.md5('KHGdnbvsgst'.$this->rand_key);
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }    
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_message .= $err."\r\n";
    }
    
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysql_error());
    }
    
    function GetFromAddress()
    {
        if(!empty($this->from_address))
        {
            return $this->from_address;
        }

        $host = $_SERVER['SERVER_NAME'];

        $from ="nobody@$host";
        return $from;
    } 
    
    function GetLoginSessionVar()
    {
        $retvar = md5($this->rand_key);
        $retvar = 'usr_'.substr($retvar,0,10);
        return $retvar;
    }
    //--------------inicio de sesion-----------------
    function CheckLoginInDB($username,$password)
    {
        if(!$this->DBLogin())
       
        {
            $this->HandleError("Database login failed!");
            return false;
        }      

        $pwdmd5 = md5($password);

        $qry = "SELECT idUsuarios, idEmprendedor, tipo FROM $this->tablename WHERE correo = '$username' AND contrasenia = '$pwdmd5'";
	echo $qry;
        $conexion = $this->connection;
        $sentencia = $conexion ->prepare($qry);
        $sentencia -> execute();

        $result = $sentencia -> rowCount();

        if(!$result || $result <= 0)
       
        {
            $this->HandleError("El usuario o password no coincide");
            return false;
        }
        
        $row = $sentencia -> fetchAll();

        foreach ($row as $row) {

               
            $_SESSION['id_usuario'] = $row['idUsuarios'];
            $_SESSION['id_emprendedor']  = $row['idEmprendedor'];//variable de sesion con el nombre 
            $_SESSION['tipo']  = $row['tipo'];//variable de sesion con el nombre 
        }
                 
        return true;

    }
    //--------fin inicio de sesion------------------
    
    function UpdateDBRecForConfirmation(&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   
        
        $confirmcode = $_GET['code'];

        $conexion = $this ->connection;

        $qry = "SELECT id_usuario, usuario, correo FRoM $this->tablename WHERE confirmacion=:confirmcode";

        $sentencia = $conexion -> prepare($qry);

        $sentencia-> bindParam(':confirmcode', $confirmcode, PDO::PARAM_STR);

        $sentencia -> execute();

        $result = $sentencia -> rowCount();
        

        if(!$sentencia || $result <= 0)
        {
            $this->HandleError("Wrong confirm code.");
            return false;
        }

        $row = $sentencia -> fetchAll();

        foreach ($row as $row ) {
        $user_rec['id_usuario'] = $row['id_usuario'];
        $user_rec['usuario'] = $row['usuario'];
        $user_rec['correo']= $row['correo'];
        }

        $qry = "UPDATE $this->tablename SET confirmacion='y' WHERE  confirmacion=:confirmcode";

        $sentencia1 =$conexion ->prepare($qry);

        $sentencia1 ->bindParam(':confirmcode', $confirmcode, PDO::PARAM_STR);

        $sentencia1->execute();
        
        if(!$sentencia1)
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$qry");
            return false;
        }      
        return true;
    }
    
    function ResetUserPasswordInDB($user_rec)
    {
        $new_password = substr(md5(uniqid()),0,8);
        
        if(false == $this->ChangePasswordInDB($user_rec,$new_password))
        {
            return false;
        }
        return $new_password;
    }
    

    //------------------cambiar contraseña-------------------------------------
    function ChangePasswordInDB($user_rec, $newpwd)
    {
       // $newpwd = $this->SanitizeForSQL($newpwd);

        $conexion = $this->connection;
        
        $qry = "UPDATE $this->tablename SET contrasenya=:password WHERE usuario=:id_user";

        $sentencia = $conexion -> prepare($qry);

        $sentencia -> bindParam(':password', md5($newpwd), PDO::PARAM_STR);

        $sentencia -> bindParam(':id_user', $user_rec['usuario'], PDO::PARAM_STR);

        $sentencia -> execute();

        if(!$sentencia)
        {
            $this->HandleDBError("Error updating the password \nquery:$qry");
            return false;
        }     
        return true;
    }
    //-----------------fin cambiar contraseña----------------------------------
    
    function GetUserFromEmail($email,&$user_rec)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }   

        $conexion = $this->connection;

       // $email = $this->SanitizeForSQL($email);
        
        $qry = ("SELECT * FROM $this->tablename WHERE correo=:email");

        $sentencia = $conexion ->prepare($qry);

        $sentencia->bindParam(':email', $email, PDO::PARAM_STR);

        $sentencia -> execute();

        $result = $sentencia -> rowCount();

        if(!$sentencia || $result <= 0)
        {
            $this->HandleError("There is no user with email: $email");
            return false;
        }
        $user_rec = $sentencia -> fetch(PDO::FETCH_ASSOC);
        
        return true;
    }
    
    function SendUserWelcomeEmail(&$user_rec)
    {
        $mail = new PHPMailer();
        
        $mail->CharSet = 'utf-8';

        $mail->IsSMTP();
        
        $mail->AddAddress($user_rec['correo'],$user_rec['usuario']);
        
        $mail->Subject = "Bienvenido a ".$this->sitename;

        $mail->From = $this->GetFromAddress();        
        
        $mail->Body ="Hola ".$user_rec['usuario']."\r\n\r\n".
        "Bienvenido! Tu registro en Admigas esta completo.\r\n".
        "\r\n";

        if(!$mail->Send())
        {
            $this->HandleError("Fallo envio de correo de bienvenida.");
            return false;
        }
        return true;
    }
    
    function SendAdminIntimationOnRegComplete(&$user_rec)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mail = new PHPMailer();
        
        $mail->CharSet = 'utf-8';

        $mail->IsSMTP();
        
        $mail->AddAddress($this->admin_email);
        
        $mail->Subject = "Registration Completed: ".$user_rec['usuario'];

        $mail->From = $this->GetFromAddress();         
        
        $mail->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$user_rec['usuario']."\r\n".
        "Email address: ".$user_rec['correo']."\r\n";
        
        if(!$mail->Send())
        {
            return false;
        }
        return true;
    }
    
    function GetResetPasswordCode($email)
    {
       return substr(md5($email.$this->sitename.$this->rand_key),0,10);
    }
    
    function SendResetPasswordLink($user_rec)
    {
        $email = $user_rec['correo'];
        
        $mail = new PHPMailer();

        $mail->IsSMTP();
        
        $mail->CharSet = 'utf-8';
        
        $mail->AddAddress($email,$user_rec['usuario']);
        
        $mail->Subject = "Su solicitud de restablecimiento de contraseña en Admigas";

        $mail->From = $this->GetFromAddress();
        
        $link = $this->GetAbsoluteURLFolder().
                '/regenerar_password.php?email='.
                urlencode($email).'&code='.
                urlencode($this->GetResetPasswordCode($email));

        $mail->Body ="Hola ".$user_rec['usuario']."\r\n\r\n".
        "Hubo una solicitud para restablecer su contraseña en Admigas\r\n".
        "Por favor, haz clic en el enlace de abajo para completar la solicitud: \r\n".$link."\r\n";
        
        if(!$mail->Send())
        {
            return false;
        }
        return true;
    }
    
    function SendNewPassword($user_rec, $new_password)
    {
        $email = $user_rec['correo'];
        
        $mail = new PHPMailer();

        $mail->IsSMTP();
        
        $mail->CharSet = 'utf-8';
        
        $mail->AddAddress($email,$user_rec['usuario']);
        
        $mail->Subject = "Tu nueva password en Admigas ";

        $mail->From = $this->GetFromAddress();
        
        $mail->Body ="Hola ".$user_rec['usuario']."\r\n\r\n".
        "Su contraseña se actualizo correctamente. ".
        "Aquí están sus datos actualizados:\r\n".
        "Usuario:".$user_rec['usuario']."\r\n".
        "Password:$new_password\r\n".
        "\r\n".
        "Login here: ".$this->GetAbsoluteURLFolder()."/index.php\r\n".
        "\r\n";
        
        if(!$mail->Send())
        {
            return false;
        }
        return true;
    }    
    
    function ValidateRegistrationSubmission()
    {
        //This is a hidden input field. Humans won't fill this field.
        if(!empty($_POST[$this->GetSpamTrapInputName()]) )
        {
            //The proper error is not given intentionally
            $this->HandleError("Automated submission prevention: case 2 failed");
            return false;
        }
        
        $validator = new FormValidator();
        $validator->addValidation("name","req","Please fill in Name");
        $validator->addValidation("email","email","The input for Email should be a valid email value");
        $validator->addValidation("email","req","Please fill in Email");
        $validator->addValidation("password","req","Please fill in Password");

        
        if(!$validator->ValidateForm())
        {
            $error='';
            $error_hash = $validator->GetErrors();
            foreach($error_hash as $inpname => $inp_err)
            {
                $error .= $inpname.':'.$inp_err."\n";
            }
            $this->HandleError($error);
            return false;
        }        
        return true;
    }
    
    function CollectRegistrationSubmission(&$formvars)
    {
        $formvars['usuario'] = $_POST['name'];
        $formvars['nombreUsuario'] = $_POST['username'];
        $formvars['correo'] = $_POST['email'];
        $formvars['id_tipo'] = $_POST['nivel'];
        $formvars['fechaAlta'] = $_POST['fechaAlta'];
        $formvars['contrasenya'] = $_POST['password'];
    }
    
    function SendUserConfirmationEmail(&$formvars)
    {
        $mail = new PHPMailer();

        $mail->IsSMTP();
        
        $mail->CharSet = 'utf-8';
        
        $mail->AddAddress($formvars['correo'],$formvars['usuario']);
        
        $mail->Subject = "Tu registro en ".$this->sitename;

        $mail->From = $this->GetFromAddress();        
        
        $confirmcode = $formvars['confirmcode'];
        
        $confirm_url = $this->GetAbsoluteURLFolder().'/sesion/confirmacion.php?code='.$confirmcode;
        
        $mail->Body ="Hola ".$formvars['usuario']."\r\n\r\n".
        "Gracias por registrarte en Admigas\r\n".
        "Usuario: ".$formvars['usuario']."\r\n".
        "Password: ".$formvars['contrasenya']."\r\n".
        "Por favor, haz clic en el enlace para confirmar su registro.\r\n".
        "$confirm_url\r\n".
        "\r\n";

        if(!$mail->Send())
        {
            $this->HandleError("Fallo envio de email de registro de confirmacion.");
            return false;
        }
        return true;
    }
    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    
    function SendAdminIntimationEmail(&$formvars)
    {
        if(empty($this->admin_email))
        {
            return false;
        }
        $mail = new PHPMailer();

        $mail->IsSMTP();
        
        $mail->CharSet = 'utf-8';
        
        $mail->AddAddress($this->admin_email);
        
        $mail->Subject = "New registration: ".$formvars['usuario'];

        $mail->From = $this->GetFromAddress();         
        
        $mail->Body ="A new user registered at ".$this->sitename."\r\n".
        "Name: ".$formvars['usuario']."\r\n".
        "Email address: ".$formvars['correo']."\r\n".
        "UserName: ".$formvars['usuario'];
        
        if(!$mail->Send())
        {
            return false;
        }
        return true;
    }
    
    function SaveToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
       /* if(!$this->Ensuretable())
        {
            return false;
        }*/
        if(!$this->IsFieldUnique($formvars,'correo'))
        {
            $this->HandleError("El correo esta registrado");
            return false;
        }
        
        if(!$this->IsFieldUnique($formvars,'usuario'))
        {
            $this->HandleError("El usuario esta registrado. Por favor intente otro");
            return false;
        }        
        if(!$this->InsertIntoDB($formvars))
        {
            $this->HandleError("Inserting to Database failed!");
            return false;
        }
        return true;
    }
    
    function IsFieldUnique($formvars,$fieldname)
    {
        $field_val = $formvars[$fieldname];

        $qry = "SELECT usuario FROM $this->tablename WHERE $fieldname=:fieldname";

        $conexion = $this->connection;

        $sentencia = $conexion->prepare($qry);

        $sentencia->bindParam(':fieldname', $field_val, PDO::PARAM_STR);

        $sentencia->execute();

        $result = $sentencia->fetchAll();

        //$result = mysqli_query($this->connection,$qry);   
        if($sentencia && $result != NULL)
        {
            return false;
        }
        return true;
    }
    
    //-----------------conexion a base de datos-------------------------
    function DBLogin()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=formularios',$this->username,$this->pwd);

        if(!$this->connection)
        {   
            $this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
            return false;
        }
        //if(!mysqli_select_db($this->connection, $this->database))
        //{
          //  $this->HandleDBError('Failed to select database: '.$this->database.' Please make sure that the database name provided is correct');
          //  return false;
        //}
        $qry = "SET NAMES 'UTF8'";

        $conexion = $this ->connection;

        $sentencia = $conexion ->prepare($qry);

        $sentencia ->execute();

        if(!$sentencia)
        {
            $this->HandleDBError('Error setting utf8 encoding');
            return false;
        }
        return true;
    }
    //---------------fin conexion a base de datos;----------------------    
    /*
    function Ensuretable()
    {
        $result = mysqli_query("SHOW COLUMNS FROM $this->tablename");   
        if(!$result || mysqli_num_rows($result) <= 0)
        {
            return $this->CreateTable();
        }
        return true;
    }
    
     function CreateTable()
    {
        $qry = "Create Table $this->tablename (".
                "id_user INT NOT NULL AUTO_INCREMENT ,".
                "name VARCHAR( 128 ) NOT NULL ,".
                "email VARCHAR( 64 ) NOT NULL ,".
                "phone_number VARCHAR( 16 ) NOT NULL ,".
                "username VARCHAR( 16 ) NOT NULL ,".
                "password VARCHAR( 32 ) NOT NULL ,".
                "confirmcode VARCHAR(32) ,".
                "PRIMARY KEY ( id_user )".
                ")";
                
        if(!mysqli_query($this->connection, $qry))
        {
            $this->HandleDBError("Error creating the table \nquery was\n $qry");
            return false;
        }
        return true;
    }*/
    
    //-------------insertar nuevo usuario-------------------------------
    function InsertIntoDB(&$formvars)
    {
    
        $confirmcode = $this->MakeConfirmationMd5($formvars['correo']);
        
        $formvars['confirmcode'] = $confirmcode;
        $status = 1;
        
        $insert_query = 'INSERT INTO '.$this->tablename.'(
                id_tipo,
                usuario,
                nombreUsuario,
                correo,
                contrasenya,
                confirmacion,
                fecha_alta,
                status
                )
                VALUES
                (:id_tipo,
                 :usuario,
                 :nombreUsuario,
                 :correo,
                 :contrasenya,
                 :confirmacion,
                 :fechaAlta,
                 :status
                )';

        $conexion = $this->connection;

        $sentencia = $conexion -> prepare($insert_query);    

        if(!$sentencia -> execute(array(':id_tipo' => $formvars['id_tipo'],
                                        ':usuario' => $formvars['usuario'],
                                        ':nombreUsuario' => $formvars['nombreUsuario'],
                                        ':correo' =>  $formvars['correo'],
                                        ':contrasenya' => md5($formvars['contrasenya']),
                                        ':confirmacion' =>  $confirmcode,
                                        ':fechaAlta' => $formvars['fechaAlta'],
                                        ':status' => $status)))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }        
        return true;
    }
    //-------------fin insertar nuevo usuario----------------------------

    function MakeConfirmationMd5($email)
    {
        $randno1 = rand();
        $randno2 = rand();
        return md5($email.$this->rand_key.$randno1.''.$randno2);
    }

 /*
    Sanitize() function removes any potential threat from the
    data submitted. Prevents email injections or any other hacker attempts.
    if $remove_nl is true, newline chracters are removed from the input.
    */
    function Sanitize($str,$remove_nl=true)
    {
        $str = $this->StripSlashes($str);

        if($remove_nl)
        {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
                );
            $str = preg_replace($injections,'',$str);
        }

        return $str;
    }    
    function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }    
}
?>
