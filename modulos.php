<?php 
session_start();

/**
 * Incluimos la conexion
 */
include '../conexion.php';

require_once("./sesion/include/membersite_config.php");

// if(!$fgmembersite->CheckLogin())
// {
//     $fgmembersite->RedirectToURL("./index.php");
//     exit;
// }

$idEmprendedor = $fgmembersite->UserFullName();

$sql = "SELECT taller, grupo, primerApellido, segundoApellido, nombre FROM emprendendores WHERE idemprendendores = $idEmprendedor";
$query = $conexion->prepare($sql);
$query->execute();
$row = $query->fetchAll();
$taller = $row[0][0];
$grupo = $row[0][1];
$nombre = $row[0][4]." ".$row[0][2]." ".$row[0][3];




?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modulos</title>
	<script src="../js/jquery-1.9.1.min.js"></script>
	<script src="../js/jquery-validate.js"></script>
	<script src="../js/modulos.js"></script>

	<link rel="stylesheet" href="../css/estilos.css">
</head>
<body>

	<header>
			
		<img src="http://corporativogaia.com.mx/gaiav2/wp-content/uploads/2014/05/logo1.png" alt="">
		<div id="datoEmpre">
			<p id="nombre">
				<?php echo $nombre; ?>
			</p>
			<a href="./sesion/salir.php">Salir</a>
		</div>

	</header>
	<div id="contenido">
		
	<br>
	<br>
	<br>
	<br>
	<form method="post" id="modulos">
		<input type="hidden" id="idEmprendedor" name="idEmprendedor" value="<?php echo $idEmprendedor ?>">
		
		<div id="taller">
			<a>Taller: <?php echo $taller; ?></a>
			<br>
			<a>Grupo: <?php echo $grupo; ?></a>
		</div>
		<p>Resumen de la Metodología Diadaara</p>
		
		<p>Módulo 2.- Identificación de los factores de innovación.</p>
		<div class="leyenda">
			<a>Menciona 2 factores de innovación que podrías llevar a cabo en tu negocio.</a>
		</div>
		<br>
		<textarea name="modulo2" id="modulo2" cols="100" rows="10" maxlength="3500"></textarea>

		<p>Módulo 3.- Desarrollo de un sistema de ayuda.</p>
		<div class="leyenda">
			<a>Menciona el objetivo de cada factor de innovación.</a>
		</div>
		<br>
		<textarea name="modulo3" id="modulo3" cols="100" rows="10" maxlength="5000"></textarea>

		<p>Módulo 4.- Análisis del costo-beneficio de los factores de innovación.</p>
		<div class="leyenda">
			<a>Factor de innovación seleccionado y porqué.</a>
		</div>
		<br>
		<textarea name="modulo4" id="modulo4" cols="100" rows="10" maxlength="5000"></textarea>

		<p>MÓDULO 5.  Sistema de Empresas de Generación I.</p>
		<div class="leyenda">
			<a>Mercado a atender.</a>
		</div>
		<br>
		<textarea name="modulo5_1" id="modulo5_1" cols="100" rows="10" maxlength="3500"></textarea>
		<br>
		<div class="leyenda">
			<a>Propuesta de innovación al negocio.</a>
		</div>
		<br>
		<textarea name="modulo5_2" id="modulo5_2" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Productos y/o servicios a ofrecer.</a>
		</div>
		<br>
		<textarea name="modulo5_3" id="modulo5_3" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Relación que vas a establecer con los clientes.</a>
		</div>
		<br>
		<textarea name="modulo5_4" id="modulo5_4" cols="100" rows="10" maxlength="3500"></textarea>
		<br>
		<div class="leyenda">
			<a>Líneas de negocio.</a>
		</div>
		<br>
		<textarea name="modulo5_5" id="modulo5_5" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Indicadores de calidad de tu negocio.</a>
		</div>
		<br>
		<textarea name="modulo5_6" id="modulo5_6" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Conocimiento y experiencia previa en el proyecto.</a>
		</div>
		<br>
		<textarea name="modulo5_7" id="modulo5_7" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Número de posibles empleos a generar.</a>
		</div>
		<br>
		<textarea name="modulo5_8" id="modulo5_8" cols="100" rows="10" maxlength="500"></textarea>
		<br>
		<div class="leyenda">
			<a href="../plan_financiero.xlsx">Descargar este archivo excel y realiza los ejercicio. Una vez terminado adjuntarlo.</a>
		</div>
		<br>
		<div class="leyenda">
			<a>Adjuntar Archivo</a><input type="file" name="plan_financiero[]" id="plan_financiero">
		</div>
		<p>MÓDULO 6. Redes de Innovación.</p>
		<div class="leyenda">
			<a>Menciona cómo te vas a vincular al ecosistema empresarial (academia, Gobierno a través de programas, otras empresas y/o proveedores, instituciones financieras).</a>
		</div>
		<br>
		<textarea name="modulo6_1" id="modulo6_1" cols="100" rows="10" maxlength="8000"></textarea>
		<br>
		<div class="leyenda">
			<a>¿Te gustaría ser canalizado a la Red Mover a México, para que te den seguimiento y te apoyen con asesoría para bajar fondos para tu proyecto?</a>
		</div>
		<br>
		<div class="leyenda">
			<a>Si</a><input type="radio" name="modulo6_2" id="modulo6_2" value="Si">	
			<a>No</a><input type="radio" name="modulo6_2" id="modulo6_2" value="No">
		</div>

		<p>MÓDULO 7. Auditoría de la Innovación.</p>
		<div class="leyenda">
			<a>Acciones que implementaras para fomentar de manera permanente a la innovación.</a>
		</div>
		<br>
		<textarea name="modulo7_1" id="modulo7_1" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
		<div class="leyenda">
			<a>Indicadores que utilizarás para medir a tu empresa en la implementación de la Innovación.</a>
		</div>
		<br>
		<textarea name="modulo7_2" id="modulo7_2" cols="100" rows="10" maxlength="5000"></textarea>
		<br>
	 	<button id="enviar">Guardar</button>

	</form>
	</div>
</body>
</html>