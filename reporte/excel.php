<?php

/**
 * Incluimos la conexion
 */
include '../conexion.php';

require_once '../libs/Classes/PHPExcel.php';
require_once '../libs/Classes/PHPExcel/IOFactory.php';
/**
 * Archivo que genera el excel con los datos enviados
 */

/**
 * Recuperamos los de la base de datos
 */

$sql = "SELECT taller, grupo FROM emprendendores ORDER BY taller, grupo";
$query = $conexion->prepare($sql);
$query->execute();
$row = $query->fetchAll();
$tam = count($row);
$j = 0;
$taller = "";
for ($i=0; $i < $tam; $i++) { 
					
	if ($taller != $row[$i]["taller"]) {
		$taller = $row[$i]["taller"];
		$talleres[$j] = $taller;
		$j++;
	}
}

$tam1 = count($talleres);

for ($i=0; $i < $tam1; $i++) { 

	$taller = $talleres[$i];

	$sql = "SELECT * FROM emprendendores WHERE taller= '".$taller."' ORDER BY grupo";
	$query = $conexion->prepare($sql);
	$query->execute();
	$row = $query->fetchAll();
	$grupoA = "";
	$grupoB = 0;
	$tam2 = count($row);
	 $j = 0;
	 for ($l=0; $l < $tam2; $l++) {

	 	if ($grupoA != $row[$l]["grupo"] ) {
						
			$grupoA = $row[$l]["grupo"];
			$grupoB = $grupoB + 1;
	 	}
	 }
		// // Creamos un objeto PHPExcel
		$objPHPExcel = new PHPExcel();
		// Leemos un archivo Excel 2007
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load("./METADATOS.xlsx");

		$objPHPExcel->getProperties()
		->setCreator("GAIA")//nombre autor
		->setLastModifiedBy("GAIA")//Ultimo usuario que lo modifico
		->setTitle("Metadatos")//Titulo
		->setSubject("Datos de usuarios registrados.")//Asunto
		->setDescription("atos de usuarios registrados..")//Descripcion
		->setKeywords("Excel Office 2007 openxml php")//Etiquetas
		->setCategory("Datos de usuarios registrados.");//Categoria

		if ($grupoB > 1) {
			
			for ($k=0; $k < $grupoB-1; $k++) { 

				$objClonedWorksheet = clone $objPHPExcel->getSheetByName("Hoja1");
				$objClonedWorksheet->setTitle("hoja".$k);
				$objPHPExcel->addSheet($objClonedWorksheet);
			}

		}

	$grupo = $row[0]["grupo"];
	$j = 0;
	$nombreAnt = $row[0]["taller"]." ".$row[0]["grupo"];

	foreach ($row as $row) {

		if ($grupo != $row["grupo"]) {
			
			$j = $j + 1;
			$grupo = $row["grupo"];

		} 
		
		$objPHPExcel->getActiveSheet($j);
		 // Indicamos que se pare en la hoja uno del libro
		 $objPHPExcel->setActiveSheetIndex($j);
		 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
		 $total_sheets=$objPHPExcel->getSheetCount();
		 $allSheetName=$objPHPExcel->getSheetNames();
		 //Se obtiene el número máximo de filas
		 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
		 //Se obtiene el número máximo de columnas
	 	$highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
		 //echo $highestColumn;
		 $highestColumn = $highestColumn + 1;
		 // Renombrar Hoja
		 $objPHPExcel->getActiveSheet()->setTitle($row["taller"]." ".$row["grupo"]);

		 $idEmprendendores = $row['idemprendendores'];

		 $sql = "SELECT giro, nombreProyecto FROM modulo_1 WHERE idEmprendendores = $idEmprendendores";
		 $query = $conexion->prepare($sql);
		 $query->execute();
		 $row1 = $query->fetchAll();

		 if ($row1 == null) {
		 	$giro = "";
		 	$servicio = "";
		 } else {
		 	$giro = $row1[0][0];
		 	$servicio = $row1[0][1];
		 }

		 /**
		  * Verificamos si esta completo sus modulos
		  */
		 $sql = "SELECT * FROM modulo_1, modulo_1_Mer, modulo_1_ET, modulo_1_EA, modulo_1_EF WHERE modulo_1.idmodulo_1 = modulo_1_EA.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_EF.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_ET.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_Mer.idModulo_1 AND modulo_1.idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo1 = $query->fetchAll();

		if ($modulo1 == null) {
			$m1 = "Incompleto";
		} else {
			$m1 = "Completo";
		}
				
		$sql = "SELECT * FROM modulo_2 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo2 = $query->fetchAll();
		
		if ($modulo2 == null) {
			$m2 = "Incompleto";
		} else {
			$m2 = "Completo";
		}

		$sql = "SELECT * FROM modulo_3 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo3 = $query->fetchAll();

		if ($modulo3 == null) {
			$m3 = "Incompleto";
		} else {
			$m3 = "Completo";
		}
		
		$sql = "SELECT * FROM modulo_4 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo4 = $query->fetchAll();

		if ($modulo4 == null) {
			$m4 = "Incompleto";
		} else {
			$m4 = "Completo";
		}

		$sql = "SELECT * FROM modulo_5 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo5 = $query->fetchAll();

		if ($modulo5 == null) {
			$m5 = "Incompleto";
		} else {
			$m5 = "Completo";
		}
		
		$sql = "SELECT * FROM modulo_6 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo6 = $query->fetchAll();

		if ($modulo6 == null) {
			$m6 = "Incompleto";
		} else {
			$m6 = "Completo";
		}

		$sql = "SELECT * FROM modulo_7 WHERE idEmprendendores = $idEmprendendores";
		$query = $conexion->prepare($sql);
		$query->execute();
		$modulo7 = $query->fetchAll();

		if ($modulo7 == null) {
			$m7 = "Incompleto";
		} else {
			$m7 = "Completo";
		}


		
		// Agregar Informacion
		$objPHPExcel->setActiveSheetIndex($j)
		->setCellValue('E'.$highestColumn, $row["tipoBene"])
		->setCellValue('I'.$highestColumn, $row["primerApellido"])
		->setCellValue('J'.$highestColumn, $row["segundoApellido"])
		->setCellValue('K'.$highestColumn, $row["nombre"])
		->setCellValue('L'.$highestColumn, $row["fechaNac"])
		->setCellValue('M'.$highestColumn, $row["sexo"])
		->setCellValue('N'.$highestColumn, $row["estadoCivil"])
		->setCellValue('O'.$highestColumn, $row["estudios"])
		->setCellValue('P'.$highestColumn, $row["identificacion"])
		->setCellValue('Q'.$highestColumn, $row["numIdentificacion"])
		->setCellValue('R'.$highestColumn, $row["nacionalidad"])
		->setCellValue('S'.$highestColumn, $row["entidadNac"])
		->setCellValue('T'.$highestColumn, $row["curp"])
		->setCellValue('U'.$highestColumn, $row["domicilio"])
		->setCellValue('V'.$highestColumn, $row["numExt"])
		->setCellValue('W'.$highestColumn, $row["numInt"])
		->setCellValue('X'.$highestColumn, $row["calle1"])
		->setCellValue('Y'.$highestColumn, $row["calle2"])
		->setCellValue('Z'.$highestColumn, $row["otraRef"])
		->setCellValue('AA'.$highestColumn, $row["colonia"])
		->setCellValue('AB'.$highestColumn, $row["localidad"])
		->setCellValue('AC'.$highestColumn, $row["municipio"])
		->setCellValue('AD'.$highestColumn, $row["entidad"])
		->setCellValue('AE'.$highestColumn, $row["cp"])
		->setCellValue('AF'.$highestColumn, $row["telFijo"])
		->setCellValue('AG'.$highestColumn, $row["telCel"])
		->setCellValue('AH'.$highestColumn, $row["correo"])
		->setCellValue('AI'.$highestColumn, $row["contrasenia"])
		->setCellValue('AJ'.$highestColumn, $servicio)
		->setCellValue('AK'.$highestColumn, $giro)
		->setCellValue('AL'.$highestColumn, $m1)
		->setCellValue('AM'.$highestColumn, $m2)
		->setCellValue('AN'.$highestColumn, $m3)
		->setCellValue('AO'.$highestColumn, $m4)
		->setCellValue('AP'.$highestColumn, $m5)
		->setCellValue('AQ'.$highestColumn, $m6)
		->setCellValue('AR'.$highestColumn, $m7);

	}
		
				
	 $ruta = "../talleres/".$taller."/";
	 $ruta = str_replace(" ", "_", $ruta) ;
	 $nombreArchivo = $taller.".xlsx" ;

	 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	 $objWriter->save($nombreArchivo);

	 rename($nombreArchivo, $ruta.$nombreArchivo);
	 chmod($ruta.$nombreArchivo, 0777);
}
?>