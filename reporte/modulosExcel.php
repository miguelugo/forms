<?php

/**
 * Incluimos la conexion
 */
include '../conexion.php';

require_once '../libs/Classes/PHPExcel.php';
require_once '../libs/Classes/PHPExcel/IOFactory.php';
/**
 * Archivo que genera el excel con los datos enviados
 */

/**
 * Recuperamos los de la base de datos
 */

$sql = "SELECT idemprendendores FROM emprendendores";
$query = $conexion->prepare($sql);
$query->execute();
$row = $query->fetchAll();
$tam = count($row);
$j = 0;
$id = 0;

for ($i=0; $i < $tam; $i++) { 
					
	if ($id != $row[$i]["idemprendendores"]) {
		$id = $row[$i]["idemprendendores"];
		$ids[$j] = $id;
		$j++;
	}
}

$tam2 = count($ids);

$j = 0;

for ($i=0; $i < $tam2; $i++) { 
	
	$id = $ids[$i];

	$sql ="SELECT nombre, primerApellido, segundoApellido, taller, grupo FROM emprendendores WHERE idemprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$datos = $query->fetchAll();
	$nombre = $datos[0][1]." ".$datos[0][2]." ".$datos[0][0];
	$taller = "../talleres/".$datos[0][3]."/".$datos[0][4]."/";
	$taller = str_replace("//", "/", $taller);
	$sql = "SELECT * FROM modulo_1, modulo_1_Mer, modulo_1_ET, modulo_1_EA, modulo_1_EF WHERE modulo_1.idmodulo_1 = modulo_1_EA.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_EF.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_ET.idModulo_1 AND modulo_1.idmodulo_1 = modulo_1_Mer.idModulo_1 AND modulo_1.idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo1 = $query->fetchAll();
	$tamModulo1 = count($modulo1[0]);

	$sql = "SELECT * FROM modulo_2 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo2 = $query->fetchAll();

	$sql = "SELECT * FROM modulo_3 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo3 = $query->fetchAll();

	$sql = "SELECT * FROM modulo_4 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo4 = $query->fetchAll();

	$sql = "SELECT * FROM modulo_5 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo5 = $query->fetchAll();

	$sql = "SELECT * FROM modulo_6 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo6 = $query->fetchAll();

	$sql = "SELECT * FROM modulo_7 WHERE idEmprendendores = $id";
	$query = $conexion->prepare($sql);
	$query->execute();
	$modulo7 = $query->fetchAll();

	echo $nombre."<br>";
	echo $taller."<br>";

	// Creamos un objeto PHPExcel
	$objPHPExcel = new PHPExcel();
	// Leemos un archivo Excel 2007
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objPHPExcel = $objReader->load("./modulos.xlsx");

	$objPHPExcel->getProperties()
	->setCreator("GAIA")//nombre autor
	->setLastModifiedBy("GAIA")//Ultimo usuario que lo modifico
	->setTitle("Metadatos")//Titulo
	->setSubject("Datos de usuarios registrados.")//Asunto
	->setDescription("atos de usuarios registrados..")//Descripcion
	->setKeywords("Excel Office 2007 openxml php")//Etiquetas
	->setCategory("Datos de usuarios registrados.");//Categoria

	// Creamos un objeto PHPExcel
		$objPHPExcel = new PHPExcel();
		// Leemos un archivo Excel 2007
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load("./modulos.xlsx");

		$objPHPExcel->getProperties()
		->setCreator("GAIA")//nombre autor
		->setLastModifiedBy("GAIA")//Ultimo usuario que lo modifico
		->setTitle("Metadatos")//Titulo
		->setSubject("Datos de usuarios registrados.")//Asunto
		->setDescription("atos de usuarios registrados..")//Descripcion
		->setKeywords("Excel Office 2007 openxml php")//Etiquetas
		->setCategory("Datos de usuarios registrados.");//Categoria

	 		
		 		 $objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

				 //nombre
				 $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('B1', $nombre);
				//giro
				$objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('B2', $modulo1[0][2]);				
				//servicio
				$objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('B3', $modulo1[0][1]);
				 $k = 6;

				 for ($m=0; $m <= $tamModulo1 ; $m++) { 
						
					 if ($modulo1[0][$m] == "A") {

					 $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('B'.$k, "X");
					 	$k = $k + 1;

					 } elseif ($modulo1[0][$m] == "B") {
					 	
					 $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('C'.$k, "X");
					 	$k = $k + 1;

					 }elseif ($modulo1[0][$m] == "C") {

					 $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('D'.$k, "X");
					 	$k = $k + 1;
					 	
					 }elseif ($modulo1[0][$m] == "D") {

					 $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('E'.$k, "X");
					 	$k = $k + 1;
					 	
					 }

					 if ($k == 18) {
					 	
					 	$k = 20;

					 } elseif ($k == 37) {
					 	
					 	$k = 39;

					 } elseif ($k == 52) {
					 	
					 	$k = 54;

					 } elseif ( $k == 74) {
					 	
					 	$k = 76;

					 } 

				 }//for modulo 1
			 	 	
		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A89', $modulo2[0][2]);

		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A93', $modulo3[0][2]);

		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A97', $modulo4[0][2]);
		 	 
		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A101', $modulo5[0][2])
					 	->setCellValue('A103', $modulo5[0][3])
					 	->setCellValue('A105', $modulo5[0][4])
					 	->setCellValue('A107', $modulo5[0][5])
					 	->setCellValue('A109', $modulo5[0][6])
					 	->setCellValue('A111', $modulo5[0][7])
					 	->setCellValue('A113', $modulo5[0][8])
					 	->setCellValue('A115', $modulo5[0][9]);
		 	 	
		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A119', $modulo6[0][2])
					 	->setCellValue('A122', $modulo6[0][3]);
		 	 	
		 	 	$objPHPExcel->getActiveSheet($j);
				 // Indicamos que se pare en la hoja uno del libro
				 $objPHPExcel->setActiveSheetIndex($j);
				 //se obtienen las hojas, el nombre de las hojas y se pone activa la primera hoja
				 $total_sheets=$objPHPExcel->getSheetCount();
				 $allSheetName=$objPHPExcel->getSheetNames();
				 //Se obtiene el número máximo de filas
				 $highestRow = $objPHPExcel->setActiveSheetIndex($j)->getHighestColumn();
				 //Se obtiene el número máximo de columnas
				 $highestColumn = $objPHPExcel->setActiveSheetIndex($j)->getHighestRow();
				// Agregar Informacion

					  $objPHPExcel->setActiveSheetIndex($j)
					 	->setCellValue('A125', $modulo7[0][2])
					 	->setCellValue('A127', $modulo7[0][3]);


	//Guardamos el archivo en formato Excel 2007
	//Si queremos trabajar con Excel 2003, basta cambiar el 'Excel2007' por 'Excel5' y el nombre del archivo de salida cambiar su formato por '.xls'	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($taller.$nombre.".xlsx");
	//copy($taller."_".$grupos.".xlsx", "./".$taller."/".$grupos."/".$taller."_".$grupos.".xlsx");
	chmod($taller.$nombre.".xlsx", 0777);

}//for tam2


?>